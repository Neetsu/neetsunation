#!/bin/bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

CRONLINE="1 0 * * * php $BASEDIR/app/console ck:check-due-date --env=prod"
if !(crontab -l 2>/dev/null|grep -Fq "$CRONLINE"); then
 (crontab -l 2>/dev/null;echo "$CRONLINE")|crontab -
fi

CRONLINE="0 * * * * php $BASEDIR/app/console ck:daily-report -q --env=prod"
if !(crontab -l 2>/dev/null|grep -Fq "$CRONLINE"); then
 (crontab -l 2>/dev/null;echo "$CRONLINE")|crontab -
fi
