/*
 * List of new users to be added by the sql script add_additional_users.sql
 */

INSERT INTO tmp_user (first_name, last_name, email) VALUES
 ('Steve',   'Austin',   'steve.austin@requestdetail.net'),
 ('Tom',     'Price',    'tom.price@requestdetail.net');
