#!/bin/bash
cd "$(dirname "${BASH_SOURCE[0]}")"
if [ \( ! -f mysql_conf \) -o mysql_conf -ot ../app/config/parameters.yml ]; then
  umask 177
  source make_mysql_conf > mysql_conf
fi
mysql --defaults-extra-file=mysql_conf -e 'UPDATE request SET traffic_light=2 WHERE due_date<CURDATE() AND phase="Delivery";'
