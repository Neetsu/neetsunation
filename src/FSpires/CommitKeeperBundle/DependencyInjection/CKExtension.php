<?php

namespace FSpires\CommitKeeperBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use FSpires\CommitKeeperBundle\Model\Enum\Selection;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CKExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
      /*
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
      */

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('security.yml');
        $loader->load('services.yml');
        $loader->load('controllers.yml');

        // Set version info in container
        require __DIR__ .'/../../../../web/version.php';
        $container->setParameter('ck.version', $version);

        // Set the commitment selections that we use
        $container->setParameter('ck.selections', join('|',Selection::getAll()));

        // Set the possible commitment types
        $container->setParameter('ck.commitmentTypes', join('|',RequestType::getAll()));
    }
}
