<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;
use FSpires\CommitKeeperBundle\Model\RequestTableInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Model\Enum\Selection as RS;

class TableController extends MainpageController {
  private $reqTable;
  private $dateFormat;

  /**
   * Constructor
   *
   * @param RequestTableInterface $reqTable
   * @param DateFormatInterface $dateFormat
   */
  public function __construct(RequestTableInterface $reqTable,
                              DateFormatInterface $dateFormat) {
    $this->reqTable = $reqTable;
    $this->dateFormat = $dateFormat;
  }

  /**
   * Parse the "table" part of the URL
   *
   * @param string $table The URL part to parse
   * @return string $sortcol the sort column
   */
  private function parseTable($table) {
    return substr($table,5);
  }

  /**
   * Parse the sort column from the URL data
   * @param string $sorting The URL part to parse
   * @return string $sortcol The sort column
   */
  private function parseSorting($sorting) {
    $sortcol = false;
    if (substr($sorting,0,4) == 'Sort') {
      // We have no table column with a name with more than 20 characters?
      $sortcol = substr($sorting,4,20);
      // May be we need more checks here
    }
    return $sortcol;
  }

  /**
   * This is the action triggered for the top table
   * Paramaters for the requests table:
   *
   * @param Request $httpRequest
   * @param string $userStr containts userId of the user the table is for
   * @param $table
   * @param string $reqSelect Get only requests from $reqSelect
   * @return RedirectResponse|Response
   */
  public function indexAction(Request $httpRequest, $userStr, $table, $reqSelect)
  {
    // Redirect if $reqSelect is not set
    if (!$reqSelect) {
      if (!$this->securityContext->isGranted(
                   new Expression("hasRole('ROLE_TYPE3')")
                                             )) {
        if ($this->securityContext->isGranted(
                   new Expression("hasRole('ROLE_TYPE2')")
                                              )) {
          $routeName = 'CK_confirm';
        } else {
          $routeName = 'login';
        }
        $newUrl = $this->router->generate($routeName);
      } else {
        $reqSelect = $this->findFirstNonEmptyTable();
        $newUrl = $this->makeTableUrl($table, $reqSelect, $userStr);
      }
      return $this->redirect($newUrl, Response::HTTP_TEMPORARY_REDIRECT);
    }

    try {
      $this->initUserId($userStr);
    } catch (AccessDeniedException $ex) {
      return $this->logoutToAnotherUser($this->makeTableUrl($table, $reqSelect));
    }
    $sorting = $this->parseTable($table);


    // Get data for header and sidebar
    $templData = $this->getMainPageData($table, $reqSelect);
    $template = 'CKBundle:Table:main.html.twig';

    // We must set the default table for other pages to return to to this
    $thisUrl = $this->makeTableUrl('table'.$sorting, $reqSelect, $userStr);
    $this->session->set('returnTableUrl', $thisUrl);

    // Need a base url for links to individual commitments
    // This is used when we click on a row in the table
    $templData['CommitmentBaseUrl'] = $this->router->generate('CK_commitment');


    $sortcol = $this->parseSorting($sorting);

    // Set the selection class
    $repository = $this->em->getRepository('CKBundle:Request');
    $s_class = 'FSpires\\CommitKeeperBundle\\Model\\Selection\\' . $reqSelect;
    $selection = new $s_class($repository);

    // Hand over control to the table model object that will do
    // the main work for us
    $templData = array_merge($templData, $this->reqTable->getData(
       $this->getUserId(), $this->getUser()->getGroup()->getId(),
       $selection, $sortcol, 'RT_'));

    $templData['DateFormat'] = $this->dateFormat->getPHPpattern();

    // Welcome screen
    if ($httpRequest->request->get('noWelcomeScreen')) {
      $this->getUser()->setWelcomeScreen(false);
      $this->em->flush();
    }
    $templData['hasWelcomeScreen'] = $this->getUser()->hasWelcomeScreen();
    $templData['form_action'] = $thisUrl;

    // Send the data to a twig template for viewing
    $templData['HasOnload'] = true;
    return $this->render($template, $templData);
  }

  /**
   * Find the first non-empty table
   * If all are empty, return the first
   */
  public function findFirstNonEmptyTable() {
    $reqSelect = RS::PendingByMe; // set to default value
    $mainData = $this->getMainPageData();
    $tabs = $mainData['tabs'];
    foreach($tabs as $tab) {
      if ($tab['count'] > 0) {
        $reqSelect = $tab['id'];
        break;
      }
    }
    return $reqSelect;
  }
}
