<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FSpires\CommitKeeperBundle\Model\UserFactory as UserFactoryInterface;
use FSpires\CommitKeeperBundle\Entity\UserName;

/**
 * Controller for ajax requests
 */
class AjaxController extends AbstractController {
  private $formFactory;
  private $userFactory;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              UserFactoryInterface $userFactory)
  {
    $this->formFactory = $formFactory;
    $this->userFactory = $userFactory;
  }

  /**
   * Make a new contact
   */
  public function newContactAction(Request $request) {

    $newUser = new UserName();
    $form = $this->formFactory->create('user_name_input', $newUser, array('virtual'  => false, 'required'=>false));
    $form->bind($request);
    $ok = false;
    if ($form->isValid()) {
      $user = $this->securityContext->getToken()->getUser();
      $contact = $this->userFactory->newContact($newUser, $user);

      $isNewContact = false;
      if (!$user->hasContact($contact)) {
        $isNewContact = true;
        $user->addContact($contact);
        $this->em->flush();
      }

      $ok = true;
    }

    $ret = array('error'=>false);
    if ($ok) {
      $ret['user'] = array('id' => $contact->getId(),
                           'fname' => $contact->getFirstName(),
                           'lname' => $contact->getLastName(),
                           'email' => $contact->getEmail(),
                           'isNewContact' => $isNewContact
                           );
    } else {
      $ret['error'] = true;
      $templData = array('formNewContact' => $form->createView());
      $ret['form'] = $this->templating->render('CKBundle:Form:Include/newContact.html.twig', $templData);
    }
    return new JsonResponse($ret);
  }
}
