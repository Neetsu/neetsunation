<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface as DoctrineInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface as TemplatingInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use FSpires\CommitKeeperBundle\Entity\User;

/**
 * Abstract controller for the CKBundle
 * Used to contain common methods and data for all the controllers
 */
class AbstractController {
  /**
   * @var EntityManagerInterface
   */
  protected $em;

  /**
   * @var array
   */
  protected $version;

  /**
   * @var SecurityContextInterface
   */
  protected $securityContext;

  /**
   * @var TemplatingInterface
   */
  protected $templating;

  /**
   * @var RouterInterface
   */
  protected $router;

  /**
   * @var SessionInterface
   */
  protected $session;

  /**
   * @var bool
   */
  protected $session_canari=false;

  /**
   * I made this an init function instead of a constructor
   * so it would be less hassle for children of this object
   * to have their own constructors
   */
  public function InitController(EntityManagerInterface $em,
                                 SecurityContextInterface $sec_context,
                                 TemplatingInterface $templating,
                                 RouterInterface $router,
                                 SessionInterface $session,
                                 array $version)
  {
    $this->em = $em;
    $this->version = $version;
    $this->securityContext = $sec_context;
    $this->templating = $templating;
    $this->router = $router;
    $this->session = $session;
    $this->set_session_canari();
  }

  /**
   * Some times we just get a blank session
   * Set a session canari so we can test if the session is alive
   */
  private function set_session_canari()
  {
    $this->session_canari = $this->session->get('session_canari',false);
    if ('alive' != $this->session_canari) {
      // We have a dead session canari, try to set it for the next request
      $this->session->set('session_canari','alive');
    }
  }

  /**
   * Redirect to same page if we do not have a live session
   */
  protected function assert_live_session($request)
  {
    $redir = false;
    if ('alive' != $this->session_canari) {
      $redir = $this->makeResponseNoCache(
         $this->redirect($request->getRequestUri()));
    }
    return $redir;
  }

  /**
   * Make sure the page is not cached
   */
  protected function makeResponseNoCache($response) {
    $response->headers->set('Expires', 'Thu, 19 Nov 1981 08:52:00 GMT');
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    return $response;
  }

  /**
   * Renders a view.
   *
   * @param string   $view The view name
   * @param array    $parameters An array of parameters to pass to the view
   * @param Response $response A response instance
   *
   * @return Response A Response instance
   */
  public function render($view, array $parameters = array(), Response $response = null)
  {
    return $this->templating->renderResponse($view, $parameters, $response);
  }

  /**
   * Returns a RedirectResponse to the given URL.
   *
   * @param string  $url The URL to redirect to
   * @param integer $status The status code to use for the Response
   *
   * @return RedirectResponse
   */
  public function redirect($url, $status = 303)
  {
    // Try to not let the client wait if some after events (like emails) are being done
    $response = new RedirectResponse($url, $status, array('Connection' =>'close'));
    // Most clients will now close the connection when they have received Content-Length bytes
    $response->headers->set('Content-Length', strlen($response->getContent()));
    return $response;
  }

  /**
   * Get the URL to return to when a user clicks "Cancel"
   * or has submitted a form
   *
   * @param User $user Get default from this user
   * @return string Url to the table the user shall return to
   */
  public function getReturnUrl(User $user=null)
  {
    $url = $this->session->get('returnTableUrl', false);
    if (!$url) {
      if ($user) {
        $url = $user->getDefaultTableUrl();
      }
      if (!$url) {
        $url = $this->router->generate('CK_default');
      }
    }
    return $url;
  }
}
