<?php
namespace FSpires\CommitKeeperBundle\Controller;

class UpgradeController extends MainpageController
{
  /**
   * Parse the "action" part of the URL
   * @param string $action The URL part to parse
   * @return string The action
   */
  private function parseAction($action)
  {
    if ('none'==$action) {
      $action = false;
    }
    return $action;
  }

  public function indexAction($action='none')
  {
    $action = $this->parseAction($action);

    $templData = $this->getMainPageData();
    $templData['action'] = $action;

    // Send the data to a twig template for viewing
    $template = 'CKBundle:Upgrade:index.html.twig';
    return $this->render($template, $templData);
  }
}
