<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FSpires\CommitKeeperBundle\Entity\TimeZone;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Model\UserFactory as UserFactoryInterface;

/**
 * Controller for user settings
 */
class SettingsController extends MainpageController {
  private $formFactory;
  private $userFactory;
  private $useLanguage;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              UserFactoryInterface $userFactory,
                              $useLanguage)
  {
    $this->formFactory = $formFactory;
    $this->userFactory = $userFactory;
    $this->useLanguage = $useLanguage;
  }

  public function chooseLanguageAction($url)
  {
    $prefix = '';
    if ('app' == substr($url, 0, 3)) {
      $pos = strpos($url, '/', 3);
      if ($pos) {
        $prefix = substr($url, 0, $pos);
        $url = substr($url, $pos+1);
      } else {
        // Begins with 'app', but has no '/'
        $prefix = $url;
        $url = '';
      }
    }

    $url = ltrim($url, '/');

    if ('images/' === substr($url, 0, 7)) {
      throw new NotFoundHttpException("Page \"/$url\" not found.");
    }
    if ('css/' === substr($url, 0, 4)) {
      throw new NotFoundHttpException("Page \"/$url\" not found.");
    }
    if ('js/' === substr($url, 0, 3)) {
      throw new NotFoundHttpException("Page \"/$url\" not found.");
    }

    if ('/' == substr($url, 2, 1)) {
      $url = htmlentities($url);
      throw new NotFoundHttpException("Page \"/$url\" not found.");
    }

    if (!$this->useLanguage) {
      return $this->redirect($prefix . '/en/' . $url, 301);
    }

    $templateData = array(
      'languages' => array(
        'en' => 'English',
        'es' => 'Español'
      ),
      'urlPrefix' => $prefix,
      'urlEnding' => $url
    );
    $template = 'CKBundle:Settings:language.html.twig';
    return $this->render($template, $templateData);
  }

  /**
   * Show a form for editing the users preferences
   */
  public function editSettingsAction(Request $request) {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }

    // Get the user
    /** @var User $user */
    $user = $this->securityContext->getToken()->getUser();

    // Get the picture, (just to make sure the default is set)
    $picture = $user->getPicture($this->em);

    // Get the form to change the personal information
    $formPersonal = $this->formFactory->create('edit_personal', $user);

    $backURL = $this->getReturnUrl($user);
    $isPost = $request->isMethod('POST');
    $hasErrors = false;

    if ($isPost) {
      if ($request->request->has('savePersonal')) {
        $formPersonal->submit($request);
        if ($formPersonal->isValid()) {
          $timeZone = TimeZone::createFromOffset($user->getTimeZoneOffset());
          $user->setTimeZoneSlot($timeZone->getSlot());

          // Update picture
          //$picture->setIsGravatar($request->request->has('isGravatar'));

          $this->em->flush();
          return $this->redirect($backURL);
        }
        $hasErrors = true;
      }
    }

    $group = $user->getGroup();
    $group->setEntityManager($this->em);

    // Get the form to edit the group preferences
    $formGroup = $this->formFactory->create('edit_group', $group);

    if ($isPost && !$hasErrors) {
      $formGroup->submit($request); // This will update $group
      if ($formGroup->isValid()) {
        $editPressed = false;
        $editBtn = $request->request->get('edit');
        if (count($editBtn) > 0) {
          $editPressed = true;
          $idx = key($editBtn);
          $oldCategories = $group->getCategories();
          $catName = trim($oldCategories[$idx]->getName());
        }

        $this->em->flush();
        $group->saveCategories();

        $retURL = false;
        if ($editPressed) {
          $catId = false;
          if ($catName) {
            $categories = $user->getCategories($this->em);
            foreach ($categories as $cat) {
              if ($cat->getName() == $catName) {
                $catId = $cat->getId();
                break;
              }
            }
          }
          if ($catId) {
            $retURL = $this->router->generate('CK_edit_usertags_category',
                                              array('category_id'=>$catId));
          } else {
            $formGroup = $this->formFactory->create('edit_group', $group);
            // Set error on formGroup
            $error =  new FormError(
              'Cannot find the right category to edit. It may have an empty label.',
              'settings.categoryNotFound'
              );
            $formGroup->addError($error);
          }
        } else {
          // No edit button pressed
          $retURL = $backURL;
        }
        if ($retURL) {
          return $this->redirect($retURL);
        }
      }
    }

    // Set data for the template
    $templData = $this->getMainPageData();
    $templData['formPersonal'] = $formPersonal->createView();
    $templData['formGroup'] = $formGroup->createView();
    $templData['form_action'] = $request->getRequestUri();
    $templData['back_url'] = $backURL;

    // Picture
    $templData['picture'] = $picture;

    // Categories
    $templData['categories'] = $user->getCategories($this->em);

    $template = 'CKBundle:Settings:edit_settings.html.twig';
    return $this->render($template, $templData);
  }

  /**
   * Show a form for editing the users ID and login infortmation
   */
  public function editIdAction(Request $request) {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }
    if (!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
      throw new AccessDeniedException();
    }
    // Get the user
    $user = $this->securityContext->getToken()->getUser();

    // Get the form to edit the user
    $formUser = $this->formFactory->create('user_register', $user,
                                           array('short_email' => true));

    $backURL = $this->getReturnUrl($user);

    if ('POST' == $request->getMethod()) {
      $formUser->submit($request); // This will update the $user object
      if ($formUser->isValid()) {
        // Save to the database
        $this->userFactory->save($user);
        return $this->redirect($backURL);
      }
    }

    // Set data for the template
    $templData = $this->getMainPageData();

    $templData['formUser'] = $formUser->createView();
    $templData['form_action'] = $request->getRequestUri();
    $templData['back_url'] = $backURL;
    $templData['addOpenIdUrl'] = $this->router->generate('CK_addopenid');

    $template = 'CKBundle:Settings:edit_id.html.twig';
    return $this->render($template, $templData);
  }

  /**
   * Show a form for editing one category of tags for a user
   */
  public function editUserTagCategoryAction(Request $request, $category_id)
  {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }
    if (!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {
      throw new AccessDeniedException();
    }
    // Get the user
    $user = $this->securityContext->getToken()->getUser();
    $user_id = $user->getId();

    $group_id = $user->getGroup()->getId();
    $user_repo = $this->em->getRepository('CKBundle:User');
    $categories = $user_repo->findCategories($user_id, $group_id, $category_id);
    $category = false;
    foreach($categories as $cat) {
      if ($cat->getId() == $category_id) {
        $category = $cat;
        break;
      }
    }
    if (!$category) {
      throw new \InvalidArgumentException('Invalid category id: '
                                          . $category_id);
    }

    $form = $this->formFactory->create('user_category', $category);
    if ('POST' == $request->getMethod()) {
      $form->submit($request); // This will update $category
      if ($form->isValid()) {
        // Save to the database
        try {
          $user_repo->saveTags($user_id, $categories);
          return $this->redirect($this->router->generate('CK_edit_settings'));
        } catch (ValidatorException $ex) {
          $error = new FormError($ex->getMessage());
          $form->addError($error);
        }
      }
      // After the form is "bound", it can not be used in a new round
      // of displaying the form to the user, because information that were
      // provided by the javascript on the page will not be there anymore.
      // The name will be missing in new tags from other sources
      // (they have just the id), and the source will be missing as well.
      // We will just have to reload the tags from the database, and
      // create the form again. (We rather do this extra work when things
      // go wrong, than making an extra deep copy of the form or the category
      // every time we do not need it.)

      // The errors found in the form must be transferred.
      // Only the main form errors are kept here,
      // (we presume they have all bubbled up)
      $errors = $form->getErrors();

      $categories = $user_repo->findCategories($user_id, $group_id, $category_id);
      $category = false;
      foreach($categories as $cat) {
        if ($cat->getId() == $category_id) {
          $category = $cat;
          break;
        }
      }
      $form = $this->formFactory->create('user_category', $category);

      // Add the errors the user did to the new form
      foreach ($errors as $error) {
        $form->addError($error);
      }
    }

    // Set data for the template
    $templData = $this->getMainPageData();
    $templData['form'] = $form->createView();
    $templData['form_action'] = $request->getRequestUri();
    $templData['category_name'] = $category->getName();

    $templData['used_tags'] =
      $user_repo->findUsedTags($user_id, $category_id);

    $template = 'CKBundle:Settings:edit_usertag.html.twig';
    return $this->render($template, $templData);
  }
}
