<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Controller for serving attachments
 */
class AttachmentController extends AbstractController
{
  /**
   * Serve an attachment to the user
   */
  public function indexAction(HttpRequest $httpRequest, $attId)
  {
    if ($redirect = $this->assert_live_session($httpRequest)) {
      return $redirect;
    }
    if (!$this->securityContext->isGranted('ROLE_LEVEL1')) {
      return $this->redirect('logout');
    }

    // Sanitize attId
    $attId = intval(substr($attId,0,12));

    // Get the attachment
    $attachment = $this->em->getRepository('CKBundle:Attachment')->find($attId);

    // Check if the user is allowed to access it
    $noAccess = true;
    if ($attachment) {
      $request = $attachment->getRequest();
      if ($request) {
        // Get the user
        $user = $this->securityContext->getToken()->getUser();
        if ($request->canBeSeenBy($user)) {
          $noAccess = false;
        }
      }
    }

    if ($noAccess || !is_file($attachment->getAbsoluteFilePath())) {
      return $this->NoAttachmentAvailable();
    }

    // Access is ok, go ahead and send the attachment

    /**
       This is the X-Sendfile method
    $headers = array(
      'X-Sendfile'          => $attachment->getAbsoluteFilePath(),
      'Content-Type'        => $attachment->getContentType(),
      'Content-Disposition' => 'attachment; filename="'
        . $attachment->getOrigFileName() . '"'
                     );

    return new Response('', 200, $headers);
    */

    $headers = array('Content-Type' => $attachment->getContentType());
    $response = new BinaryFileResponse($attachment->getAbsoluteFilePath(), 200, $headers);
    $response->setContentDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $attachment->getOrigFileName()
    );
    $response->setAutoEtag();
    return $response;
  }

  /**
   * Make a response when no attachment is available
   */
  protected function NoAttachmentAvailable()
  {
    $response = $this->render('CKBundle:Attachment:notAvailable.html.twig');
    $response->setStatusCode(404, 'Attachment not found');
    return $response;
  }
}
