<?php
namespace FSpires\CommitKeeperBundle\Controller\Commitment;

use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Response;
use FSpires\CommitKeeperBundle\Controller\MainpageController;
use FSpires\CommitKeeperBundle\Model\StateMachineInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Model\RequestTableInterface;
use FSpires\CommitKeeperBundle\Model\Enum\Action;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Form\Util\FormViewUtil;
use FSpires\CommitKeeperBundle\Model\Selection\Supporting;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class Show extends MainpageController {
  private $formFactory;
  protected $stateMachine;
  private $dateFormat;
  private $translator;
  private $reqTable;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              StateMachineInterface $stateMachine,
                              DateFormatInterface $dateFormat,
                              TranslatorInterface $translator,
                              RequestTableInterface $reqTable
                              )
  {
    $this->formFactory = $formFactory;
    $this->stateMachine = $stateMachine;
    $this->dateFormat = $dateFormat;
    $this->translator = $translator;
    $this->reqTable = $reqTable;
  }

  /**
   * Show a commitment, and accept next phase if any
   *
   * @param HttpRequest $httpRequest The http request object
   * @param string $userStr
   * @param string $commitmentIdStr The commitment id
   * @throws AccessDeniedException
   * @return RedirectResponse|Response
   */
  public function indexAction(HttpRequest $httpRequest, $userStr, $commitmentIdStr) {
    if (!$this->securityContext->isGranted(
         new Expression("hasRole('ROLE_TYPE3')")
       )) {
      if ($this->securityContext->isGranted(
           new Expression("hasRole('ROLE_TYPE1')")
         )) {
        $cid = $this->session->get('loginCommitmentId',false);
        if (!$cid || $cid!=$commitmentIdStr) {
          $routeName = 'CK_commitment_id';
          $args = array('commitmentIdStr'=>$commitmentIdStr);
          $returnUrl = $this->router->generate($routeName, $args);
          $this->session->set('returnFromConfirmUrl', $returnUrl);
          return $this->redirect($this->router->generate('CK_confirm'));
        }
      } else {
        throw new AccessDeniedException();
      }
    }

    // Set the current user
    try {
      $this->initUserId($userStr);
    } catch (AccessDeniedException $ex) {
      $thisUrl = $this->router->generate('CK_commitment_id', array('commitmentIdStr'=>$commitmentIdStr));
      return $this->logoutToAnotherUser($thisUrl);
    }

    // Get the commitment object
    $req_id = $this->parseReqIdStr($commitmentIdStr);
    $req = null;
    if ($req_id) {
      // Get the commitment from the database
      $req = $this->em->getRepository('CKBundle:Request')->find($req_id);
    }

    $noAccess = false;
    if ($req) {
      // Get the current user
      $user = $this->getUser();
      if (!$req->canBeSeenBy($user)) {
        $noAccess = true;
      }
    }

    // Make sure we have some data to show
    if ($noAccess or !$req) {
      // No data to display
      // Show a main page, but no commitment data
      $template = 'CKBundle:Commitment:nodatamain.html.twig';
      $templData = $this->getMainPageData();
      $templData['reqId'] = $req_id;
      $templData['noAccess'] = $noAccess;

      return $this->render($template, $templData);
    }

    // Set the entity manager on the request so the tags are loaded correctly
    $req->setEntityManager($this->em);
    $req->setGroupId($user->getGroup()->getId());

    // Mark request as read
    $req->getRepository()->markReadByUser($req->getId(), $user->getId());

    // Get list of allowed actions
    $actions = $this->stateMachine->getActionsForReq($req, $user);
    $getLabelFunc = function($act) {return $act->getLabel();};
    $action_choices = array_map($getLabelFunc, $actions);

    //Set some default values on the commitment
    $req->setNewDueDate($req->getDueDate());
    $req->setNewBudget($req->getBudget());
    $req->setNewAttachments(array());
    $canEdit = false;
    $actorRole = '';
    if (count($actions)>0) {
      $commentAction = end($actions);
      $req->setAction(key($actions));
      $actorRole = $commentAction->getActorRole();
      if ($actorRole!='O') {
        $canEdit = true;
      }
    }

    $canAddSupportingRequest = $canEdit;
    if ('P' != $actorRole) {
      $canAddSupportingRequest = false;
    }

    $canEditCategoryValues = $canEdit;
    if ($user->getId() != $req->getInitiator()->getId()) {
      $canEditCategoryValues = false;
    }

    if (Phase::Closed == $req->getPhase()) {
      $canAddSupportingRequest = false;
      $canEditCategoryValues = false;
    }

    $superReqId = $req->getSuperRequestId();
    if ($superReqId) {
      // This is a supporting request
      $superReq = $this->em->getRepository('CKBundle:Request')
                           ->find($superReqId);
      if (!$superReq) {
        return new Response($this->translator->trans('parent.supporting.notFound'), 404);
      }
      $canEditCategoryValues = false;
    }

    // Get catagories and hasBudgetField
    $userCategories = null;
    if ($canEditCategoryValues) {
      $userCategories = $user->getCategories($this->em);

    }
    $hasBudgetField = $req->hasBudgetField();

    // Make observers variables ready
    $observersFixed = $req->getParentObservers();
    $observerChoiceList = null;
    $canEditObservers = $canEdit;
    if ($canEdit) {
      $observerChoiceList = $user->getObserverChoiceList($req, $this->em);
      $canEditObservers = (count($observerChoiceList->getChoices()) > 0);
    }

    // Check if status updates are allowed
    $HasStatusUpdate = false;
    $statusUpdateChoices = false;
    $trafficLight = $req->getTrafficLight();
    if ($canEdit && $req->getPhase() == Phase::Delivery &&
        $req->getPerformer()->getId() == $user->getId()) {
      $req->setStatusUpdate($trafficLight->getId());
      $statusUpdateChoices = $trafficLight->getSelectArray();
      $HasStatusUpdate = true;
    }

    $req->setCCme($user->getCCme());

    // Create form
    $form = $this->formFactory->create('commitment', $req,
                       array('action_choices'=>$action_choices,
                             'hasBudgetField' => $hasBudgetField,
                             'observersFixed' => $observersFixed,
                             'observerChoiceList' => $observerChoiceList,
                             'userCategories'=>$userCategories,
                             'dateFormat'=>$this->dateFormat,
                             'canEdit'=>$canEdit,
                             'canEditObservers' => $canEditObservers,
                             'canEditCategoryValues' => $canEditCategoryValues,
                             'statusUpdateChoices'=>$statusUpdateChoices));

    $returnUrl = $this->getReturnUrl($user);
    // Do the processPost method to handle a post
    if ($this->processPost($httpRequest, $form, $req, $user,
                           $actions, $canEditCategoryValues,
                           $canEditObservers)) {
      return $this->redirect($returnUrl);
    }

    // Set the template and template data
    $template = 'CKBundle:Commitment:main.html.twig';
    $templData = $this->getMainPageData();

    $templData['form'] = $form->createView();

    // Add the form_action URL route
    $routeName = 'CK_commitment_id_posted';
    $args = array('commitmentIdStr'=>$req_id);
    $templData['form_action'] = $this->router->generate($routeName, $args);

    // the commitment
    $templData['req'] = $req;

    // Any parent request?
    if ($superReqId) {
      $templData['superReqTitle'] = $superReq->getTitle();
      if ($superReq->canBeSeenBy($user)) {
        $templData['superReqUrl'] = $this->router->generate(
               'CK_commitment_id',
               array('commitmentIdStr'=>$superReq->getId())
                                                            );
      } else {
        $templData['superReqUrl'] = false;
      }
    } else {
      $templData['superReqTitle'] = false;
    }

    // the traffic light icon
    $templData['trafficLight'] = $trafficLight;
    $type = 'R';
    if ($req->getPerformer()->getId()==$req->getInitiator()->getId()) {
      $type = 'O';
    }
    $templData['StatusMessage'] = $trafficLight->getStatusMsg($type);
    

    // if anything but the comment can be edited
    $templData['canEdit'] = $canEdit;

    // If observers can be edited
    $templData['canEditObservers'] = $canEditObservers;
    if ($canEditObservers) {
      // New contact ajax form
      $formNewContact = $this->formFactory->create('user_name_input', null,
                                                   array('virtual'  => false,
                                                         'required'=>false 
                                                         ));
      $templData['formNewContact'] = $formNewContact->createView();
      $templData['newContactUrl'] = $this->router->generate('CK_newContact');
    } else {
      $templData['ObserverList'] =  $req->getObserverList($this->translator);
    }

    // if category values can be edited
    $templData['canEditCategoryValues'] = $canEditCategoryValues;

    // if status update choice is displayed or not
    $templData['HasStatusUpdate'] = $HasStatusUpdate;
    $templData['cssDisplayStatusUpdate'] = false;
    if ($HasStatusUpdate) {
      switch($req->getAction()) {
      case Action::Comment:
      case Action::AmendToDo:
      case Action::ReportProgress:
        $templData['cssDisplayStatusUpdate'] = true;
        break;
      }
    }

    // the due date and related stuff
    $DateFormat = $this->dateFormat->getPHPpattern();
    $templData['DateFormat'] = $DateFormat; // Also needed in history

    $date = $req->getDueDate();
    $templData['DateLabel'] = 'DueDate';
    /*
    switch ($req->getPhase()) {
    case Phase::Acknowledge:
    case Phase::Closed:
      $date = $req->getDeliveredDate();
      $templData['DateLabel'] = 'DoneDate';
    }
    */
    $templData['Date'] = $date->format($DateFormat);

    // Intital showing or not showing of date field
    $templData['cssShowDate'] = 'show';
    if ($canEdit) {
      $templData['cssEditDate'] = 'hide';
      switch($req->getAction()) {
      case Action::AmendToDo:
      case Action::Amend:
      case Action::Revise:
        $templData['cssShowDate'] = 'hide';
        $templData['cssEditDate'] = 'show';
        break;
      }

      // jsDatePick data
      $templData += $this->dateFormat
        ->getDatePickerVars($req->getNewDueDate(), 'commitment_newDueDate');
    }

    $templData['hasBudgetField'] = $hasBudgetField;
    if ($hasBudgetField) {
      $templData['budgetUnit'] = $req->getBudgetUnit()->getUnit();
    }

    // Things for the status line above the textarea
    $now = new \DateTime('now');
    $templData['currentDate'] = $now->format($DateFormat);
    $templData['actorRole'] = $actorRole;
    $action = $actions[$req->getAction()];
    $templData['currentAction'] = $action->getLabel();

    // history data
    $templData['history'] = $this->em->getRepository('CKBundle:History')
                                     ->findHistory($req_id);

    // Supporting requests
    $hasSupportingRequests = (count($req->getSupportingRequests()) > 0);
    $templData['hasSupportingRequests'] = $hasSupportingRequests;
    if ($hasSupportingRequests) {
      $selection = new Supporting($req->getRepository(), $req);
      $templData = array_merge($templData, $this->reqTable->getData(
          $user->getId(), $user->getGroup()->getId(),
          $selection, false, 'RT_'));
    }
    $templData['suppUrlPrefix'] = $this->router->generate('CK_commitment').'/';
    $templData['canAddSupportingRequest'] = $canAddSupportingRequest;
    if ($canAddSupportingRequest) {
      $templData['newSuppReqUrl'] =
        $this->router->generate('CK_new_sub_commitment',
                                array('type'=>'request',
                                      'superReqId'=>$req->getId()));
    }

    // Attachments and Links
    if ($canEdit) {
      $prototype = $templData['form']['newAttachments']->vars['prototype'];
      $templData['newAttachmentPrototype'] = FormViewUtil::copy($prototype);
    }

    // Url we return to when form is done
    $templData['returnUrl'] = $returnUrl;

    $templData['HasOnload'] = true;

    // Send the data to the twig template for viewing
    return $this->render($template, $templData);
  }

  /**
   * Process http POST data from the form
   * @return boolean If we want to redirect after the post or not
   */
  protected function processPost(HttpRequest $httpRequest, $form, $req,
                                 $user, $actions, $canEditCategoryValues,
                                 $canEditObservers)
  {
    // This class handles only the GET method
    return false;
  }

  /**
   * Parse the argument we get from the routing to this page
   * @param string $reqIdStr The request id
   * @return integer or false
   */
  private function parseReqIdStr($reqIdStr)
  {
    if ($reqIdStr) {
      $req_id = intval(substr($reqIdStr,0,15));
      if ($req_id>0) {
        return $req_id;
      }
    }
    return false;
  }
}
