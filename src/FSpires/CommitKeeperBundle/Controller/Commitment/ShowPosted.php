<?php
namespace FSpires\CommitKeeperBundle\Controller\Commitment;

use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use FSpires\CommitKeeperBundle\Model\TrafficLightFactoryInterface;
use FSpires\CommitKeeperBundle\Model\StateMachineInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Model\UserFactory as UserFactoryInterface;
use FSpires\CommitKeeperBundle\Model\RequestTableInterface;

class ShowPosted extends Show {
  private $trafficLightFactory;
  private $userFactory;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              StateMachineInterface $stateMachine,
                              DateFormatInterface $dateFormat,
                              TranslatorInterface $translator,
                              RequestTableInterface $reqTable,
                              TrafficLightFactoryInterface $trafficLightFactory,
                              UserFactoryInterface $userFactory)

  {
    parent::__construct($formFactory, $stateMachine, $dateFormat, $translator, $reqTable);
    $this->trafficLightFactory = $trafficLightFactory;
    $this->userFactory = $userFactory;
  }

  /**
   * Process http POST data from the form
   * @return boolean If we want to redirect after the post or not
   */
  protected function processPost(HttpRequest $httpRequest, $form, $req,
                                 $user, $actions, $canEditCategoryValues,
                                 $canEditObservers)
  {
    $form->bind($httpRequest); // This will update the $req object
    if (!$form->isValid()) {
      return false;
    }

    $user->setCCme($req->getCCme());

    // Get the action
    $action = $actions[$req->getAction()];

    // Do the action and the request is saved to the database
    $req->setTrafficLightFactory($this->trafficLightFactory);
    $this->stateMachine->doAction($req, $user, $action);

    if ($canEditCategoryValues) {
      $req->saveTags();
    }

    if ($canEditObservers) {
      $this->userFactory->updateContacts($user, $req);
    }

    return true;
  }
}
