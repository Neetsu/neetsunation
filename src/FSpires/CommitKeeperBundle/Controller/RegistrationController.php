<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use FSpires\CommitKeeperBundle\Model\UserFactory as UserFactoryInterface;
use FSpires\CommitKeeperBundle\Security\JanrainUserToken;
use FSpires\CommitKeeperBundle\Security\JanrainAuthProvider;
use FSpires\CommitKeeperBundle\Entity\AnonymousUser;
use FSpires\CommitKeeperBundle\Entity\User;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

/**
 * Controller for user registration
 */
class RegistrationController extends AbstractController {
  private $formFactory;
  private $userFactory;
  private $janrainAuthProvider;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              UserFactoryInterface $userFactory,
                              JanrainAuthProvider $janrainAuthProvider)
  {
    $this->formFactory = $formFactory;
    $this->userFactory = $userFactory;
    $this->janrainAuthProvider = $janrainAuthProvider;
  }

  /**
   * Main start page with registration form
   */
  public function registerAction(Request $request, $root) {
    if ($this->securityContext->isGranted(
         new Expression("hasRole('ROLE_TYPE1') or hasRole('ROLE_TYPE2')")
       )) {
      return $this->redirect($this->router->generate('CK_confirm'));
    }

    $loggedIn = false;
    if ($this->securityContext->isGranted(
         new Expression("hasRole('ROLE_TYPE3')")
       )) {
      $loggedIn = true;
    }

    if (!$loggedIn) {
      // Make an user object to register any new user with
      $new_user = new User();

      // Set default to true on "keep local password account"
      $new_user->setKeep(true);

      // Get the form used to register new users
      $form = $this->formFactory->create('user_register', $new_user,
                                         array('name_in_label' => false,
                                               'show_openids' => false));
      if ('POST' == $request->getMethod()) {
        if ($request->request->get('pwdsignup')) {
          $nextUrl = $this->router->generate('CK_default');
        } else {
          $this->userFactory->setupFirstLogin($new_user);
          $nextUrl = $this->router->generate('CK_newopenid');
        }
        $form->bind($request);
        if ($form->isValid()) {
          $errorMsg = $this->userFactory->saveAndLogin($new_user, true, true);
          if (!$errorMsg) {
            return $this->redirect($nextUrl);
          }
          $form->addError(new FormError($errorMsg));
        }
      }
      $templData = array(
               'form' => $form->createView(),
               'form_action' => $this->router->generate('CK_register'));
    } else {
      $defaultUrl = $this->router->generate('CK_default');
      if ($root) {
        return $this->redirect($defaultUrl);
      }
      $user = $this->securityContext->getToken()->getUser();
      $templData = array('username' => $user->getName(),
                         'table_url' => $defaultUrl);
    }

    $templData['loggedIn'] = $loggedIn;
    $templData['HasOnload'] = !$loggedIn;

    return $this->render('CKBundle:Registration:register.html.twig', $templData);
  }

  /**
   * Show a page for registering new users
   * or connect to an existing user
   * when we have an unkown openId login
   */
  public function registerOrAddAuthAction(Request $request) {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }
    // Get the anonymous user from the session if there is one
    $anon_user = AnonymousUser::getFromSession($this->session);

    $providerName = false;

    $isPost = 'POST' == $request->getMethod();
    if (!$isPost) {
      // Get the authentication token to get data from
      $token = $this->securityContext->getToken();

      // Check if we have an anonymous opeinid login
      if ($token instanceof JanrainUserToken) {
        $roles = $token->getRoles();
        if (count($roles)>0 && 'ROLE_ANONYMOUS'==$roles[0]->getRole()) {
          $anon_user->addDataFromToken($token);
          $anon_user->saveInSession();

          //Make sure we are logged out for the next page
          $this->securityContext->setToken(null);
          $this->session->remove('_security_' . $token->getProviderKey());

          $openIdData = $token->getOpenIdData();
          $providerName = $openIdData['providerName'];
        }
      }
    }

    // Make an user object to register the new user with
    $new_user = new User();
    $anon_user->copyToUser($new_user);

    // If we have no openId prividers, just go to ordinary register page
    if (0==count($new_user->getOpenIdentifier())) {
      return $this->redirect($this->router->generate('CK_register'));
    }

    $oid = $new_user->getOpenIdentifier()->last();
    $providerId = $oid->getProvider();
    if (!$providerName) {
      $posColon = strpos($providerId,':');
      if ($posColon) {
        $providerName = substr($providerId, 0, $posColon);
      } else {
        $providerName = $providerId;
      }
    }

    // Get the form used to register new users
    $form = $this->formFactory->create('user_name_input', $new_user,
          array('virtual' => false,
                'data_class' => 'FSpires\CommitKeeperBundle\Entity\User'));
    if ($isPost) {
      $form->bind($request); // This will update the $new_user object
      if ($form->isValid()) {
        $errorMsg = $this->userFactory->saveAndLogin($new_user);
        if (!$errorMsg) {
          //Remove the anonymous user from the session
          $anon_user->removeFromSession();
          return $this->redirect($this->getReturnUrl($new_user));
        }
        $form->addError(new FormError($errorMsg));
      }
    }

    // Set data for the template
    $templData = array();
    $templData['form_action'] = $this->router->generate('CK_register_addauth');
    $templData['providerName'] = $providerName;
    $templData['providerId'] = $providerId;
    $email_list = $anon_user->getEmails();
    if (count($email_list)>0) {
      $templData['ac_email_list'] = json_encode($email_list);
      $ac_options = array('minChars'=>1,
                          'delay'=>5,
                          'autoFill'=>true,
                          'selectFirst'=>true);
      $ac_options = json_encode($ac_options, JSON_FORCE_OBJECT);
      $templData['ac_options'] = $ac_options;
    }
    $templData['form'] = $form->createView();

    $template = 'CKBundle:Registration:register_newopenid.html.twig';
    return $this->render($template, $templData);
  }

  /**
   * Confirm a newly created user
   * Present a form to ask for a password to be set or an OpenId
   * login provider to assosciate with this user
   */
  public function confirmAction(Request $request)
  {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }

    $template = 'CKBundle:Registration:confirm.html.twig';

    $returnUrl = $this->session->get('returnFromConfirmUrl', false);
    if (!$returnUrl) {
      $sTargetPath = '_security.secured_area.target_path';
      $returnUrl = $this->session->get($sTargetPath, false);
      if ($returnUrl) {
        $this->session->remove($sTargetPath);
        $this->session->set('returnFromConfirmUrl', $returnUrl);
      } else {
        $returnUrl = $this->router->generate('CK_default');
      }
    }

    return $this->inputAuthenticationMethod($request, $template, $returnUrl);
  }

  /**
   * Register a new OpenId provider with the user
   * (the user has just registered on the previous screen)
   */
  public function newOpenIdAction(Request $request)
  {
    if ($redirect = $this->assert_live_session($request)) {
      return $redirect;
    }

    $template = 'CKBundle:Registration:newopenid.html.twig';
    $returnUrl = $this->router->generate('CK_default');
    return $this->inputAuthenticationMethod($request, $template, $returnUrl);
  }

  /**
   * Add a new OpenId provider to the user account
   * or change/set the password
   */
  public function addOpenIdAction(Request $request)
  {
    // The access control is taken care of in app/config/security.yml
    $template = 'CKBundle:Registration:addopenid.html.twig';
    $returnUrl = $this->getReturnUrl(
                    $this->securityContext->getToken()->getUser());
    return $this->inputAuthenticationMethod($request, $template, $returnUrl, false);
  }

  /**
   * Present a form to ask for a password to be set or
   * an OpenId login provider to assosciate with this user
   */
  private function inputAuthenticationMethod(Request $request, $template, $returnUrl, $doLogin=true)
  {
    $hasAuthMethod = false;
    $oidErrMsg = false;
    $jtoken = false;

    // Get the user
    $user = $this->securityContext->getToken()->getUser();

    $isPost = 'POST'==$request->getMethod();
    if ($isPost) {
      $jtoken = $request->request->get('token');
      if (strlen($jtoken) == 40) {
        $janrainToken = new JanrainUserToken($jtoken, 'secured_area');
        try {
          $janrainToken = $this->janrainAuthProvider->authenticate($janrainToken);
          $roles = $janrainToken->getRoles();
          if (count($roles)>0 && $roles[0]->getRole() == 'ROLE_ANONYMOUS') {
            $anonUser = new AnonymousUser(null);
            $anonUser->addDataFromToken($janrainToken);
            foreach ($anonUser->getOpenIdentifier() as $openId) {
              $user->addOpenIdentifier($openId);
              $hasAuthMethod = true;
            }
            if ($hasAuthMethod && $user->getUserType() < 3) {
              $user->setKeep(false);
            }
          } else {
            $oidErrMsg = array('openId.alreadyAssigned%user%', array('%user%' => $janrainToken->getUsername()));
          }
        } catch (\Exception $ex) {
          $oidErrMsg = $ex->getMessage();
        }
      }
    }

    if (!$hasAuthMethod) {
      $form = $this->formFactory->create('password_input', null,
                                         array('required'=>true));
      if ($isPost && !$jtoken) {
        $form->bind($request);
        if ($form->isValid()) {
          $formData = $form->getData();
          $user->setCtPassword($formData['password']);
          $user->setKeep(true);

          $hasAuthMethod = true;
        }
      }
    }

    if ($hasAuthMethod) {
      if ($doLogin) {
        $this->userFactory->saveAndLogin($user, false);
      } else {
        $this->userFactory->save($user);
      }

      return $this->redirect($returnUrl);
    }

    $templData = array();
    $templData['form'] = $form->createView();
    $templData['user'] = $user;
    $templData['oidErrMsg'] = $oidErrMsg;
    $templData['this_url'] = $request->getUri();

    return $this->render($template, $templData);
  }
}
