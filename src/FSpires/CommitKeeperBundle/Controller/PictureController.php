<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for serving pictures
 */
class PictureController extends AbstractController {

  /**
   * Serve a picture file
   */
  public function indexAction(HttpRequest $httpRequest, $pictureId)
  {
    if ($redirect = $this->assert_live_session($httpRequest)) {
      return $redirect;
    }
    if (!$this->securityContext->isGranted('ROLE_LEVEL1')) {
      return $this->redirect('logout');
    }

    // Sanitize pictureId
    $pictureId = intval(substr($pictureId,0,12));

    // Get the picture
    $picture = $this->em->getRepository('CKBundle:Picture')->find($pictureId);

    if (!$picture || !is_file($picture->getAbsoluteFilePath())) {
      return $this->NoPictureAvailable();
    }

    // Access is ok, go ahead and send the picture

    /**
       This is the X-Sendfile method
    $headers = array(
      'X-Sendfile'          => $picture->getAbsoluteFilePath(),
      'Content-Type'        => $picture->getContentType(),
      'Content-Disposition' => 'picture; filename="'
        . $picture->getOrigFileName() . '"'
                     );

    return new Response('', 200, $headers);
    */

    $headers = array('Content-Type' => $picture->getContentType());
    $response = new BinaryFileResponse($picture->getAbsoluteFilePath(), 200, $headers);
    $response->setAutoEtag();
    return $response;
  }

  /**
   * Make a response when no picture is available
   */
  protected function NoPictureAvailable()
  {
    $response = new Response();
    $response->setStatusCode(404, 'Picture not found');
    return $response;
  }
}
