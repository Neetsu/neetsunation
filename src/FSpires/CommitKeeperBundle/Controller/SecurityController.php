<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolverInterface;
use FSpires\CommitKeeperBundle\Entity\AnonymousUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Cookie;
use JMS\SecurityExtraBundle\Security\Authorization\Expression\Expression;

class SecurityController extends AbstractController {
  private $formFactory;
  private $csrf_provider;
  private $authTrustResolv;
  private $logoutlistner;
  private $kernel;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              CsrfTokenManagerInterface $csrf_provider,
                              AuthenticationTrustResolverInterface $atr,
                              ListenerInterface $logoutlistner,
                              HttpKernelInterface $kernel)
  {
    $this->formFactory = $formFactory;
    $this->csrf_provider = $csrf_provider;
    $this->authTrustResolv = $atr;
    $this->logoutlistner = $logoutlistner;
    $this->kernel = $kernel;
  }

  /**
   * The user wants a clean login page
   */
  public function cleanLoginAction()
  {
    $anon_user = AnonymousUser::getFromSession($this->session);
    $anon_user->removeFromSession();
    return $this->redirect($this->router->generate('login'));
  }

  /**
   * Show the login page
   */
  public function loginAction(Request $request, $debug)
  {
    $isRememberMe = false; // If this is a validation of a "remember me"
    $default_username = $this->session->get(SecurityContext::LAST_USERNAME);
    $token = $this->securityContext->getToken();
    if ($token) {
      $access = true;
      foreach ($token->getRoles() as $roleObj) {
        $role = $roleObj->getRole();
        if ('ROLE_TYPE1' == $role || 'ROLE_TYPE2' == $role) {
          return $this->redirect($this->router->generate('CK_confirm'));
        }
        if ('ROLE_ANONYMOUS' == $role) {
          $access = false;
          break;
        }
      }
      if ($access) {
        if ($this->authTrustResolv->isFullFledged($token)) {
          return $this->redirect($this->getReturnUrl($token->getUser()));
        }
        if ($this->authTrustResolv->isRememberMe($token)) {
          $isRememberMe = true;
        }
      }
      if (!$this->authTrustResolv->isAnonymous($token)) {
        $default_username = $token->getUsername();
      }
    }

    // get the login error if there is one
    if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
      $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
    } else {
      $error = $this->session->get(SecurityContext::AUTHENTICATION_ERROR);
      $this->session->remove(SecurityContext::AUTHENTICATION_ERROR);
    }
    if ($error && $error->getMessage()=='WrongEmailUser') {
      $error = false;
    }

    $csrfToken = $this->csrf_provider->getToken('the_login_form');

    $login_check_url = $this->router->generate('login_check', array(), true);
    $templData = array(
            'login_check_url'  => $login_check_url,
            'default_username' => $default_username,
            'isRememberMe'   => $isRememberMe,
            'error'         => $error,
            'csrf_token'    => $csrfToken
            );

    $anon_user = AnonymousUser::getFromSession($this->session);
    $templData['openIds'] = $anon_user->getOpenIdentifier();

    $template = 'CKBundle:Security:login.html.twig';
    if ($debug) {
      $template = 'CKBundle:Security:login_debug.html.twig';
      $templData['users'] = $this->em->getRepository('CKBundle:UserName')->findAll();
    }

    $response = $this->render($template, $templData);
    if (count($templData['openIds'])>0) {
      // Remove janrain cookies expected_tab and login_tab
      $past = 1000000;
      $response->headers->setCookie(new Cookie('expected_tab', 'x', $past));
      $response->headers->setCookie(new Cookie('login_tab', 'x', $past));
    }

    //Make sure the login page is not cached
    return $this->makeResponseNoCache($response);
  }

  /**
   * Access denied to some page
   */
  public function accessDeniedAction()
  {
    if ($this->securityContext->isGranted(
         new Expression("hasRole('ROLE_TYPE1') or hasRole('ROLE_TYPE2')")
                                          )) {
      return $this->redirect($this->router->generate('CK_confirm'));
    }
    $logout_req = Request::create('/logout');
    $logout_req->setSession($this->session);
    $event = new GetResponseEvent($this->kernel, $logout_req, HttpKernelInterface::MASTER_REQUEST);
    $this->logoutlistner->handle($event);
    $response = $event->getResponse();
    $response->headers->remove('Location');
    return $this->render('CKBundle:Security:accessdenied.html.twig', array(),
                         $response);
  }
}
