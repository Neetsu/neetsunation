<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use IntlDateFormatter, Locale;

/**
 * A class that gives the date format to be used
 * in all of the the application 
 */
class DateFormat implements DateFormatInterface
{
  private $fmt;
  private $IDFformat;
  private $icuArray;
  private $phpArray;
  private $datePHPpattern;
  private $dateICUpattern;
  private $dateDPpattern;

  public function __construct()
  {
    $this->IDFformat = IntlDateFormatter::MEDIUM;
    $this->fmt = new IntlDateFormatter(
                     Locale::getDefault(),
                     $this->IDFformat,
                     IntlDateFormatter::NONE,
                     'UTC',
                     IntlDateFormatter::GREGORIAN
                     );

    $this->icuArray = array(
              'mm','m',
              'YYYY','YYY','YY','Y',
              'yyyy','yyy','yy','y','pf','ps',
              'L','MMMMM','MMMM','MMM','MM','M','pl','pf','ps',
              'dd','d','p',
              'DDD','DD','D',
              'e','c','EEEEEE','EEEEE','EEEE','EEE','EE','E',
              'hh','h','p',
              'HH','H','p',
              'a'
                            );

    $this->phpArray = array(
              'i','i',
              'o','o','o','o',
              'pf','pf','ps','Y','Y','y',
              'M','pl','pf','ps','m','n','M','F','M',
              'p','j', 'd',
              'z','z','z',
              'E','E','D','D','l','D','D','D',
              'p','g','h',
              'p','G','H',
              'A'
                            );
    $this->dpArray = array(
              'x','x',
              'pf','pf','ps','pf',
              'pf','pf','ps','yy','yy','y',
              'M','pl','pf','ps','mm','m','M','MM','M',
              'p','d','dd',
              'oo','o','o',
              'E','E','D','D','DD','D','D','D',
              'hh','h','p',
              'HH','H','p',
              'A'
                           );


    $this->dateICUpattern = false;
    $this->datePHPpattern = false;
    $this->dateDPpattern  = false;;
  }

  /**
   * Get the IntlDateFormatter date format
   */
  public function getIDFformat()
  {
    return $this->IDFformat;
  }

  /**
   * Get a ICU type (http://userguide.icu-project.org/formatparse/datetime)
   * date pattern
   */
  public function getICUpattern()
  {
    if (!$this->dateICUpattern) {
      $this->dateICUpattern = $this->fmt->getPattern();
    }
    return $this->dateICUpattern;
  }

  /**
   * Get a PHP date type date pattern
   */
  public function getPHPpattern()
  {
    if (!$this->datePHPpattern) {
      $this->datePHPpattern = str_replace($this->icuArray,
                                          $this->phpArray,
                                          $this->getICUpattern());
    }
    return $this->datePHPpattern;
  }


  /**
   * Get a date format for DatePicker
   */
  public function getDPpattern()
  {
    if (!$this->dateDPpattern) {
      $this->dateDPpattern = str_replace($this->icuArray,
                                         $this->dpArray,
                                         $this->getICUpattern());
    }
    return $this->dateDPpattern;
  }

  /**
   * Get template variables for the jQueryUi datePicker
   */
  public function getDatePickerVars($date, $id)
  {
    return array('dpDateFormat' => $this->getDPpattern(),
                 'id_date_target' => $id);
  }
}
