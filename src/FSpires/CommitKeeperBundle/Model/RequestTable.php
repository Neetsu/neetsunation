<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Model\Selection\SelectionInterface;

/**
 * A table of requests, displayed in the upper half of the screen
 * This object will provide all the data necessary to create such a table
 * with a view.
 */
class RequestTable extends ModelUsingEntityManager
                   implements RequestTableInterface
{
  /**
   * Get data to display a table of requests
   * @param int         $userId     Id of the user the table is for
   * @param int         $groupId    Id of the group the user is in,
   *                                for sorting categories of tags
   * @param SelectionInterface $selection  Get only requests from this selection
   * @param bool|string $sortcol    Initial sorting by this column
   * @param string      $prefix     Prefix in the keys in the returned array
   * @return array
   */
  public function getData($userId, $groupId, SelectionInterface $selection,
                          $sortcol=false, $prefix='') {
    $orderby = null;
    if ($sortcol) {
      if (is_array($sortcol)) {
        $orderby = $sortcol;
      } else if (is_string($sortcol)) {
        $orderby = array($sortcol => 'ASC');
      }
    } else {
      $orderby = array('trafficLight' => 'ASC',
                       'dueDate' => 'ASC',
                       'title' => 'ASC');
    }

    $data = array(
                  $prefix.'NumItems' => 0,
                  $prefix.'DispRequestor' => true,
                  $prefix.'DispPerformer' => true,
                  );

    $data = $selection->getTemplateVars($prefix) + $data;
    $table = $selection->getCommitments($userId, $orderby);
    $data[$prefix.'Table'] = $table;
    if (is_array($table)) {
      $data[$prefix.'NumItems'] =  count($table);
    }

    if (isset($groupId)) {
      $data[$prefix.'Categories'] = $selection->getCategories($userId, $groupId);
    }
    $data[$prefix.'Unread'] = $selection->getUnread($userId);

    return $data;
  }
}
