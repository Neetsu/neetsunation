<?php
namespace FSpires\CommitKeeperBundle\Model;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Model\Action\ActionInterface;

/**
 * The interface for the request state machinery
 */
interface StateMachineInterface
{
  /**
   * Get a list of allowed actions an user can do on a request
   * @param string $phase     The current phase of the request
   * @param string $pendingBy 'R' or 'P', Who is the request pending by
   * @param string $actor     'R' or 'P', Who is doing the action
   * $param string $reqType   'request', 'offer' or 'toDo', What type of request
   * @return array An array of action objects
   */
  public function getActionsAllowed($phase, $pendingBy, $actor, $reqType);

  /**
   * An user makes an action on a request
   * The request is modified and saved in the database,
   * Other modifiactions to the database is also done.
   * (History is updated etc.)

   * @param Request $req The request that will be updated
   * @param User $user The user that performs the action
   * @param $action The action (An action object)
   */
  public function doAction(Request $req, User $user, ActionInterface $action);
}
