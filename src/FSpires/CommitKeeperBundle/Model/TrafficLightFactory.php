<?php
namespace FSpires\CommitKeeperBundle\Model;

class TrafficLightFactory extends ModelUsingEntityManager
                          implements TrafficLightFactoryInterface
{
  public function getTrafficLight($trafficLightId)
  {
    $tl = $this->em->getRepository('CKBundle:TrafficLight')
                    ->find($trafficLightId);
    if (!$tl) {
      throw new \InvalidArgumentException('Traffic light "'.
                                          $trafficLightId . '" not found!');
    }
    return $tl;
  }
}
