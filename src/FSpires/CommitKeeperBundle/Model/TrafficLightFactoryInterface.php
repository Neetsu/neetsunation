<?php
namespace FSpires\CommitKeeperBundle\Model;

interface TrafficLightFactoryInterface
{
  public function getTrafficLight($trafficLightId);
}
