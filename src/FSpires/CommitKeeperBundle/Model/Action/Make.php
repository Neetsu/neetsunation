<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\RequestType;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

class Make extends Action
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    //Update the due date and the budget
    $request->updateDueDate();
    $request->updateBudget();

    // Set the next phase and the traffic light
    // and set who it is pending by next
    switch ($this->reqType) {
    case RequestType::Request:
      $request->setPhase(Phase::Negotiation);
      $request->setTrafficLight(TL::Negotiation);
      $request->setPhasePendingBy('P');
      break;
    case RequestType::Offer:
      $request->setPhase(Phase::Negotiation);
      $request->setTrafficLight(TL::Negotiation);
      $request->setPhasePendingBy('R');
      break;
    case RequestType::ToDo:
      $request->setPhase(Phase::Delivery);
      $request->setTrafficLight(TL::OnTrack);
      $request->setPhasePendingBy('P');
      break;
    default:
      throw new \UnexpectedValueException(
         'Unexpected value set for the "request type": ' . $this->reqType);
    }
  }
}
