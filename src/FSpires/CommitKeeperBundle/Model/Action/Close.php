<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * This action class covers Cancel, Decline and Revoke actions
 */
class Close extends Action
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    // Set the next phase and the traffic light
    // and set who it is pending by next
    $this->setTrafficLight($request);
    $request->setPhase(Phase::Closed);
    $request->setPhasePendingBy('');
  }

  protected function setTrafficLight($request) {
    if (RequestType::ToDo==$this->reqType) {
      $trLight = TL::CanceledToDo;
    } else {
      if (RequestType::Offer==$this->reqType) {
        // cancel Offer
        if ($request->getPhase()!=Phase::Negotiation) {
          $trLight = TL::CanceledStartedOffer;
        } else {
          if ($this->actor == 'P') {
            $trLight = TL::CanceledOffer;
          } else {
            $trLight = TL::CanceledCounteredOffer;
          }
        }
      } else {
        // cancel Request
        if ($request->getPhase()!=Phase::Negotiation) {
          $trLight = TL::CanceledStartedRequest;
        } else {
          if ($this->actor == 'R') {
            $trLight = TL::CanceledRequest;
          } else {
            $trLight = TL::CanceledCounteredRequest;
          }
        }
      }
    }
    $request->setTrafficLight($trLight);
  }
}
