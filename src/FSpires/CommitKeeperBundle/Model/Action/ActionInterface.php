<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

interface ActionInterface
{

  /**
   * Get the label for the action used in the UI
   * This label will be translated later
   */
  public function getLabel();

  /**
   * Get the role of the actor in this action
   * Either 'R', 'P', or 'O'.
   */
  public function getActorRole();

  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request);
}
