<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\RequestType;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

class Decline extends Close
{
  protected function setTrafficLight($request) {
    if (RequestType::Offer==$this->reqType) {
      // decline Offer
      if ($this->actor == 'R') {
        $trLight = TL::DeclinedOffer;
      } else {
        $trLight = TL::DeclinedCounteredOffer;
      }
    } else {
      // decline Request
      if ($this->actor == 'P') {
        $trLight = TL::DeclinedRequest;
      } else {
        $trLight = TL::DeclinedCounteredRequest;
      }
    }
    $request->setTrafficLight($trLight);
  }
}
