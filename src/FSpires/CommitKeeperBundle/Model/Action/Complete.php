<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

class Complete extends Close
{
  protected function setTrafficLight($request) {
    $request->setTrafficLight(TL::Completed);
  }
}
