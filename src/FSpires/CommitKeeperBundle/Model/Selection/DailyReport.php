<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class DailyReport extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlDailyReport();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findDailyReport($userId, $orderby);
  }
}
