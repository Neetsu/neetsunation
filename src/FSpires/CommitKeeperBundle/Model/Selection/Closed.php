<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class Closed extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlClosed();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findClosed($userId, $orderby);
  }
}
