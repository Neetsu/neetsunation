<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class Observed extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlByObserver();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findByObserver($userId, $orderby);
  }
}
