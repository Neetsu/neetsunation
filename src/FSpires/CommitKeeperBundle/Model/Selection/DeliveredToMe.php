<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class DeliveredToMe extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlByRequestor();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findByRequestor($userId, $orderby);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateVars($prefix) {
    return array($prefix.'DispRequestor' => false);
  }
}
