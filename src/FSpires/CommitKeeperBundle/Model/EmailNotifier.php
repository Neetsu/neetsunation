<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Entity\UserBase;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Entity\History;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Model\Enum\Action;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;
use FSpires\CommitKeeperBundle\Model\Selection\DailyReport;
use Symfony\Component\Templating\EngineInterface as TemplatingInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use Swift_Mailer, Swift_Message, Swift_Image;

/**
 * This class is responsible for email notifications
 */
class EmailNotifier implements EmailNotifierInterface
{
  private $mailer;
  private $templating;
  private $em;
  private $router;
  private $translator;

  /**
   * @var DateFormatInterface
   */
  private $dateFormat;

  private $returnPath;
  private $replyTo;
  private $systemTo;
  private $unSubscribeAddress;

  private $fromUserId;
  private $toUsers;
  private $toNewUsers;

  private $from;
  private $subject;
  private $subjectPrefix;
  private $version;
  private $webDir;

  /**
   * Constructor
   *
   * @param Swift_Mailer $mailer Mailer
   * @param TemplatingInterface $templating Template service
   * @param EntityManagerInterface $em
   * @param RouterInterface $router Url router service
   * @param TranslatorInterface $translator Translation service
   * @param DateFormatInterface $dateFormat Dateformat service
   * @param string $returnPath E.g. 'bounce@example.com'
   * @param string $replyTo E.g. 'noreply@example.com'
   * @param string $systemTo System notification address
   * @param string $unSubscribeAddress
   * @param string $subjectPrefix
   * @param array $version Version information
   * @param string $kernelRootDir
   */
  public function __construct(
    Swift_Mailer $mailer,
    TemplatingInterface $templating,
    EntityManagerInterface $em,
    RouterInterface $router,
    TranslatorInterface $translator,
    DateFormatInterface $dateFormat,
    $returnPath,
    $replyTo,
    $systemTo,
    $unSubscribeAddress,
    $subjectPrefix,
    array $version,
    $kernelRootDir
  ) {
    $this->mailer = $mailer;
    $this->templating = $templating;
    $this->em = $em;
    $this->router = $router;
    $this->translator = $translator;
    $this->dateFormat = $dateFormat;
    $this->returnPath = $returnPath;
    $this->replyTo = $replyTo;
    $this->systemTo = $systemTo;
    $this->unSubscribeAddress = $unSubscribeAddress;
    $this->subjectPrefix = $subjectPrefix;
    $this->version = $version;
    $this->webDir = $kernelRootDir . '/../web/';

    $this->imageCache = array();
  }

  /**
   * Reset all mail related class variables,
   * ready for a new round of sending mail
   */
  private function startNewMail()
  {
    $this->from = $this->replyTo;
    $this->fromUserId = null;
    $this->toUsers = array();
    $this->toNewUsers = array();
    $this->subject = '';
  }

  /**
   * Add a user to the $this->toUsers array
   * @param UserBase $user User to add
   */
  private function addRecipient(UserBase $user)
  {
    if ($user->getId() != $this->fromUserId) {
      $this->toUsers[$user->getId()] = $user;
    }
  }

  /**
   * Split toUsers into toNewUsers and toUsers
   */
  private function splitRecipients()
  {
    $this->toNewUsers = $this->em->getRepository('CKBundle:User')
                        ->findNewUsers(array_keys($this->toUsers));
    foreach ($this->toNewUsers as $newUser) {
      unset($this->toUsers[$newUser->getId()]);
    }
  }

  /**
   * Get URL for new user to login and view the request
   */
  private function getUrlForNewUser(User $newUser, $reqId=false)
  {
    $url = $this->router->generate('login_check', array(), true);
    $url .= '?' . $newUser->getNewUserURLpart();
    if ($reqId) {
      $url .= '&c=' . $reqId;
    }
    return $url;
  }

  /**
   * Start a new email based on the class variables
   * @return Swift_Message new email message instance
   */
  private function getMessageInstance()
  {
    $email = Swift_Message::newInstance($this->subject);
    $email->setReturnPath($this->returnPath);
    $email->setSender($this->returnPath);
    $email->setReplyTo($this->replyTo);
    $email->setFrom($this->from);
    return $email;
  }

  /**
   * Generate action string to put in subject
   *
   * @param $act
   * @return string Part of subject line
   */
  private function filterActionString($act) {
    $pos = strlen(Action::Make) + 1;
    if (substr($act, 0, $pos) != (Action::Make.'.')) {
      return '';
    }
    $reqType = substr($act, $pos);
    return '[' .
      $this->translator->trans('Email.' . $reqType, array(), 'history') . ']';
  }

  /**
   * Notify by email the other users about the action taken.
   * Main method, called after someone has done an action.
   */
  public function notify(UserBase $user, Request $req, History $hist)
  {
    // Reset variables
    $this->startNewMail();

    // Find out who to send to and send from
    $this->fromUserId = $user->getId();
    $this->from = $user->getEmailArray();
    if ($user->getCCme()) {
      $this->toUsers = array($user->getId() => $user);
    }

    $this->addRecipient($req->getPerformer());
    $this->addRecipient($req->getRequestor());
    foreach ($req->getObservers() as $observer) {
      $this->addRecipient($observer);
    }

    $this->splitRecipients();

    $numEmails = count($this->toUsers);
    $numNewUsers = count($this->toNewUsers);

    if (($numEmails+$numNewUsers) < 1) {
      return;
    }

    // Mail subject
    $this->subject = $this->subjectPrefix . $this->filterActionString($hist->getActionStr())
      . ' ' . $req->getTitle();

    // Generate the template data for the message body
    $templData = array('req' => $req, 'hist' => $hist);

    // Add un-subscribe email
    $templData['unSubscribeAddress'] = $this->unSubscribeAddress;

    // Get version info to display
    $templData['version'] = $this->version;

    // The date format is needed
    $templData['DateFormat'] = $this->dateFormat->getPHPpattern();

    // Set list of observers
    $templData['observers'] = $req->getObserverList($this->translator);

    // Budget field
    $hasBudgetField = $req->hasBudgetField();
    $templData['hasBudgetField'] = $hasBudgetField;
    if ($hasBudgetField) {
      $templData['budgetUnit'] = $req->getBudgetUnit()->getUnit();
    }

    $reqId = $req->getId();

    if ($numEmails > 0) {
      foreach ($this->toUsers as $toUserId => $toUser) {
        $email = $this->getMessageInstance();
        $email->setTo($toUser->getEmailArray());

        // Get the respond image that we want to embed in the email
        $respondImage = Swift_Image::fromPath($this->webDir . 'images/respond.png');

        // Make URL so the mail can link to the show commitment web-page
        $routeName = 'CK_commitment_user_id';
        $args = array(
          'userStr' => 'user' . $toUserId,
          'commitmentIdStr' => $reqId
        );
        $templData['showUrl'] = $this->router->generate($routeName, $args, true);

        $templData['replyImage'] = $email->embed($respondImage);

        $email->addPart($this->templating->render('CKBundle:Email:email.txt.twig', $templData), 'text/plain');
        $email->setBody($this->templating->render('CKBundle:Email:email.html.twig', $templData), 'text/html');
        $this->mailer->send($email);
      }
    }

    foreach ($this->toNewUsers as $newUser) {
      $email = $this->getMessageInstance();
      $email->setTo($newUser->getEmailArray());

      // Get the respond image that we want to embed in the email
      $respondImage = Swift_Image::fromPath($this->webDir . 'images/respond.png');

      $templData['showUrl'] = $this->getUrlForNewUser($newUser, $reqId);
      $templData['replyImage'] = $email->embed($respondImage);

      $email->addPart($this->templating->render('CKBundle:Email:email.txt.twig', $templData), 'text/plain');
      $email->setBody($this->templating->render('CKBundle:Email:email.html.twig', $templData), 'text/html');
      $this->mailer->send($email);
    }
  }

  /**
   * Send email to a new user that has just been created.
   */
  public function newUser(User $newUser, UserBase $creator)
  {
    $this->subject = $this->subjectPrefix . ' ' . $this->translator->trans('subject.welcome');

    // Generate the template data for the message body
    $templData = array('newUser' => $newUser,
                       'creator' => $creator);

    /* Not in current welcome email
    $templData['loginUrl'] = $this->router->generate('login', array(), true);
    $templData['showUrl'] = $this->getUrlForUser($newUser);
    */

    // Get version info to display
    $templData['version'] = $this->version;

    $this->from = $creator->getEmailArray();
    $email = $this->getMessageInstance();
    $email->setTo($newUser->getEmailArray());

    $email->addPart($this->templating->render('CKBundle:Email:newuser.txt.twig', $templData), 'text/plain');
    $email->setBody($this->templating->render('CKBundle:Email:newuser.html.twig', $templData), 'text/html');
    $this->mailer->send($email);
  }

  /**
   * Send a password reset email
   * to a user that has forgotten the password.
   */
  public function sendPasswordReset(UserBase $user, $tmpPassword, $expiry_date)
  {
    $loginUrl = $this->router->generate('login_check', array(), true);
    $loginUrl .= '?u=' . urlencode(base64_encode($user->getEmail()) .
                            ':' . $tmpPassword) . '&r=1';
    $templData =
      array('user' => $user,
            'tmpPassword' => $tmpPassword,
            'expiry_date' => $expiry_date,
            'hostname' => $_SERVER['SERVER_NAME'],
            'loginURL' => $loginUrl);

    $this->from = $this->replyTo;
    $this->subject = $this->subjectPrefix . ' ' . $this->translator->trans('subject.passwordReset');
    $email = $this->getMessageInstance();
    $email->setTo($user->getEmailArray());

    $email->addPart($this->templating->render('CKBundle:Email:password_reset.txt.twig', $templData), 'text/plain');
    $email->setBody($this->templating->render('CKBundle:Email:password_reset.html.twig', $templData), 'text/html');
    $this->mailer->send($email);
  }

  /**
   * Send a system notification that a new user
   * has logged in for the first time
   */
  public function firstLogin(UserBase $user)
  {
    $templData =
      array('user'      => $user,
            'date_time' => date('Y-m-d H:i'),
            'ip'        => $_SERVER['REMOTE_ADDR']);

    $this->from = $this->replyTo;
    $this->subject = $this->subjectPrefix . ' First login of ' . $user->getName();
    $email = $this->getMessageInstance();
    $email->setTo($this->systemTo);
    $email->setBody($this->templating->render('CKBundle:Email:first_login.txt.twig', $templData), 'text/plain');
    $this->mailer->send($email);
  }

  /**
   * Send a daily report about the commitments of the user
   */
  public function dailyReport(UserBase $user, RequestTableInterface $requestTable)
  {
    // Set the selection class
    $selection = new DailyReport($this->em->getRepository('CKBundle:Request'));

    // Hand over control to the table model object that will do
    // the main work for us
    $templateData = $requestTable->getData($user->getId(), null, $selection);

    // Do not send empty reports
    if (0 == $templateData['NumItems']) {
      return;
    }

    // Need a base url for links to individual commitments
    $templateData['CommitmentBaseUrl'] = $this->router->generate(
      'CK_commitment_user',
      array('userStr' => 'user' . $user->getId()),
      true
    );

    $templateData['DateFormat'] = $this->dateFormat->getPHPpattern();

    $this->subject = $this->translator->trans('dailyReport.subject');
    $this->from = array('support@commitkeeper.com' => 'CommitKeeper');
    $email = $this->getMessageInstance();
    $email->setTo($user->getEmailArray());

    // Add embedded images
    // Get the header image
    $headerImage = Swift_Image::fromPath($this->webDir . 'images/dailyreportheader.png');
    $templateData['headerImage'] = $email->embed($headerImage);

    // Get the traffic light images
    $uriToCid = array();
    $numAdded = 0;
    foreach ($templateData['Table'] as $req) {
      /** @var Request $req */
      $uri = $req->getTrafficLight()->getImageUri();
      if (!array_key_exists($uri, $uriToCid)) {
        $uriToCid[$uri] = $email->embed(Swift_Image::fromPath($this->webDir . $uri));
        if (++$numAdded >= TL::DifferentOpenTrafficLights) {
          break;
        }
      }
    }
    $templateData['uriToCid'] = $uriToCid;

    $email->setBody($this->templating->render('CKBundle:Email:daily_report.html.twig', $templateData), 'text/html');
    $this->mailer->send($email);
  }
}
