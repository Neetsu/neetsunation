<?php
namespace FSpires\CommitKeeperBundle\Model\Enum;

/**
 * A class to enumerate the different traffic lights
 * The number correspond to the id in the database
 *
 * 1 = blue circle = negotiation
 * 2 = green arrow = on track
 * 3 = yellow triangle = at risk
 * 4 = slashed red circle = off track

 * 5 = silver checkmark = delivered
 * 6 = gold star = completed
 * 
 *     white crossed circle = cancelled
 *     crossed blue circle = declined
 */
final class TrafficLight
{
  private function __construct(){}

  // The number of different traffic lights
  // that can possibly be in a list of still open commitments
  const DifferentOpenTrafficLights = 5;

  const Preparation = 1;
  const Negotiation = 1;

  //Delivery
  const OffTrack    = 2;
  const AtRisk      = 3;
  const OnTrack     = 4;

  // Aknowledgement
  const Delivered   = 5;

  // Closed
  const Completed                =  6;
  const DeclinedRequest          =  7;
  const DeclinedOffer            =  8;
  const DeclinedCounteredRequest =  9;
  const DeclinedCounteredOffer   = 10;
  const CanceledStartedRequest   = 11;
  const CanceledStartedOffer     = 12;
  const CanceledRequest          = 13;
  const CanceledOffer            = 14;
  const CanceledCounteredRequest = 15;
  const CanceledCounteredOffer   = 16;
  const CanceledToDo             = 17;
}
