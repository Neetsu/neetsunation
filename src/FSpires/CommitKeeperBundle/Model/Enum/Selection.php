<?php
namespace FSpires\CommitKeeperBundle\Model\Enum;

/**
 * A class to enumerate the different selections
 * The strings are class names in the namespace "FSpires\CommitKeeperBundle\Model\Selection"
 */
final class Selection
{
  private function __construct(){}

  const PendingByMe = 'PendingByMe';
  const PendingByOthers = 'PendingByOthers';
  const AllActive = 'AllActive';
  const Closed = 'Closed';
  const DailyReport = 'DailyReport';

  // Not in use
  const Observed = 'Observed';
  const DoneByMe = 'DoneByMe';
  const DeliveredToMe = 'DeliveredToMe';
  const All = 'All';

  static public function getAll() {
    return array (
                  self::PendingByMe,
                  self::PendingByOthers,
                  self::AllActive,
                  self::Closed
                  );

  }
}
