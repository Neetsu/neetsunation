<?php
namespace FSpires\CommitKeeperBundle\Model\Enum;

/**
 * A class to enumerate the different actions that can be done to a request
 * These strings correspond to class names in the namespace FSpires\CommitKeeperBundle\Model\Action\
 */
final class Action
{
  private function __construct(){}

  const Make = 'Make';
  const Comment = 'Comment';

  const Accept = 'Accept';
  const Agree = 'Accept';

  const Cancel = 'Close';
  const Decline = 'Decline';

  const Amend = 'Amend';
  const Counter = 'Amend';
  const Revise = 'Revise';
  const AmendToDo = 'AmendToDo';

  const ReqProgress = 'ReqProgress';
  const ReportProgress = 'ReportProgress';

  const Deliver = 'Deliver';

  const Done = 'Done';

  const Complete = 'Complete';

  const ReqRework = 'ReqRework';
}
