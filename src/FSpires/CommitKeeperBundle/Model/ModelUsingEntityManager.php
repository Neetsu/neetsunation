<?php
namespace FSpires\CommitKeeperBundle\Model;

use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;

/**
 * A base class for all models that is using the entity manager
 */
class ModelUsingEntityManager
{
  /**
   * The Doctrine Entity Manager Object used to connect to the database
   * @var \Doctrine\ORM\EntityManager
   */
  protected $em;

  /**
   * Constructor
   * @param \Doctrine\ORM\EntityManager $em  Entity Manager
   **/
  public function __construct(EntityManagerInterface $em) {
    $this->em = $em;
  }
}
