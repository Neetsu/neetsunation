<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Entity\UserBase;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Entity\History;
use FSpires\CommitKeeperBundle\Model\Action\ActionInterface;
use FSpires\CommitKeeperBundle\Model\Enum\Action as AE;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType as RT;
use FSpires\CommitKeeperBundle\Model\EmailNotifierInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;

/**
 * The request state machinery
 */
class StateMachine extends ModelUsingEntityManager
                   implements StateMachineInterface
{
  /**
   * The object used to notify by email the other users
   * @var FSpires\CommitKeeperBundle\Model\EmailNotifier
   */
  private $emailNotifier;

  private $revision;

    /**
     * Constructor
     *
     * @param EntityManagerInterface $em Entity Manager
     * @param EmailNotifierInterface $emailNotifier Object responsible for
     *                                              email notifications
     * @param array $version Version information
     */
  public function __construct(EntityManagerInterface $em,
                              EmailNotifierInterface $emailNotifier,
                              array $version) {
    $this->em = $em;
    $this->emailNotifier = $emailNotifier;
    $this->revision = $version['revno'];
  }

  /**
   * Get a list of possible actions the user can do on a request
   * @param Request $req The request in question
   * @param UserBase $user The user that may perform the action
   * @return array An array of action objects
   */
  public function getActionsForReq(Request $req, UserBase $user)
  {
      $reqType = false;
      $actor = false;
      $user_id = $user->getId();
      $performer_id = $req->getPerformer()->getId();
      $requestor_id = $req->getRequestor()->getId();
      $initiator_id = $req->getInitiator()->getId();

      if ($initiator_id == $performer_id) {
        $reqType = RT::Offer;
      } else {
        $reqType = RT::Request;
      }
      if ($performer_id == $requestor_id) {
        $reqType = RT::ToDo;
      }

      if ($performer_id == $user_id) {
        $actor = 'P';
      } else if ($requestor_id == $user_id) {
        $actor = 'R';
      }
      if (!$actor) {
        // Check if the user is an observer
        foreach ($req->getAllObservers() as $observer) {
          if ($observer->getId()==$user_id) {
            $actor = 'O';
            break;
          }
        }
        if (!$actor) {
          // the user is not an observer
          return array();
        }
      }

      return $this->getActionsAllowed($req->getPhase(),
                                      $req->getPhasePendingBy(),
                                      $actor, $reqType);
  }

    /**
     * Get a list of allowed actions an user can do on a request
     *
     * @param string $phase The current phase of the request
     * @param string $pendingBy 'R' or 'P', Who is the request pending by
     * @param string $actor 'R' or 'P', Who is doing the action
     * @param string $reqType 'request', 'offer' or 'toDo', What type of request
     * @throws \UnexpectedValueException
     * @return array An array of action objects
     */
  public function getActionsAllowed($phase, $pendingBy, $actor, $reqType)
  {
    $acts = array();
    if ('O'!=$actor) {
      switch($phase) {
      case Phase::Preparation:
        //Make request or offer
        $acts[] = array(AE::Make, $reqType);
        break;
      case Phase::Negotiation:
        $label_ending = $actor . '.' . $reqType;
        if ($pendingBy==$actor) {
          $acts[] = array(AE::Agree, $label_ending);
          $acts[] = array(AE::Counter, $label_ending);
          $acts[] = array(AE::Decline, $label_ending);
        } else {
          $acts[] = array(AE::Revise, $label_ending);
          $acts[] = array(AE::Cancel, $label_ending);
        }
        break;
      case Phase::Delivery:
        $label_ending ='delivery.' . $actor . '.' . $reqType;
        if ('P'==$actor) {
          if (RT::ToDo==$reqType) {
            $acts[] = array(AE::Done);
            $acts[] = array(AE::Cancel, $label_ending);
          } else {
            $acts[] = array(AE::ReportProgress);
            $acts[] = array(AE::Deliver);
            $acts[] = array(AE::Amend, 'delivery');
          }
        } else {
          $acts[] = array(AE::ReqProgress);
          $acts[] = array(AE::Amend, 'delivery');
          $acts[] = array(AE::Cancel, $label_ending);
        }
        break;
      case Phase::Acknowledge:
        if ('R'==$actor) {
          $acts[] = array(AE::Complete);
          $acts[] = array(AE::ReqRework);
          $acts[] = array(AE::Cancel, 'delivery.R.' . $reqType );
        }
        break;
      case Phase::Closed:
        break;
      default:
        throw new \UnexpectedValueException(
           'Unexpected value set for the "commitment phase": ' . $phase);

      }
    }

    // Allways allowed to add a comment
    if (RT::ToDo==$reqType && Phase::Delivery==$phase) {
      $acts[] = array(AE::AmendToDo);
    } else {
      $acts[] = array(AE::Comment);
    }

    $actions = array();
    foreach ($acts as $act_array) {
      $act = array_shift($act_array);
      $label_ending = array_shift($act_array);

      // Set a correct label on the action
      $label = $act;
      if ($label_ending) {
        $label .= '.' . $label_ending;
      }

      // Instantiate the action
      $a_class = 'FSpires\\CommitKeeperBundle\\Model\\Action\\' . $act;
      $actions[$act] = new $a_class($label, $actor, $reqType);
    }
    return $actions;
  }

  /**
   * An user makes an action on a request
   * The request is modified and saved in the database,
   * Other modifiactions to the database is also done.
   * (History is updated etc.)

   * @param Request $req The request that will be updated
   * @param User $user The user that performs the action
   * @param $action The action (An action object)
   */
  public function doAction(Request $req, User $user, ActionInterface $action)
  {
    $hasBudgetField = $req->hasBudgetField();

    // Save the previous values
    $prevPhase = $req->getPhase();
    $prevTrafficLight = $req->getTrafficLight();
    $prevDueDate = $req->getDueDate();
    if ($hasBudgetField) {
      $prevBudget = $req->getBudget();
    }

    // Update the request
    $req->updateTrafficLight();
    $action->updateRequest($req);
    $req->updateAttachments($this->em);
    $this->em->persist($req);

    // Update the history
    $hist = new History();
    $hist->setRevision($this->revision);
    $hist->setFromRequest($req);
    $hist->setPhaseChanged($req->getPhase()!=$prevPhase);
    $hist->setTrafficLightChanged($req->getTrafficLight()!=$prevTrafficLight);
    $hist->setActionStr($action->getLabel());
    $hist->setDueDatePrevious($prevDueDate);
    if ($hasBudgetField) {
      $hist->setBudgetPrevious($prevBudget);
    }
    $hist->setActor($user);
    $hist->setRole($action->getActorRole());
    $this->em->persist($hist);

    // Save everything in the database
    $this->em->flush();

    // Mark as unread for other users
    $req->getRepository()->actionByUser($req->getId(), $user->getId());

    //Send email about what happened
    $this->emailNotifier->notify($user, $req, $hist);
  }
}
