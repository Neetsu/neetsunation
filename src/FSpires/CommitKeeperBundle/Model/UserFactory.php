<?php
namespace FSpires\CommitKeeperBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use FSpires\CommitKeeperBundle\Model\EmailNotifierInterface;
use FSpires\CommitKeeperBundle\Entity\TimeZone;
use FSpires\CommitKeeperBundle\Entity\Group;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Entity\UserBase;
use FSpires\CommitKeeperBundle\Entity\UserName;
use FSpires\CommitKeeperBundle\Security\CustomBadCredentialsException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type as DocType;
use PDO;

/**
 * Create new users
 */
class UserFactory
{
  const DEFAULT_TIMEZONE_OFFSET = -480;

  private $em;
  private $passwordEncoder;
  private $authenticationManager;
  private $secureRandom;
  private $securityContext;
  private $session;

  /**
   * The object used to notify by email the other users
   * @var \FSpires\CommitKeeperBundle\Model\EmailNotifier
   */
  private $emailNotifier;

  /**
   * Constructor
   *
   * @param EntityManagerInterface $em Entity Manager
   * @param EncoderFactoryInterface $passwordEncoderFactory
   * @param AuthenticationManagerInterface $authManager
   * @param SecureRandomInterface $secureRandom
   * @param SecurityContextInterface $securityContext
   * @param SessionInterface $session
   * @param EmailNotifierInterface $emailNotifier Object responsible for
   *                                                  email notifications
   * @internal param EncoderFactoryInterface $passwordEncoder
   */
  public function __construct(EntityManagerInterface $em,
                              EncoderFactoryInterface $passwordEncoderFactory,
                              AuthenticationManagerInterface $authManager,
                              SecureRandomInterface $secureRandom,
                              SecurityContextInterface $securityContext,
                              SessionInterface $session,
                              EmailNotifierInterface $emailNotifier) {
    $this->em = $em;
    $this->passwordEncoder = $passwordEncoderFactory->getEncoder('FSpires\CommitKeeperBundle\Entity\User');
    $this->authenticationManager = $authManager;
    $this->secureRandom = $secureRandom;
    $this->securityContext = $securityContext;
    $this->session = $session;
    $this->emailNotifier = $emailNotifier;
  }

  /**
   * Add inital settings to a newly created user
   */
  public function initiate(User $user, $addLinkedTables=true)
  {
    $this->em->persist($user);

    if (!$user->getPasswordExpiryDate()) {
      $user->setPasswordExpiryDate(new \DateTime('0000-00-00'));
    }

    $date = new \DateTime('now');
    $user->setCreatedDate($date);
    $user->setUpdatedDate($date);
    $user->setActive(7);
    $user->setWelcomeScreen(true);
    $user->setCCme(true);

    $timeZone = TimeZone::createFromOffset(self::DEFAULT_TIMEZONE_OFFSET);
    $user->setTimeZoneSlot($timeZone->getSlot());
    $user->setTimeZoneOffset($timeZone->getOffsetInMinutes());

    $user->setDailyReportEnabled(true);
    $user->setDefaultDateInterval('P1W');

    // Set initial authorization level and user type
    $user->setUserLevel(1);
    if (!$user->getUserType()) {
      $user->setUserType(3);
    }

    // TODO: How to set the initial userOwner and company
    $user->setUserOwnerId(1);
    $user->setCompanyId(1);

    if ($addLinkedTables) {
      $this->addLinkedTables($user);
    } else {
      // Set minimum to make this user a valid row in the database
      $user->setGroup($this->em->getPartialReference(
                'FSpires\CommitKeeperBundle\Entity\Group', 1));
    }
  }

  public function addLinkedTables(User $user, $creatorIsAdmin = false)
  {
    $user->createDefaultPicture($this->em);

    $group = new Group();
    $group->setNameFromUser($user);
    $group->setHasBudgetField(false);
    $group->setBudgetUnit($this->em->getPartialReference(
               'FSpires\CommitKeeperBundle\Entity\BudgetUnit', 1));
    $user->setGroup($group);
    $this->em->persist($group);
    $this->em->flush();

    if (!$creatorIsAdmin) {
      // Add admin as contact
      $urepo     = $this->em->getRepository('CKBundle:User');
      $adminUser = $urepo->find(1);
      $user->addContact(UserName::filter($adminUser, $this->em));
      $adminUser->addContact(UserName::filter($user, $this->em));
    }
  }

  /**
   * Save changes to a user
   */
  public function save($user)
  {
    // Only keep those openId providers that are checked as keep
    // and encrypt the password if set
    $user->finishAuthSetup($this->passwordEncoder, $this->em);

    $user->setUpdatedDate();
    $this->em->flush();
  }

  /**
   * Save a newly created user and login as that user
   */
  public function saveAndLogin($user, $isNew=true, $tryLogin=false)
  {
    if ($tryLogin) {
      $ctPassword = $user->getCtPassword();
    }
    $user->finishAuthSetup($this->passwordEncoder);

    try {
      // Add some initial data to the new user
      if ($isNew) {
        $this->initiate($user);
      } else {
        $user->setUserType(3);
        $user->setUpdatedDate();
      }

      $this->em->flush();

      if ($isNew) {
        $creator_id = $user->getUserOwnerId();
        $user->getGroup()->addDefaultCategories($creator_id, $this->em);
      }

      // Log in as the new user
      $token = new UsernamePasswordToken($user, null, 'secured_area',
                                         $user->getRoles());
      $this->securityContext->setToken($token);

      if ((3 == $user->getUserType()) && (7 == $user->getActive())) {
        $this->emailNotifier->firstLogin($user);
        $user->setActive(1);
        $this->em->flush();
      }

      return false;
    } catch (DBALException $ex) {
      $innerEx = $ex->getPrevious();
      if ($isNew && $innerEx && $innerEx instanceof \PDOException &&
          1062==$innerEx->errorInfo[1]) {
        if ($tryLogin) {
          if ($this->loginUser($user->getEmail(), $ctPassword)) {
            return;
          }
        }
        return 'user.emailTaken';
      } else {
        throw $ex;
      }
    }
  }

  /**
   * Try to login the user
   */
  private function loginUser($email, $password) {
    
    $token = new UsernamePasswordToken($email, $password, 'secured_area');
    try {
      $token = $this->authenticationManager->authenticate($token);
      if ($token->isAuthenticated()) {
        // Log in as the new user
        $this->securityContext->setToken($token);
        return true;
      }
    } catch (BadCredentialsException $ex) {
    }
    return false;
  }

  /**
   * Setup a new type 1 user
   */
  public function setupFirstLogin(User $user) {
    $password = base64_encode($this->secureRandom->nextBytes(18));
    $user->setupFirstLogin($password);
  }

  /**
   * Used when adding a new contact
   * Either find an existing user or make a new one
   * @param UserBase $contact Info about contact input from form and checked
   * @param UserBase $creator The user that is adding the new contact
   * @return UserName pointing to a user that is stored in the database
   */
  public function newContact(UserBase $contact, User $creator)
  {
    // Check first if we have the contact in the system already
    $urepo = $this->em->getRepository('CKBundle:UserName');
    $existingUser = $urepo->findOneBy(array('email' => $contact->getEmail()));
    if ($existingUser) {
      return $existingUser;
    }

    $newUser = new User();
    $contact->copyToUser($newUser);

    $this->initiate($newUser, false);
    $newUser->setUserOwnerId($creator->getId());
    $newUser->setTimeZoneOffset($creator->getTimeZoneOffset());
    $newUser->setTimeZoneSlot($creator->getTimeZoneSlot());

    $this->setupFirstLogin($newUser);

    $newUser->setUserType(0);
    $newUser->setActive(0);

    // Make sure the user is saved to the database
    $this->em->flush();

    // Flag in the session that users have been added
    $this->session->set('newUsersAdded', true);

    return UserName::filter($newUser, $this->em);
  }

  /**
   * Finalize a newly added contact
   * @param UserBase $contact Info about contact input from form and checked
   * @param UserBase $creator The user that is adding the new contact
   * @return UserName pointing to a user that is stored in the database
   */
  public function finalizeNewContact(User $contact, User $creator)
  {
    $creatorIsAdmin = (1 == $creator->getId());
    $this->addLinkedTables($contact, $creatorIsAdmin);

    // Copy "has budget field" and unit from creators group
    $toGroup = $contact->getGroup();
    $fromGroup = $creator->getGroup();
    if ($toGroup->getId()!=$fromGroup->getId()) {
      $toGroup->setHasBudgetField($fromGroup->hasBudgetField());
      $toGroup->setBudgetUnit($fromGroup->getBudgetUnit());
      $toGroup->addDefaultCategories($creator->getId(), $this->em);
    }

    $contact->addContact(UserName::filter($creator, $this->em));

    $contact->setUserType(1);
    $contact->setActive(1);

    // Send e-mail to the new user
    $this->emailNotifier->newUser($contact, $creator);
  }

  /**
   * Update contacts connected to a commitment
   * Called every time a new commitment is created or updated
   */
  public function updateContacts($user, $requests) {
    $newUsersAdded = $this->session->get('newUsersAdded', false);
    if (!is_array($requests)) {
      $requests = array($requests);
    }
    if ($newUsersAdded) {
      $checkUserIds = array();
      foreach ($requests as $req) {
        $checkUserIds[$req->getPerformer()->getId()] = 1;
        $checkUserIds[$req->getRequestor()->getId()] = 1;
        foreach ($req->getObservers() as $observer) {
          $checkUserIds[$observer->getId()] = 1;
        }
      }
      unset($checkUserIds[$user->getId()]);
      $urepo = $this->em->getRepository('CKBundle:User');
      $newUsers = $urepo->findNewUsers(array_keys($checkUserIds), 0);
      foreach ($newUsers as $newUser) {
        $this->finalizeNewContact($newUser, $user);
      }
    }
    $this->em->flush();
    if ($newUsersAdded) {
      $deleteSql = 'DELETE FROM uc USING user_contact AS uc'
        . ' INNER JOIN user AS u1 ON u1.id=uc.user_id'
        . ' INNER JOIN user AS u2 ON u2.id=uc.contact_id'
        . ' WHERE (u1.user_type=0 AND u1.user_owner_id=:userId)'
        . ' OR (u2.user_type=0 AND u2.user_owner_id=:userId)';
      $param_values = array('userId' => $user->getId());
      $param_types  = array('userId' => PDO::PARAM_INT);
      $conn = $this->em->getConnection();
      $conn->executeUpdate($deleteSql, $param_values, $param_types);
      $deleteSql = 'DELETE FROM ru USING request_unread AS ru'
        . ' INNER JOIN user AS u ON u.id=ru.user_id'
        . ' WHERE u.user_type=0 AND u.user_owner_id=:userId';
      $conn->executeUpdate($deleteSql, $param_values, $param_types);
      $deleteSql = 'DELETE FROM user'
        . ' WHERE user_type=0 AND user_owner_id=:userId';
      $conn->executeUpdate($deleteSql, $param_values, $param_types);

      $this->session->set('newUsersAdded', false);
    }
    foreach ($requests as $req) {
      $this->connectContacts($req->getId());
    }
  }

  /**
   * Connect all users in this request as contacts of each other
   * Called every time a commintment is updated
   */
  public function connectContacts($requestId) {
    // All users in this request should have each other as a contact
    $union = '(SELECT performer_id AS user_id FROM request WHERE id=:requestId'
    .' UNION SELECT requestor_id AS user_id FROM request WHERE id=:requestId'
    .' UNION SELECT user_id FROM request_parentobserver WHERE request_id=:requestId'
    .' UNION SELECT user_id FROM request_observer WHERE request_id=:requestId)';
    $sql = 'INSERT INTO user_contact SELECT u1.user_id, u2.user_id FROM '
      . $union . ' AS u1 CROSS JOIN ' . $union . ' AS u2'
      . ' LEFT JOIN user_contact AS uc'
      .   ' ON uc.user_id=u1.user_id AND uc.contact_id=u2.user_id'
      . ' WHERE uc.user_id IS NULL AND u1.user_id!=u2.user_id;';
    $param_values = array('requestId' => $requestId);
    $param_types  = array('requestId' => PDO::PARAM_INT);
    $conn = $this->em->getConnection();
    $conn->executeUpdate($sql, $param_values, $param_types);
  }

  /**
   * Add a temporary password and send email
   * Used when a user has forgotten the password
   *
   * @param  string $email   The email of the User
   */
  public function passwordReset($email)
  {
    $user = $this->em->getRepository('CKBundle:User')
      ->findOneBy(array('email' => $email));
    if (!$user) {
      throw new CustomBadCredentialsException(
        'login.wrongResetPassword%email%',
        array('%email%' => $email)
      );
    }

    if ($user->getUserType() < 2) {
      $tmpPassword = $user->getPassword();
      $expiry_date = false;
    } else {
      $tmpPassword = base64_encode($this->secureRandom->nextBytes(18));
      $encPasswd = $this->passwordEncoder->encodePassword($tmpPassword, null);
      $created_date = new \DateTime();
      $expiry_date = new \DateTime('+20 minute');
      $sql = 'INSERT INTO temp_password'
        . ' (user_id, created_date, expiry_date, password, ip, used)'
        . ' VALUES (:user_id, :created_date, :expiry_date, :password, :ip, 0)';
      $paramValues = array('user_id'  => $user->getId(),
                           'created_date' => $created_date,
                           'expiry_date'  => $expiry_date,
                           'password' => $encPasswd,
                           'ip' => $_SERVER['REMOTE_ADDR']);
      $paramTypes = array('user_id'  => PDO::PARAM_INT,
                          'created_date' => DocType::DATETIME,
                          'expiry_date'  => DocType::DATETIME,
                          'password' => PDO::PARAM_STR,
                          'ip' => PDO::PARAM_STR);
      $conn = $this->em->getConnection();
      $conn->executeUpdate($sql, $paramValues, $paramTypes);
    }
    $this->emailNotifier->sendPasswordReset($user, $tmpPassword, $expiry_date);
  }
}
