<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Entity\Picture;
use FSpires\CommitKeeperBundle\Form\Type\GravatarDefault;

class ChangePicture extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    /*
    In calling code: option url = $picture->getGravatarUrl('') . '&d=';

    $fb->add('gravatarDefault', new GravatarDefault(),
             array('label' => 'picture.label.gravatar',
                   'choices' => Picture::getDefaultGravatarChoices(),
                   'url' => $options['url']));
    */

    $fb->add('file', 'file',
             array('label' => 'picture.label.file',
                   'required' => false));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
          'url' => null,
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\Picture'
                                 ));
  }

  public function getName()
  {
    return 'change_picture';
  }
}
