<?php
namespace FSpires\CommitKeeperBundle\Form\DataMapper;

use Symfony\Component\Form\Extension\Core\DataMapper\PropertyPathMapper;
use FSpires\CommitKeeperBundle\Entity\UserName;
use Doctrine\Common\Collections\Collection;


class UserDataMapper extends PropertyPathMapper
{
  private $contacts;
  /**
   * Creates a new user data property path mapper.
   *
   * @param Collection $contacts
   */
  public function __construct(Collection $contacts)
  {
    parent::__construct();
    $this->contacts = $contacts;
  }

  /**
   * {@inheritdoc}
   */
  public function mapFormsToData($forms, &$data)
  {
    $passedForms = [];
    foreach($forms as $form) {
      $passedForms[$form->getName()] = $form;
    }
    $id = $passedForms['id']->getData();
    if ($id) {
      // We have got back one of the existing contacts
      foreach($this->contacts as $contact) {
        if ($contact->getId()==$id) {
          $data = $contact;
          return;
        }
      }
      // Got a non-existing id
      $data = null;
      return;
    }
    unset($passedForms['id']);

    // A new user, just do normal mapping of the other subforms
    parent::mapFormsToData($passedForms, $data);
  }
}
