<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditPersonal extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('picture', 'change_picture');

    $fb->add('timeZoneOffset', 'time_zone', array('label' => 'settings.timeZone'));

    $fb->add('dailyReportEnabled', 'checkbox',
             array(
               'label' => 'settings.dailyReport',
               'required' => false
             )
    );
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\User'
                                 ));
  }

  public function getName()
  {
    return 'edit_personal';
  }
}
