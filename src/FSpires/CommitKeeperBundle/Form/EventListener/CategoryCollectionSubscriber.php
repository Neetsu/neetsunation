<?php
namespace FSpires\CommitKeeperBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;

class CategoryCollectionSubscriber implements EventSubscriberInterface
{
  /**
   * @var array
   */
  private $choices;

  /**
   * @var array
   */
  private $options;

  /**
   * @var boolean
   */
  private $add_current_value;

  public function __construct($choices, $options=array(), $add_current_value=false)
  {
    $this->choices = $choices;
    $this->options = $options;
    $this->add_current_value = $add_current_value;
  }

  public static function getSubscribedEvents()
  {
    // Tells the dispatcher that we want to listen on the form.pre_set_data
    // event and that the preSetData method should be called.
    return array(FormEvents::PRE_SET_DATA => 'preSetData');
  }

  public function preSetData(FormEvent $event)
  {
    $data = $event->getData();
    $form = $event->getForm();

    /* During form creation setData() is called with null as an argument
     * by the FormBuilder constructor. We're only concerned with when
     * setData is called with an actual Entity object in it (whether new
     * or fetched with Doctrine). This if statement lets us skip right
     * over the null condition.*/
    if (null == $data) {
      return;
    }

    // First remove all rows
    foreach ($form as $name => $child) {
      $form->remove($name);
    }

    // Then add all rows again in the correct order
    foreach ($data as $name => $value) {
      $category = false;
      $cid = $value->getId();
      foreach($this->choices as $cat) {
        if ($cat->getId() == $cid) {
          $category = $cat;
          break;
        }
      }
      if (!$category) {
        throw new \InvalidArgumentException('Invalid category id: '
                                            . $cid);
      }

      $choose_tags = $category->getTags();
      if ($this->add_current_value) {
        $selected_tag = $value->getTag();
        if ($selected_tag) {
          $selected_id = $selected_tag->getId();
          $notfound = true;
          $given_tags = $choose_tags;
          $choose_tags = array();
          foreach ($given_tags as $tag) {
            if ($tag->getId()==$selected_id) {
              $choose_tags[] = $selected_tag;
              $notfound = false;
            } else {
              $choose_tags[] = $tag;
            }
          }
          if ($notfound) {
            $choose_tags[] = $selected_tag;
          }
        }
      }
      $choice_list = new ObjectChoiceList($choose_tags,
                                          'Name', array(), null, 'Id');
      $options = array_replace(
          array('property_path'=>'['.$name.'].Tag',
                'choice_list'=>$choice_list,
                'label'=>$category->getName(),
                'error_bubbling' => true),
          $this->options);
      $form->add($name, 'choice', $options);
    }
  }
}
