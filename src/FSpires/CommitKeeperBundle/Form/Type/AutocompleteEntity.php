<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * AutoComplete Entity
 */
class AutocompleteEntity extends EntityType
{
  public function getParent()
  {
    return 'autocomplete_choice';
  }

  public function getName()
  {
    return 'autocomplete_entity';
  }
}
