<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class PasswordInput extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $passwordOptions = array(
     'type' => 'password',
     'invalid_message' => 'password.mustMatch',
     'options' => array('required'=>$options['required'],
                        'error_bubbling'=>true),
     'label' => 'password.localAccount',
     'first_options'=> array('label' =>'password.first'),
     'second_options'=> array('label' => 'password.second'));
    if ($options['required']) {
      $passwordOptions['required'] = true;
      $passwordOptions['constraints'] = array(
        new NotBlank(array('message' => 'password.notBlank')),
        new Length(array(
         'min' => 6,
         'minMessage'=>'password.shortPassword'
                         )));
    }
    if ($options['real_property_path']) {
      $passwordOptions['property_path'] = $options['real_property_path'];
    }
    $fb->add('password', 'repeated', $passwordOptions);
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
                   'real_property_path' => false,
                   'compound' => true
                                 ));
  }

  public function getName()
  {
    return 'password_input';
  }
}
