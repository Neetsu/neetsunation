<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OpenIdentifier extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('keep', 'checkbox', array('required'=>false));
    $fb->add('provider', 'info');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\OpenIdentifier'
                                 ));
  }

  public function getName()
  {
    return 'openIdentifier';
  }
}
