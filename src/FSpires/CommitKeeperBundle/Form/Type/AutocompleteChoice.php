<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Exception\FormException;
use Symfony\Component\Form\Extension\Core\DataTransformer\ChoiceToValueTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * AutoComplete Choice
 */
class AutocompleteChoice extends ChoiceType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    if (!$options['choice_url'] && !$options['choice_list'] && !is_array($options['choices']) && !$options['choices'] instanceof \Traversable) {
      throw new FormException('Either the option "choice_url","choices" or "choice_list" must be set.');
    }

    $fb->addViewTransformer(new ChoiceToValueTransformer($options['choice_list']));
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    parent::setDefaultOptions($resolver);
    $resolver->setDefaults(array(
            'compound'          => false,
            'choice_url'        => null,
            'options'           => array(),
                                 ));
  }

  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    $choice_url = $options['choice_url'];
    $choices = null;
    $default_text = null;

    if (!$choice_url) {
      $choices_raw = $options['choice_list']->getPreferredViews() +
                     $options['choice_list']->getRemainingViews();

      // Get the default text
      $value = $form->getViewData();
      if (!empty($value)) {
        $index = $options['choice_list']->getIndicesForValues(array($value));
        if (1==count($index)) {
          $index = current($index);
          $cview = $choices_raw[$index];
          $default_text = $cview->label;
        }
      }

      // Transform the choices array to what the autocomplete javascript wants
      $choices = array();
      foreach ($choices_raw as $cview) {
        $choices[] = array($cview->label, $cview->value);
      }
      $choices = json_encode($choices);
    }

    $ac_options = $options['options'];
    //Allways set this onSelect so the form can get the key value
    $onSelectFunction = 'aitsel_' . $view->vars['id'];
    $ac_options['onItemSelect'] = $onSelectFunction;

    // Transform the ac_options array for use in the javascript
    $ac_options = json_encode($ac_options, JSON_FORCE_OBJECT);
    $ac_options = str_replace('"','',$ac_options);

    $view->vars['text_value'] = $default_text;
    $view->vars['choice_url'] = $choice_url;
    $view->vars['choices'] = $choices;
    $view->vars['ac_options'] = $ac_options;
    $view->vars['onSelectFunction'] = $onSelectFunction;
  }

  /**
   * {@inheritdoc}
   */
  public function finishView(FormView $view, FormInterface $form, array $options)
  {
  }

  public function getParent()
  {
    return 'form';
  }

  public function getName()
  {
    return 'autocomplete_choice';
  }
}
