<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;

class JsDatePickType extends AbstractType
{
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $format = function (Options $options, $default) {
      $dateFormat = $options['dateFormat'];
      if (empty($dateFormat)) {
        return $default;
      }
      if ($dateFormat instanceof DateFormatInterface) {
        return $dateFormat->getIDFformat();
      }
      return $dateFormat;
    };
    $resolver->setDefaults(array('compound'=> false,
                                 'widget'=>'single_text',
                                 'format' =>$format,
                                 'dateFormat'=>\IntlDateFormatter::MEDIUM)
                           );
  }

  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'date';
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'date_picker';
  }
}
