<?php

namespace FSpires\CommitKeeperBundle\Form\Type;

use FSpires\CommitKeeperBundle\Entity\TimeZone as TimeZoneEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeZone extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $choices = array();

    foreach(TimeZoneEntity::getRepository()->findAll() as $key => $zone) {
      $choices[$key] = $zone->getLabel();
    }

    $resolver->setDefaults(array(
      'choices' => $choices,
      'invalid_message' => 'Invalid time zone offset'
    ));
  }

  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'choice';
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'time_zone';
  }
}
