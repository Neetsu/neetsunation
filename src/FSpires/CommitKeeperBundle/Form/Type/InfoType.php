<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InfoType extends AbstractType
{
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array('compound'=> false,
                                 'read_only'=>true,
                                 'disabled'=>true)
                           );
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'info';
  }
}
