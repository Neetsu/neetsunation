<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserNameInput extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $nameLabelType = 'short';
    if ($options['name_in_label']) {
      $nameLabelType = 'long';
    }
    $fb->add('firstName', null, array('label'=>'user.firstName.' . $nameLabelType));
    $fb->add('lastName',  null, array('label'=>'user.lastName.'  . $nameLabelType));

    $emailLabel = 'user.email.long';
    if ($options['short_email']) {
      $emailLabel = 'user.email.short';
    }
    $fb->add('email', 'email', array('label'=>$emailLabel, 'max_length'=>200));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
                   'name_in_label' => true,
                   'short_email' => false,
                   'compound' => true,
                   'virtual'  => true,
                   'data_class' => 'FSpires\CommitKeeperBundle\Entity\UserName'
                                 ));
  }

  public function getName()
  {
    return 'user_name_input';
  }
}
