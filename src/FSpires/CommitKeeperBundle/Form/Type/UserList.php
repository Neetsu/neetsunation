<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;

/**
 * Special input class for a list of users
 */
class UserList extends AbstractType
{
  private $translator;
  public function __construct($translator) {
    $this->translator = $translator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $prototype = $builder->create('__value__', 'checkbox', array(
                  'label' => '__label__',
                  'value' => '__value__',
                  'required' => false
                                                                ));
    $builder->setAttribute('prototype', $prototype->getForm());
  }

  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    $fixedUsers = (($options['fixed'] instanceof \IteratorAggregate)
                 || is_array($options['fixed'])) ? $options['fixed'] : array();

    $list_text = $this->translator->trans($options['empty_value']);
    $formData = $form->getData();
    if ($formData) {
      $values = $formData->toArray();
      $chosen = $options['choice_list']->getIndicesForChoices($values);
    } else {
      $chosen = array();
    }
    if ((count($chosen)+count($fixedUsers))>0) {
      $choices =  $options['choice_list']->getPreferredViews() +
                  $options['choice_list']->getRemainingViews();
      $labels = array();
      foreach ($fixedUsers as $user) {
        $labels[] = trim($user->getName());
      }
      foreach ($chosen as $index) {
        $labels[] = trim(preg_replace('/\s*<.*>$/','',$choices[$index]->label));
      }
      if (count($labels)) {
        $list_text = array_pop($labels);
        if (count($labels)) {
          $and = $this->translator->trans('and');
          $list_text = join(', ', $labels).' '.$and.' '.$list_text;
        }
      }
    }

    $view->vars['fixed'] = $fixedUsers;
    $view->vars['list_text'] = $list_text;
    $prototypeView = $form->getConfig()->getAttribute('prototype')->createView($view);
    $prototypeView->vars['full_name'] = $view->vars['full_name'] . '[]';
    $view->vars['prototype'] = $prototypeView;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
        'fixed'=>null,
        'expanded'=>true,
        'multiple'=>true,
        'class'=>'CKBundle:UserName'
    ));

    $emptyValueNormalizer = function (Options $options, $emptyValue) {
      return $emptyValue;
    };
    $resolver->setNormalizers(array(
        'empty_value' =>  $emptyValueNormalizer
    ));
  }

  public function getParent()
  {
    return 'entity';
  }

  public function getName()
  {
    return 'user_list';
  }
}
