<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Form\EventListener\CategoryCollectionSubscriber;

class CommitmentCategory extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $subformsSubscriber = new CategoryCollectionSubscriber(
            $options['choices'],
            $options['options'],
            $options['add_current_value']);
    $fb->addEventSubscriber($subformsSubscriber);
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
       'options' => array(),
       'choices' => null,
       'add_current_value'=>false,
       'block_name' => 'entry'));
  }

  public function getName()
  {
    return 'ccategory';
  }
}
