<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SepChoiceType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    $view->vars['separator'] = $options['separator'];
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array('separator'=>'---------'));
  }

  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'choice';
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'sep_choice';
  }
}
