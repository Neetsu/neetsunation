<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Form\Type\AttachmentInput;
use FSpires\CommitKeeperBundle\Form\Type\AttachmentEdit;

/**
 * FSpires\CommitKeeperBundle\Form\Commitment
 *
 */
class Commitment extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    if ($options['canEdit']) {
      $fb->add('newDueDate', 'date_picker',
               array('dateFormat'=>$options['dateFormat'],
                     'attr'=>array('class'=>'date_input'),
                     'invalid_message' => 'commitment.dateInvalid'));

      if ($options['hasBudgetField']) {
        $fb->add('newBudget', null, array('required'=>false));
      }

      if ($options['canEditObservers']) {
        $fb->add('observers', 'user_list', array(
               'fixed'       => $options['observersFixed'],
               'choice_list' => $options['observerChoiceList'],
               'empty_value'=>'observers.none',
               'read_only' => !$options['canEdit'],
               'disabled' => !$options['canEdit']
                                                     )
                 );
      }
    }

    if ($options['canEditCategoryValues']) {
      $fb->add('categories', 'ccategory',
               array('options'=>array('empty_value'=>'tag.no.value'),
                     'choices'=> $options['userCategories'],
                     'add_current_value'=>true,
                     'required' => false,
                     'error_bubbling' => false
                     ));
    }

    $fb->add('action', 'Achoice' ,
             array('label'=>'NextAction',
                   'translation_domain'=>'action',
                   'expanded'=>true,
                   'choices'=>$options['action_choices']
                   )
             );

    if ($options['statusUpdateChoices']) {
      $fb->add('statusUpdate', 'statusUpdate',
               array('choices'=>$options['statusUpdateChoices']));
    }

    $fb->add('description', 'textarea', array('label'=>'Description'));
    $fb->add('CCme', 'checkbox', array('label'=>'CCme', 'required'=>false));

    if ($options['canEdit']) {
      $fb->add('attachments', 'collection',
               array('label'=>'Attachments.label',
                     'type' => new AttachmentEdit(),
                     )
               );
      $fb->add('newAttachments', 'collection',
               array('label'=> 'NewAttachments',
                     'type' => new AttachmentInput(),
                     'allow_add' => true,
                     'allow_delete' => true,
                     'prototype' => true
                     )
               );
    }
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
                   'data_class' => 'FSpires\CommitKeeperBundle\Entity\Request',
                   'action_choices' => null,
                   'hasBudgetField' => false,
                   'observersFixed' => null,
                   'observerChoiceList' => null,
                   'userCategories' => null,
                   'dateFormat' => \IntlDateFormatter::MEDIUM,
                   'canEdit' => true,
                   'canEditObservers' => true,
                   'canEditCategoryValues' => true,
                   'statusUpdateChoices' => false
                                 ));
  }

  public function getName()
  {
    return 'commitment';
  }
}
