<?php
namespace FSpires\CommitKeeperBundle\Form\Util;

use Symfony\Component\Form\FormView;

/**
 * Utility class for FormViews
 */
class FormViewUtil
{
  /**
   * Make a deep copy of a form view
   */
  public static function copy(FormView $old)
  {
    $new = clone $old;
    $new->vars['form'] = $new;
    $new->children = array();
    foreach ($old->children as $key => $child) {
      $newChild = self::copy($child);
      $newChild->parent = $new;
      $new->children[$key] = $newChild;
    }

    return $new;
  }
}
