<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Form\Type\OpenIdentifier;

class UserRegister extends AbstractType
{
    public function buildForm(FormBuilderInterface $fb, array $options)
    {
      $fb->add('username', 'user_name_input',
               array('name_in_label' => $options['name_in_label'],
                     'short_email' => $options['short_email']));

      if ($options['show_openids']) {
        $fb->add('openIdentifier', 'collection',
                 array('label'=>'user.registration.authenticationProvider',
                       'type'=> new OpenIdentifier(),
                       'error_bubbling' => false
                       ));

        $fb->add('keep', 'checkbox', array('required'=>false));
      }

      $fb->add('passwordinput', 'password_input',
               array('virtual'=>true,
                     'required'=>false,
                     'real_property_path' => 'ctPassword',
                     'data_class' => $options['data_class']
                     ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      $resolver->setDefaults(array(
                   'name_in_label' => true,
                   'short_email' => false,
                   'show_openids' => true,
                   'data_class' => 'FSpires\CommitKeeperBundle\Entity\User'
                                   ));
    }

    public function getName()
    {
        return 'user_register';
    }
}
