<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Form\Type\Tag;

class UserTagCategory extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('tags', 'collection',
             array('type'=> new Tag(),
                   'options' => array('hasReadOnly'=>true),
                   'allow_add' => true,
                   'allow_delete' => true,
                   'prototype' => true,
                   'error_bubbling' => true
                   ));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
       'data_class' => 'FSpires\CommitKeeperBundle\Entity\Category'
                                 ));
  }

  public function getName()
  {
    return 'user_category';
  }
}
