<?php

namespace FSpires\CommitKeeperBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use FSpires\CommitKeeperBundle\Model\RequestTableInterface;
use FSpires\CommitKeeperBundle\Model\EmailNotifierInterface;
use FSpires\CommitKeeperBundle\Entity\UserRepository;
use FSpires\CommitKeeperBundle\Entity\User;

class DailyReport extends Command
{
  const SENDING_HOUR = 5; // The hour of the day when we send out the emails

  /**
   * @var EntityManagerInterface
   */
  private $em;

  /**
   * @var RequestTableInterface
   */
  private $reqTable;

  /**
   * @var EmailNotifierInterface
   */
  private $emailNotifier;

  /**
   * Constructor
   *
   * @param EntityManagerInterface $em
   * @param RequestTableInterface $reqTable
   * @param EmailNotifierInterface $emailNotifier
   */
  public function __construct(
      EntityManagerInterface $em,
      RequestTableInterface $reqTable,
      EmailNotifierInterface $emailNotifier
    ) {
    $this->em = $em;
    $this->reqTable = $reqTable;
    $this->emailNotifier = $emailNotifier;

    parent::__construct();
  }

  protected function configure()
  {
    $this
      ->setName('ck:daily-report')
      ->setDescription('Run the CommitKeeper daily report')
      ->addOption(
        'time-zone',
        't',
        InputOption::VALUE_REQUIRED,
        'Timezone offset(-11 to +12). If set, the daily report will be run for that time zone.'
      )
      ->addOption(
        'user',
        'u',
        InputOption::VALUE_REQUIRED,
        'User id. Run daily report for only the user with this user id.'
      )
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->em->getRepository('CKBundle:User');

    $userId = $input->getOption('user');
    if ($userId) {
      $user = $userRepository->find($userId);
      if ($user) {
        $this->sendDailyReport($user, $output);
      } else {
        $output->writeln('<error>User not found.</error>');
      }
      return;
    }

    $timeZoneSlot = $this->getTimeZoneSlot($input->getOption('time-zone'), $output);
    if (is_null($timeZoneSlot)) {
      return;
    }
    $output->writeln('Sending to users in time zone slot ' . $timeZoneSlot);

    $users = $userRepository->findByTimeZoneSlot($timeZoneSlot);
    if ($users) {
      foreach ($users as $user) {
        $this->sendDailyReport($user, $output);
      }
    }
    else {
      $output->writeln('Found no users in slot ' . $timeZoneSlot);
    }
  }

  private function sendDailyReport(User $user, OutputInterface $output)
  {
    $output->writeln('Sending daily report for ' . $user->getNameAndEmail());
    $this->emailNotifier->dailyReport($user, $this->reqTable);
  }

  private function getTimeZoneSlot($inputTzOffset, OutputInterface $output)
  {
    if (isset($inputTzOffset)) {
      $offset = intval($inputTzOffset);
      if ($offset < -11) {
        $output->writeln('<error>Timezone cannot be less than -11</error>');
        return null;
      }
      if ($offset > 12) {
        $output->writeln('<error>Timezone cannot be greater than 12</error>');
        return null;
      }
    } else {
      $now = time();
      $hour = gmdate('G', $now);
      $minutes = gmdate('i', $now);
      if ($minutes > 30) {
        ++$hour;
      }
      $offset = self::SENDING_HOUR - $hour;
      if ($offset < -11) {
        $offset += 24;
      }
    }
    return $offset;
  }
}
