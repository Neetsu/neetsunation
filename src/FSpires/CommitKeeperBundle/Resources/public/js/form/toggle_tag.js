function toggle_tag(checkbox) {
    var td_node = checkbox.parentNode.nextSibling;
    var text, inputbox, span_node, text_node;
    if(checkbox.checked) {
        // Put input box back
        span_node = td_node.firstChild;
        inputbox = $(span_node).data('inputbox');
        td_node.removeChild(span_node);
        td_node.appendChild(inputbox);
    } else {
        // Remove input box
        inputbox = td_node.firstChild;
        text = inputbox.value;
        if (td_node.className!='newtag' || text!='') {
            td_node.removeChild(inputbox);
            span_node = document.createElement('SPAN');
            text_node = document.createTextNode(text);
            span_node.appendChild(text_node);
            span_node.className = 'strikethrough';
            $(span_node).data('inputbox', inputbox);
            td_node.appendChild(span_node);
        } else {
            var tr_node = td_node.parentNode;
            tr_node.parentNode.removeChild(tr_node);
        }
    }
}

function add_tag(button) {
    var last_tr = $(button.parentNode.parentNode);
    var table = last_tr.parents('table');
    var prototype = table.attr('data-prototype');
    var newIndex = last_tr.parent().children().length - 3;
    var newForm = prototype.replace(/__name__/g, newIndex);
    var newTD = $('<td class="newtag"></td>').append(newForm);
    var newTR = $('<tr><td><input type="checkbox" checked="checked" onchange="toggle_tag(this);"/></td></tr>').append(newTD);
    last_tr.before(newTR);
}
