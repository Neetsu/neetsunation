// Insert received new user in the HTML
function newUserInsert(user, $table) {
  if (user.isNewContact) {
    // Add to possible users
    $('.users_table').each(function(){
      var $userTable = $(this);
      var userNewTr = $userTable.attr('data-prototype');
      userNewTr = userNewTr.replace(/__value__/g, user.id);
      userNewTr = userNewTr.replace('__label__', user.fname + ' ' + user.lname + ' &lt;' + user.email + '&gt;');
      $userTable.append(userNewTr);
    });

    // Add to the list of contacts if the list exists as an UserInput field
    if (typeof acUserEntity == "object" && acUserEntity) {
        acUserEntity.fn.addItem([user.fname, user.id, user.fname, user.lname, user.email]);
        acUserEntity.ln.addItem([user.lname, user.id, user.fname, user.lname, user.email]);
        acUserEntity.em.addItem([user.email, user.id, user.fname, user.lname, user.email]);
    }
  }
  // Make user checked
  $table.find('input[value="'+user.id+'"]').attr('checked','checked').trigger('change');
}

// Receive new user from server
function newUserReceive(data, $dialog, $formSpan, $table) {
  $dialog.removeClass('sending-ajax');

  var newForm = false;
  if (data.error) {
    newForm = data.form;
  } else {
    if (data.user) {
      newUserInsert(data.user, $table);
      newUserClean($formSpan);
      return;
    }
  }
  if (newForm) {
    $formSpan.html(newForm);
  } else {
    $formSpan.find('.error').remove();
    var errorMsg = Translator.trans('error.contactNotSaved');
    var errorUl =
      '<ul class="error"><li>'+errorMsg+'</li></ul>';
    $formSpan.prepend(errorUl);
  }
}

function newUserClean($formSpan) {
    $formSpan.find('.error').remove();
    $formSpan.find('input:not(:last)').val('');
}

function newUserAdd($formSpan, $table) {
  var $dialog = $formSpan.parent();
  $dialog.addClass('sending-ajax');
  $.post($dialog.attr('data-action'),
          $formSpan.find('input').serialize()).always(
          function(data) {
          newUserReceive(data, $dialog, $formSpan, $table);
          });
}
