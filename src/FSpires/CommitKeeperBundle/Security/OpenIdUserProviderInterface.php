<?php
namespace FSpires\CommitKeeperBundle\Security;

/**
 * Provides users from an open id identifier
 */
interface OpenIdUserProviderInterface
{
  /**
   * Try to match a user to an Open Id (Janrain) login token
   *
   * @param JanrainUserToken $token A temporary JanrainUserToken
   *
   * @return mixed A UserInterface object or just the user name as a string
   * @throws UsernameNotFoundException if a user could not be found
   */
  public function loadUserByToken(JanrainUserToken $token);
}
