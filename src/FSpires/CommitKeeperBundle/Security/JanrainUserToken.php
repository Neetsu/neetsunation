<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class JanrainUserToken extends UsernamePasswordToken
{
  /**
   * Constructor.
   *
   * @param string/object $user_or_token This an user object or
   *                                     the jamrain token_id
   * @param string $providerKey          The provider key
   * @param array  $openIdData           Data received from openid
   * @param array  $roles                An array of roles
   *
   * @throws \InvalidArgumentException
   */
  public function __construct($user_or_token, $providerKey,
                              array $openIdData = array(),
                              array $roles = array()) {
    $user = '';
    $credentials = null;
    $hasOpenIdData = false;
    if (count($openIdData)>0) {
      $user = $user_or_token;
      $credentials = $openIdData['identifier'];
      $hasOpenIdData = true;
    } else {
      if (is_string($user_or_token) && strlen($user_or_token)==40) {
        $credentials = $user_or_token;
        $roles = array(); // Not authenticated yet
      } else {
        throw new \InvalidArgumentException('Invalid jamrain token');
      }
    }

    parent::__construct($user, $credentials, $providerKey, $roles);
    if ($hasOpenIdData) {
      $this->setAttributes($openIdData);
    }
  }

  /**
   * Get the jamrain token when we have not contacted the openid service yet
   * @return string
   */
  public function getJToken() {
    return $this->getCredentials();
  }

  /**
   * Get array of data returned from the OpenId service
   * @return array
   */
  public function getOpenIdData() {
    return $this->getAttributes();
  }


  /**
   * {@inheritdoc}
   */
  public function eraseCredentials()
  {
    parent::eraseCredentials();
    $this->setAttributes(array());
  }

  /**
   * Retrives a an array of emails to try (in order)
   * extraced from the openIdData in the JanrainUserToken
   */
  public function retriveEmails()
  {
    $openIdData = $this->getOpenIdData();

    $email_list = array();
    $emails_found = array();

    if (isset($openIdData['verifiedEmail'])) {
      $email = $openIdData['verifiedEmail'];
      $email_list[] = $email;
      $emails_found[$email] = true;
    }

    if (isset($openIdData['email'])) {
      $email = $openIdData['email'];
      if (!isset($emails_found[$email])) {
        $email_list[] = $email;
        $emails_found[$email] = true;
      }
    }

    if (isset($openIdData['emails'])) {
      $emails = $openIdData['emails'];
      if (is_array($emails)) {
        $email_types = array();
        foreach ($emails as $item) {
          if (isset($item['value'])) {
            $email = $item['value'];
            if (!isset($emails_found[$email])) {
              $type = false;
              if (isset($item['primary']) && $item['primary']) {
                $type = 'primary';
              } else if (isset($item['type'])) {
                $type = $item['type'];
              }
              if (!$type) {
                $type = 'other';
              }
              if (!isset($email_types[$type])) {
                $email_types[$type] = array();
              }
              $email_types[$type][] = $email;
              $emails_found[$email] = true;
            }
          }
        }
        // Loop over the known types in prioritized order
        $ordered_types = array('primary','work','other','home');
        foreach ($ordered_types as $type) {
          if (isset($email_types[$type])) {
            $email_list = array_merge($email_list,
                                      $email_types[$type]);
            unset($email_types[$type]);
          }
        }
        // Loop over any remaining types we may have forgot
        foreach ($email_types as $type_arr) {
          $email_list = array_merge($email_list, $type_arr);
        }
      }
    }

    return $email_list;
  }
}
