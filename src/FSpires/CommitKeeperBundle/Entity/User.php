<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\DBAL\Connection AS DBAL_Conn;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityChoiceList;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Form\ChoiceList\ORMNativeQueryLoader;

/**
 * FSpires\CommitKeeperBundle\Entity\User
 *
 * @Assert\Callback(methods={"validateAuthMethods"})
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\UserRepository")
 */
class User extends UserBase implements AdvancedUserInterface, EquatableInterface
{
  const MIN_PASSWORD_LENGTH = 6;

  /**
   * A collection of OpenIdentifier objects
   * @var \Doctrine\Common\Collections\ArrayCollection $openIdentifier
   *
   * @ORM\OneToMany(targetEntity="OpenIdentifier", mappedBy="user", cascade={"persist"})
   */
  private $openIdentifier;

  /**
   * Encrypted password
   * @var string $password
   *
   * @ORM\Column(name="password", type="string", length=60, nullable=false)
   */
  private $password;

  /**
   * Clear text password
   * @var string $ctPassword
   * Not saved in the database, only used for input of new password
   */
  private $ctPassword;

  /**
   * Do we want to keep a local password for this user, checkbox
   * Not stored in database, but used in user register and user edit forms
   * It is a string and not a bool because we want more than two states
   * 
   * @var string $keep
   */
  private $keep = null;

  /**
   * @var \DateTime $passwordExpiryDate
   *
   * @ORM\Column(name="password_expiry_date", type="datetime", nullable=false)
   */
  private $passwordExpiryDate;

  /**
   * @var \DateTime $createdDate
   *
   * @ORM\Column(name="created_date", type="datetime", nullable=false)
   */
  private $createdDate;

  /**
   * @var \DateTime $updatedDate
   *
   * @ORM\Column(name="updated_date", type="datetime", nullable=false)
   */
  private $updatedDate;

  /**
   * @var string $userLevel
   *
   * @ORM\Column(name="user_level", type="integer", nullable=false)
   */
  private $userLevel;

  /**
   * @var integer $userType
   *
   * @ORM\Column(name="user_type", type="integer", nullable=false)
   */
  private $userType;

  /**
   * @var integer $active
   *
   * @ORM\Column(name="active", type="boolean", nullable=false)
   */
  private $active;

  /**
   * @var integer $userOwnerId
   *
   * @ORM\Column(name="user_owner_id", type="integer", nullable=false)
   */
  private $userOwnerId;

  /**
   * @var \FSpires\CommitKeeperBundle\Entity\Picture $picture
   *
   * @Assert\Valid
   * @ORM\OneToOne(targetEntity="Picture", mappedBy="user")
   */
  private $picture;

  /**
   * @var string $defaultDateInterval;
   *
   * @ORM\Column(name="default_date_interval", type="string", length=12, nullable=false)
   */
  private $defaultDateInterval;


  /**
   * @var integer $welcomeScreen
   *
   * @ORM\Column(name="hasWelcomeScreen", type="boolean", nullable=false)
   */
  private $welcomeScreen;

  /**
   * @var integer $CCme
   *
   * @ORM\Column(name="ccme", type="boolean", nullable=false)
   */
  private $CCme;

  /**
   * Default table that shows up if no table view
   * is selected as the current table
   * @var string $defaultTableUrl;
   */
  private $defaultTableUrl = false;

  /**
   * @var \Doctrine\Common\Collections\Collection $contacts
   *
   * @ORM\ManyToMany(targetEntity="UserName")
   * @ORM\JoinTable(name="user_contact",
   *      joinColumns={@ORM\JoinColumn(name="user_id")},
   *      inverseJoinColumns={@ORM\JoinColumn(name="contact_id")}
   *      )
   * @ORM\OrderBy({"lastName" = "ASC", "firstName" = "ASC", "email" = "ASC"})
   */
  private $contacts;

  /**
   * @var integer $managerUserId
   *
   * @ORM\Column(name="manager_user_id", type="integer", nullable=true)
   */
  private $managerUserId;

  /**
   * @var integer $companyId
   *
   * @ORM\Column(name="company_id", type="integer", nullable=false)
   */
  private $companyId;

  /**
   * @var \FSpires\CommitKeeperBundle\Entity\Group $group
   *
   * @ORM\ManyToOne(targetEntity="Group")
   * @ORM\JoinColumn(name="cgroup_id")
   */
  private $group;

  /**
   * @var \Doctrine\Common\Collections\Collection $categories;
   *
   * A collection of tags with categories as keys to different
   * sub-collections of tags, one for each category.
   * For each user that is a member of a group, the categories
   * are in the same order as in the group.
   * (This relationship is too complex to be handled automatically
   * by doctrine, so we have to remember to retrive and save this
   * field in the database with our own code.)
   */
  private $categories;

  /**
   * Offset in minutes from GMT, describing the users time zone
   *
   * @var integer
   * @ORM\Column(name="time_zone_offset", type="integer", nullable=false)
   */
  private $timeZoneOffset;

  /**
   * Time zone slot. An integer hour offset from GMT in the range -11 to 12
   *
   * @var integer
   * @ORM\Column(name="time_zone_slot", type="integer", nullable=false)
   */
  private $timeZoneSlot;


  /**
   * True if this user wants the daily report by email
   *
   * @var bool
   * @ORM\Column(name="daily_report_enabled", type="boolean", nullable=false)
   */
  private $dailyReportEnabled;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->openIdentifier = new ArrayCollection();
    $this->contacts = new ArrayCollection();
  }

  /**
   * Set up the user so everything is ready for the first login
   * The user will need to set a password or an openID provider
   * on the first login.
   */
  public function setupFirstLogin($password)
  {
    $this->userType = 1;
    $this->password = $password;
    $this->ctPassword = '';
  }

  public function getNewUserURLpart()
  {
    if (1 < $this->userType) {
      throw new \LogicException('Wrong user type');
    }

    return 'u=' . urlencode(base64_encode($this->getEmail()) .
                            ':' . $this->password);
  }

  /**
   * Add openIdentifier
   *
   * @param \FSpires\CommitKeeperBundle\Entity\OpenIdentifier $openIdentifier
   */
  public function addOpenIdentifier(OpenIdentifier $openIdentifier)
  {
    $openIdentifier->setUser($this);
    $this->openIdentifier[] = $openIdentifier;
  }

  /**
   * Set the openIdentifier collection
   *
   * @param \Doctrine\Common\Collections\ArrayCollection $openIdentifiers
   */
  public function setOpenIdentifier(ArrayCollection $openIdentifiers)
  {
    foreach ($openIdentifiers as $oid) {
      $oid->setUser($this);
    }
    $this->openIdentifier = $openIdentifiers;
  }

  /**
   * Get openIdentifier
   *
   * @return \Doctrine\Common\Collections\ArrayCollection
   */
  public function getOpenIdentifier()
  {
    return $this->openIdentifier;
  }

  /**
   * Check if we have a valid password
   *
   */
  protected function validatePassword($ec)
  {
    if (empty($this->ctPassword)) {
      if (!empty($this->password)) {
        // The user does not want to change the password
        return;
      }
      //Make sure it is a string, so the next test is problem free
      $this->ctPassword='';
    }

    if (strlen($this->ctPassword) < self::MIN_PASSWORD_LENGTH) {
      $ec->addViolationAt('ctPassword', 'user.shortPassword%minLength%',
          array('%minLength%' => self::MIN_PASSWORD_LENGTH), $this->ctPassword);
    }
  }

  /**
   * Check if we have at least one valid authentication method.
   * Either at least one OpenIdentifier or a valid password
   */
  public function validateAuthMethods($ec) {
    if ($this->getKeep()) {
      $this->validatePassword($ec);
      // We have a valid password we are going to keep
      return;
    }
    foreach ($this->openIdentifier as $oid) {
      if ($oid->getKeep()) {
        // We have at least one openIdentifier we are going to keep
        // So everything is ok
        return;
      }
    }
    // We have no authentication method
    $ec->addViolationAt('openIdentifier', 'user.noAuthMethod', array(), false);
  }

  /**
   * Finish the authorization setup for this user.
   *
   * Check if any authentication providers have been unchecked
   * Then the user do not want them anymore, so remove them
   *
   * Also make sure the password is encrypted.
   *
   * @param \Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface
   *              $PWEncoder Password encoder
   * @param \Doctrine\ORM\EntityManager $em Needed when the users auth.
   *                                       providers already are in the DB
   */
  public function finishAuthSetup(PasswordEncoderInterface $PWEncoder,
                                  EntityManagerInterface $em=null) {
    foreach ($this->openIdentifier as $key => $oid) {
      if (!$oid->getKeep()) {
        $this->openIdentifier->remove($key);
        if (isset($em)) {
          $em->remove($oid);
        }
      }
    }
    if ($this->getKeep()) {
      // Only encrypt a new password if it is not empty
      if (!empty($this->ctPassword)) {
        $this->password = $PWEncoder->encodePassword(trim($this->ctPassword), null);
      }
    } else {
      $this->password = '';
    }
    $this->ctPassword = null;
  }

  /**
   * Returns the roles granted to the user.
   *
   * @return Role[] The user roles
   */
  function getRoles() {
    return array('ROLE_LEVEL' . $this->userLevel,
                 'ROLE_TYPE' . $this->userType);
  }

  /**
   * Returns the encrypted password used to authenticate the user.
   *
   * @return string The password
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * Sets the encrypted password used to authenticate the user.
   *
   * @param string $encPasswd The encrypted password
   */
  public function setPassword($encPasswd) {
    $this->password = $encPasswd;
  }

  /**
   * Returns the salt that was originally used to encode the password.
   *
   * This is null because bcrypt stores the salt in the encrypted password
   *
   * @return string The salt
   */
  public function getSalt() {
    return null;
  }

  /**
   * Returns the username used to authenticate the user.
   *
   * @return string The username
   */
  public function getUsername() {
    return $this->email;
  }

  /**
   * Removes sensitive data from the user.
   *
   * This is important if, at any given point, sensitive information like
   * the plain-text password is stored on this object.
   *
   * @return void
   */
  public function eraseCredentials() {
    $this->ctPassword = null;
  }

  /**
   * Returns whether or not the given user is equivalent to *this* user.
   *
   * The equality comparison should neither be done by referential equality
   * nor by comparing identities (i.e. getId() === getId()).
   *
   * However, you do not need to compare every attribute, but only those that
   * are relevant for assessing whether re-authentication is required.
   *
   * @param UserInterface $user
   *
   * @return Boolean
   */
  public function isEqualTo(UserInterface $user) {
    if (!$user instanceof User) {
      return false;
    }

    if ($this->email !== $user->getUsername()) {
      return false;
    }

    if ($this->userLevel !== $user->getUserLevel()) {
      return false;
    }

    if ($this->userType !== $user->getUserType()) {
      return false;
    }

    return true;
  }

  /**
   * Checks whether the user's account has expired.
   *
   * Internally, if this method returns false, the authentication system
   * will throw an AccountExpiredException and prevent login.
   *
   * @return Boolean true if the user's account is non expired, false otherwise
   *
   * @see AccountExpiredException
   */
  public function isAccountNonExpired() {
    return true;
  }

  /**
   * Checks whether the user is locked.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a LockedException and prevent login.
   *
   * @return Boolean true if the user is not locked, false otherwise
   *
   * @see LockedException
   */
  public function isAccountNonLocked() {
    return true;
  }

  /**
   * Checks whether the user's credentials (password) has expired.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a CredentialsExpiredException and prevent login.
   *
   * @return Boolean true if the user's credentials are non expired,
   *                 false otherwise
   *
   * @see CredentialsExpiredException
   */
  public function isCredentialsNonExpired() {
    $valid = true;
    if ($this->passwordExpiryDate &&
        $this->passwordExpiryDate > new \DateTime('1111-01-01')) {
      $valid = $this->passwordExpiryDate > new \DateTime('now');
    }
    return $valid;
  }

  /**
   * Checks whether the user is enabled.
   *
   * Internally, if this method returns false, the authentication system
   * will throw a DisabledException and prevent login.
   *
   * @return Boolean true if the user is enabled, false otherwise
   *
   * @see DisabledException
   */
  public function isEnabled() {
    return ($this->active > 0) || ($this->active === true);
  }

  /**
   * Set clear text password
   *
   * @param string $cTxtPassword
   */
  public function setCtPassword($cTxtPassword)
  {
    $this->ctPassword = $cTxtPassword;
  }

  /**
   * Get clear text password
   */
  public function getCtPassword()
  {
    return $this->ctPassword;
  }

  /**
   * Set to false if we want to drop using local password
   *
   * @param bool $keep
   */
  public function setKeep($keep)
  {
    if ($keep) {
      $this->keep = 'Yes';
    } else {
      $this->keep = 'No';
    }
  }

  /**
   * Do we keep using local password authentication?
   *
   * @return bool
   */
  public function getKeep()
  {
    if (empty($this->keep)) {
      if (empty($this->password)) {
        $this->keep = 'No';
      } else {
        $this->keep = 'Yes';
      }
    }
    return 'Yes'===$this->keep;
  }

  /**
   * Set passwordExpiryDate
   *
   * @param datetime $passwordExpiryDate
   */
  public function setPasswordExpiryDate($passwordExpiryDate)
  {
    $this->passwordExpiryDate = $passwordExpiryDate;
  }

  /**
   * Get passwordExpiryDate
   *
   * @return datetime 
   */
  public function getPasswordExpiryDate()
  {
    return $this->passwordExpiryDate;
  }

  /**
   * Set createdDate
   *
   * @param datetime $createdDate
   */
  public function setCreatedDate($createdDate)
  {
    $this->createdDate = $createdDate;
  }

  /**
   * Get createdDate
   *
   * @return \DateTime
   */
  public function getCreatedDate()
  {
    return $this->createdDate;
  }

  /**
   * Set updatedDate
   *
   * @param \DateTime $updatedDate
   */
  public function setUpdatedDate(\DateTime $updatedDate=null)
  {
    if (empty($updatedDate)) {
      $updatedDate = new \DateTime('now');
    }
    $this->updatedDate = $updatedDate;
  }

  /**
   * Get updatedDate   *

   *
   * @return \DateTime
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Set picture
   *
   * @param Picture $picture
   */
  public function setPicture(Picture $picture)
  {
    $this->picture = $picture;
  }

  /**
   * Get picture
   *
   * @param EntityManagerInterface $em
   * @return Picture
   */
  public function getPicture(EntityManagerInterface $em=null)
  {
    if (!$this->picture) {
      $this->createDefaultPicture($em);
    }
    return $this->picture;
  }

  /**
   * Create a default picture for a new user
   *
   * @param EntityManagerInterface $em
   */
  public function createDefaultPicture(EntityManagerInterface $em=null)
  {
    $this->picture = new Picture();
    $this->picture->setUser($this);
    $this->picture->useGravatar();
    if ($em) {
      $em->persist($this->picture);
    }
  }

  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
    if ($this->picture && $this->picture->isGravatar()) {
      $this->picture->useGravatar();
    }
  }

  /**
   * Set userLevel
   *
   * @param string $userLevel
   */
  public function setUserLevel($userLevel)
  {
    $this->userLevel = $userLevel;
  }

  /**
   * Get userLevel
   *
   * @return string 
   */
  public function getUserLevel()
  {
    return $this->userLevel;
  }

  /**
   * Set userType
   *
   * @param integer $userType
   */
  public function setUserType($userType)
  {
    $this->userType = $userType;
  }

  /**
   * Get userType
   *
   * @return integer 
   */
  public function getUserType()
  {
    return $this->userType;
  }

  /**
   * Set userOwnerId
   *
   * @param integer $userOwnerId
   */
  public function setUserOwnerId($userOwnerId)
  {
    $this->userOwnerId = $userOwnerId;
  }

  /**
   * Get userOwnerId
   *
   * @return integer 
   */
  public function getUserOwnerId()
  {
    return $this->userOwnerId;
  }

  /**
   * Set active
   *
   * @param integer $active
   */
  public function setActive($active)
  {
    $this->active = $active;
  }

  /**
   * Get active
   *
   * @return integer 
   */
  public function getActive()
  {
    return $this->active;
  }

  /**
   * Get Default Date to be used when creating new requests/commitments
   *
   * @return \DateTime
   */
  public function getDefaultDate()
  {
    $defaultDate = new \DateTime('now');
    return $defaultDate->add(new \DateInterval($this->defaultDateInterval));
  }

  /**
   * Set DefaultDateInterval
   *
   * @param string $defaultDateInterval (1 week = 'P1W')
   */
  public function setDefaultDateInterval($defaultDateInterval)
  {
    $this->defaultDateInterval = $defaultDateInterval;
  }

  /**
   * Get DefaultDateInterval
   *
   * @return string
   */
  public function getDefaultDateInterval()
  {
    return $this->defaultDateInterval;
  }

  /**
   * Set the default Table Url
   *
   * @param string $defaultTableUrl
   */
  public function setDefaultTableUrl($defaultTableUrl)
  {
    $this->defaultTableUrl = $defaultTableUrl;
  }

  /**
   * Get the default Table Url
   *
   * @return string
   */
  public function getDefaultTableUrl()
  {
    return $this->defaultTableUrl;
  }

  /**
   * Get a list of observers to choose from
   *
   * @return ChoiceList
   */
  public function getObserverChoiceList(Request $req,
                                        EntityManagerInterface $em)
  {
    $class = 'CKBundle:UserName';
    $rsm = new ResultSetMappingBuilder($em);
    $rsm->addRootEntityFromClassMetadata($class, 'u');
    $natQ = $em->createNativeQuery('', $rsm)
      ->setParameter('uid', $this->id);

    $sql = 'SELECT u.id, u.first_name, u.last_name, u.email'
      . ' FROM user AS u';
    $altSQL = $sql;

    $sql .= ' INNER JOIN user_contact AS uc ON u.id=uc.contact_id'
      . ' WHERE uc.user_id=:uid';

    $pObs = $req->getParentObservers();
    if ($pObs->count()>0) {
      $sql .= ' AND u.id NOT IN (:pObs)';
      $natQ->setParameter('pObs',
                          $pObs->map(
                             function ($user) {return $user->getId();}
                                     )->toArray(),
                          DBAL_Conn::PARAM_INT_ARRAY);
    }
    $sql .= ' ORDER BY u.last_name, u.first_name, u.email';
    $natQ->setSQL($sql);
    $altSQL .= ' WHERE ' . ORMNativeQueryLoader::PlaceHolder;
    $loader = new ORMNativeQueryLoader($natQ, 'u', $altSQL);
    return new EntityChoiceList($em, $class, 'NameAndEmail', $loader);
  }

  /**
   * Get contacts
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getContacts()
  {
    return $this->contacts;
  }

  /**
   * Check if we already have a contact
   * @param UserBase $contact User to search for among contacts
   * @return boolean True if the contact is in the list
   */
  public function hasContact(UserBase $contact)
  {
    $contact_id = $contact->getId();
    if (!$contact_id) {
      throw new \InvalidArgumentException(
        'Trying to check for the existence of a contact without an userId');
    }
    foreach ($this->contacts as $econt) {
      if ($econt->getId() == $contact_id) {
        return true;
      }
    }

    return false;
  }

  /**
   * Add contact
   *
   * @param \FSpires\CommitKeeperBundle\Entity\UserName $contacts
   */
  public function addContact(UserName $contact)
  {
    $this->contacts->add($contact);
  }

  /**
   * Remove contacts
   *
   * @param \FSpires\CommitKeeperBundle\Entity\UserName $contact
   */
  public function removeContact(UserName $contact)
  {
    $this->contacts->removeElement($contact);
  }

  /**
   * Set if user has a welcome screen
   *
   * @param boolean $welcomeScreen
   */
  public function setWelcomeScreen($welcomeScreen)
  {
    $this->welcomeScreen = (boolean) $welcomeScreen;
  }

  /**
   * Has the user a welcome screen?
   *
   * @return boolean 
   */
  public function hasWelcomeScreen()
  {
    return $this->welcomeScreen;
  }

  /**
   * Set CCme
   *
   * @param boolean $CCme
   */
  public function setCCme($CCme)
  {
    $this->CCme = (boolean)$CCme;
  }

  /**
   * Get CCme
   *
   * @return boolean
   */
  public function getCCme()
  {
    return $this->CCme;
  }

  /**
   * Set managerUserId
   *
   * @param integer $managerUserId
   */
  public function setManagerUserId($managerUserId)
  {
    $this->managerUserId = $managerUserId;
  }

  /**
   * Get managerUserId
   *
   * @return integer 
   */
  public function getManagerUserId()
  {
    return $this->managerUserId;
  }

  /**
   * Set companyId
   *
   * @param integer $companyId
   */
  public function setCompanyId($companyId)
  {
    $this->companyId = $companyId;
  }

  /**
   * Get companyId
   *
   * @return integer 
   */
  public function getCompanyId()
  {
    return $this->companyId;
  }

  /**
   * Set group
   *
   * @param \FSpires\CommitKeeperBundle\Entity\Group $group
   */
  public function setGroup(Group $group)
  {
    $this->group = $group;
  }

  /**
   * Get group
   *
   * @return \FSpires\CommitKeeperBundle\Entity\Group
   */
  public function getGroup()
  {
    return $this->group;
  }

  /**
   * Get categories that belong to this user
   *
   * @param EntityManagerInterface $em
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getCategories(EntityManagerInterface $em)
  {
    if ($this->categories) {
      return $this->categories;
    }
    $repo = $em->getRepository('CKBundle:User');
    $this->categories = $repo->findCategories($this->getId(),
                                              $this->group->getId());
    return $this->categories;
  }

  /**
   * Set the Time Zone Offset in minutes
   *
   * @param integer $offset
   */
  public function setTimeZoneOffset($offset)
  {
    $this->timeZoneOffset = $offset;
  }

  /**
   * Get the Time Zone Offset in minutes
   *
   * @return integer
   */
  public function getTimeZoneOffset()
  {
    return $this->timeZoneOffset;
  }

  /**
   * Set Time Zone Slot
   *
   * @param integer $slot
   */
  public function setTimeZoneSlot($slot)
  {
    $this->timeZoneSlot = $slot;
  }

  /**
   * Get Time Zone Slot
   *
   * @return integer
   */
  public function getTimeZoneSlot()
  {
    return $this->timeZoneSlot;
  }

  /**
   * Set if the daily report is wanted
   *
   * @param bool $wantIt
   */
  public function setDailyReportEnabled($wantIt)
  {
    $this->dailyReportEnabled = (bool)$wantIt;
  }

  /**
   * Is the daily report enabled?
   *
   * @return integer
   */
  public function isDailyReportEnabled()
  {
    return $this->dailyReportEnabled;
  }

  //Serializing related functions

  public function serialize()
  {
    return serialize(array(
                 $this->userLevel,
                 $this->userType,
                 $this->active,
                 parent::serialize()
                           ));
  }

  public function unserialize($str)
  {
    list(
                 $this->userLevel,
                 $this->userType,
                 $this->active,
                 $parentstr
         ) = unserialize($str);
    parent::unserialize($parentstr);
  }
}
