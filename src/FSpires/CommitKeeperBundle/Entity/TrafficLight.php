<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * FSpires\CommitKeeperBundle\Entity\TrafficLight
 *
 * Works with the trafficlights enumerated in
 * FSpires\CommitKeeperBundle\Model\Enum\TrafficLight
 *
 * @ORM\Table(name="traffic_light")
 * @ORM\Entity
 */
class TrafficLight
{
  /**
   * Base of the URI string that leads to the images.
   * Must end with a /
   */
  const basepath = 'images/';

  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;
  
  /**
   * @var string $title
   *
   * @ORM\Column(name="title", type="string", length=20, nullable=false)
   */
  private $title;

  /**
   * @var string $filename
   *
   * @ORM\Column(name="filename", type="string", length=20, nullable=false)
   */
  private $filename;

  public static $statusUpdateMsgs = array(TL::OnTrack  => 'status.OnTrack',
                                          TL::AtRisk   => 'status.AtRisk',
                                          TL::OffTrack => 'status.OffTrack');

  private static $imgFiles = array(TL::OnTrack  => 'on_track.png',
                                   TL::AtRisk   => 'at_risk.png',
                                   TL::OffTrack => 'off_track.png');

  public static $statusMsgs = array(
                          TL::Negotiation => 'status.Negotiation',
                          TL::OnTrack     => 'status.OnTrack',
                          TL::AtRisk      => 'status.AtRisk',
                          TL::OffTrack    => 'status.OffTrack',
                          TL::Delivered   => 'status.Delivered',
                          TL::Completed   => 'status.Closed',
                          TL::CanceledStartedRequest => 'status.Canceled.R',
                          TL::CanceledStartedOffer   => 'status.Canceled.O',
                          TL::CanceledRequest   => 'status.Canceled.R',
                          TL::CanceledOffer     => 'status.Canceled.O',
                          TL::DeclinedRequest => 'status.Declined.R',
                          TL::DeclinedOffer   => 'status.Declined.O',
                          TL::CanceledCounteredRequest => 'status.Canceled.R',
                          TL::CanceledCounteredOffer   => 'status.Canceled.O',
                          TL::DeclinedCounteredRequest => 'status.Declined.R',
                          TL::DeclinedCounteredOffer   => 'status.Declined.O',
                          TL::CanceledToDo => 'status.Canceled.R');

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get title
   *
   * @return string 
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * Get filename of image
   *
   * @return string 
   */
  public function getFilename()
  {
    return $this->filename;
  }

  /**
   * Get an URI to the image
   *
   * @return string 
   */
  public function getImageUri()
  {
    return self::basepath . $this->filename;
  }

  /**
   * Get the traffic light as a status message string
   * @return string 
   */
  public function getStatusMsg($reqType=false)
  {
    $statusMsg = self::$statusMsgs[$this->id];
    if (TL::Negotiation==$this->id && $reqType) {
      $statusMsg .= '.' . $reqType;
    }
    return $statusMsg;
  }

  /**
   * Get an array to be used in the status update control
   */
  public function getSelectArray()
  {
    $retval = array();
    foreach (self::$statusUpdateMsgs as $id => $text) {
      $retval[$id] = array('text'=> $text,
                           'img' => self::basepath . self::$imgFiles[$id]);
    }
    return $retval;
  }
}
