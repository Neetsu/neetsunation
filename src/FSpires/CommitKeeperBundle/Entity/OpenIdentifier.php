<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSpires\CommitKeeperBundle\Entity\OpenIdentifier
 *
 * @ORM\Table(name="openIdentifier")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\OpenIdentifierRepository")
 */
class OpenIdentifier
{
  /**
   * @var object $user
   *
   * @ORM\ManyToOne(targetEntity="User", inversedBy="openIdentifier")
   * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   */
  private $user;

  /**
   * @var string $identifier
   *
   * @ORM\Id
   * @ORM\Column(name="identifier", type="string", unique=true, length=100, nullable=false)
   */
  private $identifier;

  /**
   * Human readable provider of this authentication method
   * @var string $provider
   *
   * @ORM\Column(name="provider", type="string", length=100, nullable=false)
   */
  private $provider;

  /**
   * Do we want to keep this provider, checkbox
   * Not stored in database, but used in user register and user edit forms
   * 
   * @var bool $keep
   */
  private $keep = true;

  public function __construct(array $openIdData=null, $email=null) {
    if (is_array($openIdData)) {
      if (array_key_exists('identifier', $openIdData)) {
        $this->identifier = $openIdData['identifier'];
      } else {
        throw new \InvalidArgumentException('No identifier in openIdData');
      }
      // Set the provider name
      $this->provider = 'None';
      if (array_key_exists('providerName', $openIdData)) {
        $this->provider = $openIdData['providerName'];
      }
      if (empty($email)) {
        if (array_key_exists('verifiedEmail', $openIdData)) {
          $email = $openIdData['verifiedEmail'];
        } else if (array_key_exists('email', $openIdData)) {
          $email = $openIdData['email'];
        }
      }
      if (!empty($email)) {
        $this->provider .= ': ' . $email;
      }
    }
  }

  /**
   * Set identifier
   *
   * @param string $identifier
   */
  public function setIdentifier($identifier)
  {
    $this->identifier = $identifier;
  }

  /**
   * Get identifier
   *
   * @return string 
   */
  public function getIdentifier()
  {
    return $this->identifier;
  }

  /**
   * Set user
   *
   * @param \FSpires\CommitKeeperBundle\Entity\User $user
   */
  public function setUser(User $user)
  {
    $this->user = $user;
  }

  /**
   * Get user
   *
   * @return \FSpires\CommitKeeperBundle\Entity\User
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Set provider
   *
   * @param string $provider
   */
  public function setProvider($provider)
  {
    $this->provider = $provider;
  }

  /**
   * Get provider
   *
   * @return string 
   */
  public function getProvider()
  {
    return $this->provider;
  }

  /**
   * Set to false if we want to drop this provider
   *
   * @param bool $keep
   */
  public function setKeep($keep)
  {
    $this->keep = (boolean)$keep;
  }

  /**
   * Do we keep this?
   *
   * @return bool
   */
  public function getKeep()
  {
    return $this->keep;
  }


  //Serializing related functions
  public function __sleep()
  {
    return array('identifier', 'provider');
  }

  public function __wakeup()
  {
    $this->keep = true;
  }
}
