<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Common\Collections\ArrayCollection;
use FSpires\CommitKeeperBundle\Model\Enum\Selection as RS;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;
use PDO;
use FSpires\CommitKeeperBundle\Entity\Tag;
use FSpires\CommitKeeperBundle\Entity\Category;

/**
 * RequestRepository
 *
 * Repository methods to get data from the requests table in the database
 */
class RequestRepository extends EntityTagRepository
{
  /**
   * @var string $dqlSelect
   */
  private $dqlSelect;

  public function __construct($em, ClassMetadata $class) {
    parent::__construct($em, $class);
    $this->dqlSelect =
      'SELECT r, ur, up, t FROM FSpires\CommitKeeperBundle\Entity\Request r'
      . ' JOIN r.requestor ur'
      . ' JOIN r.performer up'
      . ' JOIN r.trafficLight t';
  }

  public function findDeliveredDate($reqId)
  {
    $sql = 'SELECT event_date FROM history'
      . " WHERE request_id=:reqId AND action='Deliver'"
      . ' ORDER BY event_date DESC';
    $param_values = array('reqId'=>$reqId);
    $param_types  = array('reqId'=>PDO::PARAM_INT);
    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    if ($stmt) {
      return $stmt->fetchColumn();
    }
    return false;
  }

  /**
   * Mark request as read by this user
   */
  public function markReadByUser($reqId, $userId)
  {
    $sql = 'DELETE FROM request_unread'
      . ' WHERE request_id=:reqId AND user_id=:userId';
    $this->getEntityManager()->getConnection()
      ->executeUpdate($sql,
                      array('reqId' =>$reqId,
                            'userId'=>$userId),
                      array('reqId' =>PDO::PARAM_INT,
                            'userId'=>PDO::PARAM_INT));
  }

  /**
   * Mark request as unread by all other users
   */
  public function actionByUser($reqId, $userId)
  {
    $sql = 'INSERT INTO request_unread (request_id, user_id)'
      . ' SELECT DISTINCT :reqId, uids.user_id FROM'
      . ' ('
      .          'SELECT performer_id AS user_id FROM request'
      .          ' WHERE id=:reqId'
      .   ' UNION SELECT requestor_id AS user_id FROM request'
      .          ' WHERE id=:reqId'
      .   ' UNION SELECT user_id FROM request_observer'
      .          ' WHERE request_id=:reqId'
      .   ' UNION SELECT user_id FROM request_parentobserver'
      .          ' WHERE request_id=:reqId'
      . ' ) AS uids'
      . ' LEFT JOIN request_unread AS ru'
      .  ' ON uids.user_id=ru.user_id AND ru.request_id=:reqId'
      . ' WHERE ru.user_id IS NULL AND uids.user_id<>:userId';
    $this->getEntityManager()->getConnection()
      ->executeUpdate($sql,
                      array('reqId' =>$reqId,
                            'userId'=>$userId),
                      array('reqId' =>PDO::PARAM_INT,
                            'userId'=>PDO::PARAM_INT));
  }

  protected function completeFindByUser($dql, $userId, $orderby) {
    if ($orderby) {
      $orderstr = '';
      foreach ($orderby as $sortcol => $direction) {
        $orderstr .= ', r.'.$sortcol.' '.$direction;
      }
      if ($orderstr) {
        $dql .= ' ORDER BY' . substr($orderstr,1);
      }
    }

    $query = $this->getEntityManager()->createQuery($dql);
    $query->setParameter('userId', $userId);
    return $query->getResult();
  }

  public function getCategoriesWithTags($condition_sql, $joinObservers,
                                       $userId, $groupId, $requestId=false)
  {
    $obs_join = '';
    if ($joinObservers) {
      $obs_join = ' LEFT JOIN request_observer AS o ON o.request_id=r.id'
        . ' LEFT JOIN request_parentobserver AS op ON op.request_id=r.id';
    }

    if ($userId) {
      $param_values = array('userId'=>$userId);
      $param_types  = array('userId'=>PDO::PARAM_INT);
    }
    if ($requestId) {
      $param_values = array('requestId'=>$requestId);
      $param_types  = array('requestId'=>PDO::PARAM_INT);
    }

    $sql = 'SELECT DISTINCT c.id AS category_id, c.name AS category,'
      . ' t.id, t.name, r.id AS req_id'
      . ' FROM category AS c';
    if ($groupId) {
      $param_values['groupId'] = $groupId;
      $param_types['groupId']  = PDO::PARAM_INT;
      $sql .=
          ' INNER JOIN cgroup_category AS gc ON gc.category_id=c.id'
        .   ' AND gc.cgroup_id=:groupId'
        . ' LEFT JOIN (tag AS t'
        .   ' INNER JOIN request_tag AS rt ON rt.tag_id=t.id'
        .   ' INNER JOIN request AS r ON r.id=rt.request_id'
        .   $obs_join
        .   ') ON t.category_id=c.id'
        .   ' AND ' . $condition_sql
        . ' ORDER BY gc.corder ASC, ';
    } else {
      $sql .=
          ' INNER JOIN tag AS t ON t.category_id=c.id'
        . ' INNER JOIN request_tag AS rt ON rt.tag_id=t.id'
        . ' INNER JOIN request AS r ON r.id=rt.request_id'
        . $obs_join
        . ' WHERE ' . $condition_sql
        . ' ORDER BY ';
    }
    $sql .= 'c.name ASC, t.name ASC';

    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    $categories = array();
    $categories_all = array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $category_id = $row['category_id'];
      $category = $row['category'];
      if (!array_key_exists($category_id, $categories_all)) {
        $category_obj = new Category($category_id);
        $category_obj->setName($category);
        $categories_all[$category_id] = $category_obj;
      }
      $req_id = $row['req_id'];
      if ($req_id) {
        if (!array_key_exists($req_id, $categories)) {
          $categories[$req_id] = array();
        }
        if (!array_key_exists($category_id, $categories[$req_id])) {
          $category_obj = new Category($category_id);
          $category_obj->setName($category);
          $categories[$req_id][$category_id] = $category_obj;
        }else {
          $category_obj = $categories[$req_id][$category_id];
        }
        $tag = new Tag($row['id']);
        $tag->setName($row['name']);
        $category_obj->addTag($tag);
      }
    }
    $categories['all'] = $categories_all;
    return $categories;
  }

  public function getUnread($condition_sql, $joinObservers, $userId, $requestId=false)
  {
    $obs_join = '';
    if ($joinObservers) {
      $obs_join = ' LEFT JOIN request_observer AS o ON o.request_id=r.id'
        . ' LEFT JOIN request_parentobserver AS op ON op.request_id=r.id';
    }
    
    $sql = 'SELECT r.id FROM request AS r'
      . ' INNER JOIN request_unread AS ru ON r.id=ru.request_id'
      . $obs_join
      . ' WHERE ru.user_id=:userId AND ' . $condition_sql;

    $param_values = array('userId'=>$userId);
    $param_types  = array('userId'=>PDO::PARAM_INT);
    if ($requestId) {
      $param_values['requestId'] = $requestId;
      $param_types['requestId']  = PDO::PARAM_INT;
    }

    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    $unread = array();
    while ($row = $stmt->fetch(PDO::FETCH_COLUMN)) {
      $unread[$row] = true;
    }
    return $unread;
  }

  public function findPendingByMe($userId, $orderby) {
    $qstr = $this->dqlSelect
      . " WHERE (r.requestor=:userId AND r.phasePendingBy='R')"
      . " OR (r.performer=:userId AND r.phasePendingBy='P')";
    return $this->completeFindByUser($qstr, $userId, $orderby);
  }
  public function getSqlPendingByMe() {
    $csql = "((r.requestor_id=:userId AND r.phase_pending_by='R')"
      . " OR (r.performer_id=:userId AND r.phase_pending_by='P'))";
    return array($csql, false);
  }

  public function findPendingByOthers($userId, $orderby) {
    $qstr = $this->dqlSelect
      . ' WHERE (r.performer=:userId AND r.requestor!=:userId'
      .    " AND r.phasePendingBy='R')"
      . ' OR (r.requestor=:userId AND r.performer!=:userId'
      .    " AND r.phasePendingBy='P')";
    return $this->completeFindByUser($qstr, $userId, $orderby);
  }
  public function getSqlPendingByOthers() {
    $csql = '((r.performer_id=:userId AND r.requestor_id!=:userId'
      .    " AND r.phase_pending_by='R')"
      . " OR (r.requestor_id=:userId AND r.performer_id!=:userId"
      .    " AND r.phase_pending_by='P'))";
    return array($csql, false);
  }

  public function findAllActive($userId, $orderby) {
    $qstr = $this->dqlSelect
      . ' WHERE (r.requestor=:userId OR r.performer=:userId'
      . ' OR EXISTS (SELECT r2.id FROM FSpires\CommitKeeperBundle\Entity\Request AS r2 LEFT JOIN r2.observers o LEFT JOIN r2.parentObservers op WHERE r2.id=r.id AND (o=:userId OR op=:userId)))'
      . " AND r.phasePendingBy!=''";
    return $this->completeFindByUser($qstr, $userId, $orderby);
  }
  public function getSqlAllActive() {
    $csql = '(r.requestor_id=:userId OR r.performer_id=:userId'
      . " OR o.user_id=:userId OR op.user_id=:userId) AND r.phase_pending_by!=''";
    return array($csql, true);
  }

  public function findClosed($userId, $orderby) {
    $qstr = $this->dqlSelect
      . ' WHERE (r.requestor=:userId OR r.performer=:userId'
      . ' OR EXISTS (SELECT r2.id FROM FSpires\CommitKeeperBundle\Entity\Request AS r2 LEFT JOIN r2.observers o LEFT JOIN r2.parentObservers op WHERE r2.id=r.id AND (o=:userId OR op=:userId)))'
      . " AND r.phasePendingBy=''";
    return $this->completeFindByUser($qstr, $userId, $orderby);
  }
  public function getSqlClosed() {
    $csql = '(r.requestor_id=:userId OR r.performer_id=:userId'
      . " OR o.user_id=:userId OR op.user_id=:userId) AND r.phase_pending_by=''";
    return array($csql, true);
  }

  public function findDailyReport($userId, $orderby) {
    $qstr = $this->dqlSelect
      . " WHERE (r.requestor=:userId OR r.performer=:userId) AND r.phasePendingBy!=''"
      . " AND r.dueDate <= DATE_ADD(CURRENT_DATE(), 7, 'day')";
    return $this->completeFindByUser($qstr, $userId, $orderby);
  }
  public function getSqlDailyReport() {
    $csql = "(r.requestor_id=:userId OR r.performer_id=:userId) AND r.phase_pending_by!=''"
      . ' AND r.due_date <= DATE_ADD(CURRENT_DATE(), INTERVAL 7 DAY)';
    return array($csql, false);
  }

  public function getSqlSupporting() {
    $csql = 'r.super_request_id=:requestId';
    return array($csql, false);
  }


  /**
   * A count of the different requests.
   * Used on the buttons in the sidebar
   *
   * Remember to update this function of you change any of the other
   * find functions above
   *
   * @param int $userId Id of the user
   * @throws \Doctrine\DBAL\DBALException
   * @throws \ErrorException
   * @return array data to display a count of the requests
   */
  public function getCountData($userId) {
    $conn = $this->getEntityManager()->getConnection();
    $query = 'SELECT'
      ." COUNT(CASE WHEN (requestor_id=:userId AND phase_pending_by='R')" 
      .  " OR (performer_id=:userId AND phase_pending_by='P')"
      .  ' THEN 1 END) AS ' . RS::PendingByMe
      .",COUNT(CASE WHEN (performer_id=:userId AND requestor_id!=:userId AND phase_pending_by='R')"
      .  " OR (requestor_id=:userId AND performer_id!=:userId AND phase_pending_by='P')"
      .  ' THEN 1 END) AS ' . RS::PendingByOthers
      .",COUNT(CASE WHEN phase_pending_by!='' THEN 1 END) AS " . RS::AllActive
      .",COUNT(CASE WHEN phase_pending_by='' THEN 1 END) AS " . RS::Closed
      .' FROM request'
      .' WHERE requestor_id=:userId OR performer_id=:userId'
      . ' OR id IN (SELECT request_id FROM request_observer'
      .    ' WHERE user_id=:userId UNION'
      .   ' SELECT request_id FROM request_parentobserver'
      .    ' WHERE user_id=:userId)';

    $stmt = $conn->executeQuery($query,
                                array('userId'=>$userId),
                                array('userId'=>PDO::PARAM_INT));
    if (!$data = $stmt->fetch(PDO::FETCH_ASSOC)) {
      throw new \ErrorException('Not able to count the data in the database');
    }
    return $data;
  }

  public function setEmptyTagCache($id)
  {
    $this->setEntEmptyTagCache('request', $id);
  }

  /**
   * Find categories and tags that belongs to a request.
   *
   * Returns a collection of categories with the category ids as keys.
   * Each category has a sub-collection of tags.
   * If a group_id is given, the categories of the group comes first
   * and in the same order as in the group.
   *
   * @param integer $request_id The id of the Request
   * @param integer $group_id Optional, the id of the Group
   * @return ArrayCollection of categories with tags
   */
  public function findCategories($request_id, $group_id=null)
  {
    return $this->findEntCategories('request', $request_id, $group_id);
  }

  /**
   * Save tags that belong to a request
   * @param integer $request_id The id of the Request
   * @param ArrayCollection $categories The categories with tags to save
   */
  public function saveTags($request_id, $categories)
  {
    $this->saveEntTags('request', $request_id, $categories, false, false);
  }

  /**
   * Find commitments that has gone past their due date and has not yet been flagged as off track
   */
  public function findPastDueDate()
  {
    $dql = $this->dqlSelect . ' WHERE r.dueDate<CURRENT_DATE() AND r.phase=\'' . Phase::Delivery . "'"
        . ' AND t.id!=' . TL::OffTrack;
    $query = $this->getEntityManager()->createQuery($dql);
    return $query->getResult();
  }
}
