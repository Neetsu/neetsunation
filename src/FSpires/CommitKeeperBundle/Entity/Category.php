<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FSpires\CommitKeeperBundle\Entity\Tag;

/**
 * FSpires\CommitKeeperBundle\Entity\Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string $name
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;

  /**
   * @var Doctrine\Common\Collections\ArrayCollection $tags
   *
   * We get these tags manually, using the repository class EntityTagRepository
   * (It became too comlicated to let doctrine handle this
   * association.)
   */
  private $tags;


  public function __construct($id=null) {
    $this->id = $id;
    $this->tags = new ArrayCollection();
  }

  /**
   * Set Id
   *
   * @param integer $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * Get Id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Get name
   *
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * Get tags that belong to this group and category
   *
   * @return \Doctrine\Common\Collections\ArrayCollection
   */
  public function getTags()
  {
    return $this->tags;
  }

  /**
   * Clear the tags so that we have no tags in this category
   *
   */
  public function clearTags()
  {
    $this->tags = new ArrayCollection();
  }


  /**
   * Add a tag to this category
   * @param \FSpires\CommitKeeperBundle\Entity\Tag $tag to add
   */
  public function addTag(Tag $tag)
  {
    $tag->setCategory($this);
    $this->tags->add($tag);
  }

  /**
   * Get just one tag from this category
   *
   * @return Tag
   */
  public function getTag()
  {
    return $this->tags->first();
  }

  /**
   * Set first tag on this category
   */
  public function setTag(Tag $tag=null)
  {
    if ($tag) {
      $tag->setCategory($this);
      $this->tags->set(0,$tag);
    } else {
      $this->tags->clear();
    }
  }
}
