<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\Action;
use FSpires\CommitKeeperBundle\Model\Action\ActionInterface;
use FSpires\CommitKeeperBundle\Entity\TrafficLight;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;
use FSpires\CommitKeeperBundle\Model\TrafficLightFactoryInterface;
use FSpires\CommitKeeperBundle\Entity\UserName;
use FSpires\CommitKeeperBundle\Entity\UserBase;

/**
 * FSpires\CommitKeeperBundle\Entity\Request
 *
 * @ORM\Table(name="request")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\RequestRepository")
 * @Assert\Callback(methods={"validateDueDate"})
 */
class Request
{
    /**
     * @var string $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $title
     * @Assert\NotBlank(message="Please enter a title")
     * @ORM\Column(name="title", type="string", length=200, nullable=false)
     */
    private $title;

    /**
     * @var TrafficLight $trafficLight
     *
     * Icon to display what state the request is in
     *
     * @ORM\ManyToOne(targetEntity="TrafficLight", fetch="EAGER")
     * @ORM\JoinColumn(name="traffic_light", referencedColumnName="id")
     */
    private $trafficLight;


    /**
     * @var object $trafficLightFactory;
     *
     * The service that gives new traffic light objects
     * (Not saved in the database)
     */
    private $trafficLightFactory;

    /**
     * @var integer $statusUpdate
     *
     * The performer wants to update to this status.
     * Not saved in DB, only used for input.
     */
    private $statusUpdate = false;

    /**
     * @var string $phase
     *
     * @ORM\Column(name="phase", type="string", length=20, nullable=false)
     */
    private $phase;

    /**
     * Array of how to display the step that is active now
     * @var array $activeStep
     *
     * Calculated from $this->phase, not saved in DB.
     */
    private $activeStep = false;

    /**
     * @var string $phasePendingBy
     *
     * @ORM\Column(name="phase_pending_by", type="string", length=1, nullable=false)
     */
    private $phasePendingBy;

    /**
     * @var string $priority
     *
     * @ORM\Column(name="priority", type="string", length=10, nullable=false)
     */
    private $priority;


    /**
     * @var \FSpires\CommitKeeperBundle\Entity\UserName $initiator
     *
     * @ORM\ManyToOne(targetEntity="UserName")
     * @ORM\JoinColumn(name="initiator_id", referencedColumnName="id")
     */
    private $initiator;


    /**
     * @var \FSpires\CommitKeeperBundle\Entity\UserName $requestor
     *
     * @ORM\ManyToOne(targetEntity="UserName", fetch="EAGER")
     * @ORM\JoinColumn(name="requestor_id", referencedColumnName="id")
     * @Assert\NotNull(message="Invalid Requestor id")
     * @Assert\Valid
     */
    private $requestor;

    /**
     * @var \FSpires\CommitKeeperBundle\Entity\UserName $performer
     *
     * @ORM\ManyToOne(targetEntity="UserName", fetch="EAGER")
     * @ORM\JoinColumn(name="performer_id", referencedColumnName="id")
     * @Assert\NotNull(message="Invalid Performer id")
     * @Assert\Valid
     */
    private $performer;

    /**
     * @var ArrayCollection $observers
     *
     * @ORM\ManyToMany(targetEntity="UserName")
     * @ORM\JoinTable(name="request_observer",
     *      joinColumns={@ORM\JoinColumn(name="request_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id")}
     *      )
     */
    private $observers;

    /**
     * @var ArrayCollection $parentObservers
     *
     * All observers that are added (if this is a supporting request)
     * because they are a requester in one of the parent requests.
     *
     * @ORM\ManyToMany(targetEntity="UserName")
     * @ORM\JoinTable(name="request_parentobserver",
     *      joinColumns={@ORM\JoinColumn(name="request_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id")}
     *      )
     */
    private $parentObservers;

    /**
     * The due date stored in the database
     * @var \DateTime $dueDate
     *
     * @ORM\Column(name="due_date", type="date", nullable=false)
     */
    private $dueDate;

    /**
     * @var boolean $has_budget_field
     *
     * @ORM\Column(name="has_budgetf", type="boolean", nullable=false)
     */
    private $hasBudgetField;

    /**
     * The budget stored in the database
     * @var string $budget
     *
     * @ORM\Column(name="budget", type="string", length=25, nullable=true)
     */
    private $budget;

    /**
     * @var string $budgetUnit;
     * @ORM\ManyToOne(targetEntity="BudgetUnit", fetch="EAGER")
     * @ORM\JoinColumn(name="budgetf_unit_id", referencedColumnName="id")
     */
    private $budgetUnit;

    /**
     * @var date $newDueDate
     * Temporary store for a new due date before
     * it is stored in the database
     */
    private $newDueDate;

    /**
     * @var string $newBudget
     *
     * Temporary store for a new budget
     * before it is stored in the database
     */
    private $newBudget = null;

    /**
     * True if we shall send email to the user that do an action
     * This is not saved in the database, but set each time an action
     * is posted.
     *
     * @var bool
     */
    private $CCme;

    /**
     * @var string $superRequestId
     *
     * @ORM\Column(name="super_request_id", type="integer", nullable=true)
     */
    private $superRequestId;

    /**
     * @var ArrayCollection $supportingRequests
     *
     * Read only. The collection of supporting requests that
     * have this request as the super request
     */
    private $supportingRequests;

    /**
     * @var ArrayCollection $attachments
     *
     * A collection of attachments and links that belong to this commitment.
     *
     * @ORM\OneToMany(targetEntity="Attachment", mappedBy="request")
     */
    private $attachments;

    /**
     * @var ArrayCollection $newAttachments
     *
     * A collection of attachments and links that are being added.
     *
     * @Assert\Valid(traverse="true")
     */
    private $newAttachments;

    /**
     * @var ArrayCollection $categories
     *
     * A collection of categories with the category ids as keys.
     * Each category has a sub-collection of tags.
     * For each user that is a member of a group, the categories
     * are in the same order as in the group.
     * (This relationship is too complex to be handled automatically
     * by doctrine, so we have to remember to retrive and save this
     * field in the database with our own code.)
     */
    private $categories;

    /**
     * A property just to store the action when inputing
     * new data from a form
     * (This is not stored in tha database on the request table,
     * but is instead stored in the action_history.)
     */
    private $action;

    /**
     * A property just to store a description when inputing
     * new data from a form
     * (This is not stored in tha database on the request table,
     * but is instead stored in the action_history.)
     * @Assert\NotBlank(message="Please enter some description")
     */
    private $description;

    /**
     * Stores the users group id
     * This is not stored in the database, but set according to each user.
     * It is used to get the right order of the tag categories.
     */
    private $group_id;

    /**
     * @var EntityManagerInterface $em Doctrine\ORM\Entitymanager
     * Not saved in the database
     */
    private $em;

    /**
     * @var RequestRepository $repository The repository that belongs
     *                                    to this class
     * Not saved in the database
     */
    private $repository;

    public function __construct()
    {
      $this->observers = new ArrayCollection();
      $this->parentObservers = new ArrayCollection();
      $this->attachments = new ArrayCollection();
      $this->newAttachments = new ArrayCollection();
    }

    public function __clone()
    {
        $newAtts = array();
        if (!empty($this->newAttachments)) {
            foreach ($this->newAttachments as $att) {
                $att->setFileAttributes();
                $newAtts[] = clone $att;
            }
        }
        $this->newAttachments = new ArrayCollection($newAtts);
    }

    /**
     * Check that ve have a valid newDueDate before any
     * form can be submitted.
     */
    public function validateDueDate($ec)
    {
      // Check action first
      switch ($this->action) {
        // Only check the due date for these actions
      case Action::Make:
      case Action::Amend:
      case Action::AmendToDo:
      case Action::Revise:
        break;
      default:
        $this->newDueDate = $this->dueDate; // Just to make sure
        return;
      }

      if (empty($this->newDueDate)) {
        $ec->addViolationAt('newDueDate', 'commitment.emptyDate');
        return;
      }
      if (!($this->newDueDate instanceof \DateTime)) {
        throw new UnexpectedTypeException($this->newDueDate, 'DateTime');
      }
      if (Action::AmendToDo == $this->action &&
          $this->newDueDate == $this->dueDate) {
        return;
      }
      // Check that it is a future date
      if ($this->newDueDate < new \DateTime('today')) {
        $DateFormat = 'M j, Y';
        $ec->addViolationAt('newDueDate', 'commitment.pastDate%date%',
               array('%date%' => $this->newDueDate->format($DateFormat)));
      }
    }

    /**
     * Initialize a new request that is about to be made
     */
    public function initNewReqest($type, $user, $em) {
      $userName = UserName::filter($user, $em);
      $this->initiator = $userName;
      $this->phase = Phase::Preparation;
      $this->setTrafficLight(TL::Preparation);
      $this->action = Action::Make;
      switch ($type) {
      case RequestType::Request:
        $this->requestor = $userName;
        //$this->performer = $user->getLastPerformer();
        $this->phasePendingBy = 'R';
        break;
      case RequestType::Offer:
        //$this->requestor = $user->getLastRequestor();
        $this->performer = $userName;
        $this->phasePendingBy = 'P';
        break;
      case RequestType::ToDo:
        $this->requestor = $userName;
        $this->performer = $userName;
        $this->phasePendingBy = 'R';
        break;
      default:
        throw new \InvalidArgumentException('Invalid New Request type: '. $type);
      }

      // Set an initial priority
      $this->priority = 1;

      // Set a default due date
      $this->newDueDate = null;

      $this->group_id = $user->getGroup()->getId();
      $this->em = $em;
    }

    /**
     * Set Entitymanager
     *
     * @param EntityManagerInterface $em Doctrine\ORM\Entitymanager
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
      $this->em = $em;
    }

    /**
     * Get Repository
     *
     * @return RequestRepository
     */
    public function getRepository()
    {
      if (!$this->repository) {
        if (!$this->em) {
          throw new \RuntimeException('EntityManager is not set in the Request object');
        }
        $this->repository = $this->em->getRepository('CKBundle:Request');
      }
      return $this->repository;
    }

    /**
     * Get requestId
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set a TrafficLightFactory that is used when setting the traffic light
     */
    public function setTrafficLightFactory(TrafficLightFactoryInterface $tlf) {
      $this->trafficLightFactory = $tlf;
    }

  /**
   * Set trafficLight
   *
   * @param TrafficLight|integer $trafficLight
   *   (integer must be Enum \FSpires\CommitKeeperBundle\Model\Enum\TrafficLight)
   * @throws \UnexpectedValueException
   */
    public function setTrafficLight($trafficLight)
    {
      if ($trafficLight instanceof TrafficLight) {
        $this->trafficLight = $trafficLight;
        return;
      }
      if (!$this->trafficLightFactory) {
        throw new \UnexpectedValueException(
          'Request->setTrafficLightFactory() must be called first before setting the traffic light'
        );
      }
      $this->trafficLight = $this->trafficLightFactory->getTrafficLight($trafficLight);
    }

    /**
     * Update trafficLight
     * Set new trafficlight if statusUpdate is set
     */
    public function updateTrafficLight()
    {
      if ($this->statusUpdate) {
        $this->setTrafficLight($this->statusUpdate);
      }
    }

    /**
     * Get trafficLight
     *
     * @return TrafficLight
     */
    public function getTrafficLight()
    {
        return $this->trafficLight;
    }

    /**
     * Set statusUpdate
     *
     * @param \FSpires\CommitKeeperBundle\Model\Enum\TrafficLight $statusUpdate
     */
    public function setStatusUpdate($statusUpdate)
    {
      $this->statusUpdate = $statusUpdate;
    }

    /**
     * Get statusUpdate
     *
     * @return \FSpires\CommitKeeperBundle\Model\Enum\TrafficLight
     */
    public function getStatusUpdate()
    {
        return $this->statusUpdate;
    }

    /**
     * Set action
     *
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }


    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set priority
     *
     * @param string $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set phase
     *
     * @param string $phase
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;
        $this->activeStep = false;
    }

    /**
     * Get phase
     *
     * @return string 
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * Get css class for given phase step marker.
     * Phase steps are given as numbers.
     * @param integer $stepNum Step number to provide css class for
     * @return string          CSS class to apply to the step marker
     */
    public function activeStep($stepNum)
    {
      if (!$this->activeStep) {
        $dots = array(1=>'',2=>'',3=>'',4=>'');
        switch ($this->phase) {
        case Phase::Acknowledge:
          $dots[3] ='silver';
        case Phase::Delivery:
          $dots[2] ='green';
        case Phase::Negotiation:
          $dots[1] ='blue';
          break;
        case Phase::Closed:
          switch ($this->trafficLight->getId()) {
          case TL::Completed:
            $dots = array(1=>'gold',2=>'gold',3=>'gold',4=>'gold');
            break;
          case TL::DeclinedRequest:
          case TL::DeclinedOffer:
          case TL::DeclinedCounteredRequest:
          case TL::DeclinedCounteredOffer:
            $dots[1] ='blue';
          }
        }
        $this->activeStep = $dots;
      }

      $color = $this->activeStep[$stepNum];
      if ($color) {
        return ' active-step-' . $color;
      }
      return '';
    }

    /**
     * Set newDueDate
     *
     * @param date $newDueDate
     */
    public function setNewDueDate($newDueDate)
    {
        $this->newDueDate = $newDueDate;
    }

    /**
     * Get newDueDate
     *
     * @return \DateTime
     */
    public function getNewDueDate()
    {
        return $this->newDueDate;
    }

    /**
     * Set dueDate to the new due date
     */
    public function updateDueDate()
    {
        $this->dueDate = $this->newDueDate;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Get delivered date
     *
     * @return bool|\DateTime
     */
    public function getDelivered()
    {
      switch ($this->trafficLight->getId()) {
      case TL::Delivered:
      case TL::Completed:
        break;
      default:
        return false;
      }

      return $this->getRepository()->findDeliveredDate($this->id);
    }

    /**
     * Set newBudget
     *
     * @param string $newBudget
     */
    public function setNewBudget($newBudget)
    {
        $this->newBudget = $newBudget;
    }

    /**
     * Get newBudget
     *
     * @return string 
     */
    public function getNewBudget()
    {
        return $this->newBudget;
    }

    /**
     * Set budget to the new budget
     */
    public function updateBudget()
    {
      if ($this->hasBudgetField) {
        $this->budget = $this->newBudget;
      }
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set requestor
     *
     * @param \FSpires\CommitKeeperBundle\Entity\UserName $requestor
     */
    public function setRequestor(UserName $requestor)
    {
      $this->requestor = $requestor;
    }

    /**
     * Get requestor
     *
     * @return \FSpires\CommitKeeperBundle\Entity\UserName
     */
    public function getRequestor()
    {
      return $this->requestor;
    }

    /**
     * Set performer
     *
     * @param \FSpires\CommitKeeperBundle\Entity\UserName $performer
     */
    public function setPerformer($performer)
    {
      $this->performer = $performer;
    }

    /**
     * Get performer
     *
     * @return \FSpires\CommitKeeperBundle\Entity\UserName
     */
    public function getPerformer()
    {
      return $this->performer;
    }


    /**
     * Set initiator
     *
     * @param \FSpires\CommitKeeperBundle\Entity\UserName $initiator
     */
    public function setInitiator(UserName $initiator)
    {
      $this->initiator = $initiator;
    }

    /**
     * Get initiator
     *
     * @return \FSpires\CommitKeeperBundle\Entity\UserName
     */
    public function getInitiator()
    {
      return $this->initiator;
    }

    /**
     * Set phasePendingBy
     *
     * @param string $phasePendingBy
     */
    public function setPhasePendingBy($phasePendingBy)
    {
      $this->phasePendingBy = $phasePendingBy;
    }

    /**
     * Get phasePendingBy
     *
     * @return string 
     */
    public function getPhasePendingBy()
    {
      return $this->phasePendingBy;
    }

    /**
     * Set CCme
     *
     * @param boolean $CCme
     */
    public function setCCme($CCme)
    {
      $this->CCme = (boolean)$CCme;
    }

    /**
     * Get CCme
     *
     * @return boolean
     */
    public function getCCme()
    {
      return $this->CCme;
    }

    /**
     * Set superRequestId
     *
     * @param integer $superRequestId
     */
    public function setSuperRequestId($superRequestId)
    {
      $this->superRequestId = $superRequestId;
    }

    /**
     * Get superRequestId
     *
     * @return integer 
     */
    public function getSuperRequestId()
    {
      return $this->superRequestId;
    }

    /**
     * Get child requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupportingRequests()
    {
      if (!$this->supportingRequests) {
        $this->supportingRequests =
          $this->getRepository()->findBy(array('superRequestId'=>$this->id));
      }
      return $this->supportingRequests;
    }

    /**
     * Update attachments
     *
     */
    public function updateAttachments(EntityManagerInterface $em)
    {
      foreach ($this->attachments as $index => $att) {
        if ($att->getDelete()) {
          $em->remove($att);
          $this->attachments->remove($index);
        } else {
          if (!$att->getEdit()) {
            $em->detach($att);
            $this->attachments->remove($index);
          }
        }
      }
      foreach ($this->newAttachments as $att) {
        $this->addAttachment($att);
        $em->persist($att);
      }
      $this->newAttachments->clear();
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachments()
    {
      return $this->attachments;
    }

    /**
     * Add attachment
     *
     * @param \FSpires\CommitKeeperBundle\Entity\Attachment $attachment
     */
    public function addAttachment(Attachment $attachment)
    {
      $attachment->setRequest($this);
      $this->attachments[] = $attachment;
    }

    /**
     * Get new attachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewAttachments()
    {
      return $this->newAttachments;
    }

    public function setNewAttachments($atts)
    {
      if (!($atts instanceof Collection)) {
        $atts = new ArrayCollection($atts);
      }
      $this->newAttachments = $atts;
    }


    /**
     * Add an observer
     *
     * @param \FSpires\CommitKeeperBundle\Entity\UserName $observer
     */
    public function addObserver(UserName $observer)
    {
      $this->observers[] = $observer;
    }

    /**
     * Get observers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getObservers()
    {
      return $this->observers;
    }

    /**
     * Get observers
     *
     * @return array
     */
    public function getAllObservers()
    {
      return array_merge($this->parentObservers->toArray(),
                         $this->observers->toArray());
    }

    /**
     * Get comma separated list of observers as a string
     *
     * @param TranslatorInterface $translator to translate the string 'and'
     * @return string|null
     */
    public function getObserverList(TranslatorInterface $translator)
    {
      $observers = $this->getAllObservers();
      $list = null;
      if (count($observers)) {
        $observer = array_pop($observers);
        $list = $observer->getName();
        if (count($observers)) {
          $list_first = '';
          foreach ($observers as $observer) {
            $list_first .= ', ' . $observer->getName();
          }
          $list_first = substr($list_first,2);
          $and = $translator->trans('and');
          $list = $list_first . ' ' . $and . ' ' . $list;
        }
      }
      return $list;
    }

    /**
     * Add an observer coming from a parent request
     *
     * @param \FSpires\CommitKeeperBundle\Entity\UserName $observer
     */
    public function addParentObserver(UserName $observer)
    {
      $this->parentObservers[] = $observer;
    }

    /**
     * Get observers coming from parent requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParentObservers()
    {
      return $this->parentObservers;
    }

    /**
     * Set Id of the Group the current user belongs to
     *
     * @param integer $group_id
     */
    public function setGroupId($group_id) {
      $this->group_id = $group_id;
    }

    /**
     * Set hasBudgetField
     *
     * @param boolean $hasBudgetField
     * @return Group
     */
    public function setHasBudgetField($hasBudgetField)
    {
      $this->hasBudgetField = (boolean)$hasBudgetField;
    }

    /**
     * hasBudgetField
     *
     * @return boolean 
     */
    public function hasBudgetField()
    {
      return $this->hasBudgetField;
    }

    /**
     * Set budgetUnit
     *
     * @param BudgetUnit $budgetUnit
     */
    public function setBudgetUnit(BudgetUnit $budgetUnit)
    {
      $this->budgetUnit = $budgetUnit;
    }

    /**
     * Get budgetUnit
     *
     * @return BudgetUnit 
     */
    public function getBudgetUnit()
    {
      return $this->budgetUnit;
    }

    /**
     * Set the categories and tags that belong to this request
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $group_id
     * @param boolean $new True if this is a new set of tags not
     *                     gotten from the database
     */
    public function setCategories($categories, $new=false) {
      $this->categories = $categories;
      if ($new) {
        $this->getRepository()->setEmptyTagCache($this->id);
      }
    }

    /**
     * Get the catagories and tags that belong to this request
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories($group_id=null)
    {
      if ($this->categories) {
        return $this->categories;
      }
      if ($group_id) {
        $this->group_id = $group_id;
      }
      $this->categories = $this->getRepository()->
                          findCategories($this->id, $this->group_id);
      return $this->categories;
    }

    /**
     * Save the tags connected to this request
     */
    public function saveTags()
    {
      $this->getRepository()->saveTags($this->id, $this->categories);
    }

    /**
     * Determine if a user is allowed to see this commitment
     *
     * @param UserBase $user
     * @return bool
     */
    public function canBeSeenBy(UserBase $user)
    {
      $userId = $user->getId();
      if ($this->performer->getId()==$userId) {
        return true;
      }
      if ($this->requestor->getId()==$userId) {
        return true;
      }
      foreach ($this->parentObservers as $observer) {
        if ($observer->getId()==$userId) {
          return true;
        }
      }
      foreach ($this->observers as $observer) {
        if ($observer->getId()==$userId) {
          return true;
        }
      }
      return false;
    }
}
