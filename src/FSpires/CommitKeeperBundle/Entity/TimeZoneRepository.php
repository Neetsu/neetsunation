<?php

namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class TimeZoneRepository
{
  public static $timeZones = array(
    -660 =>
      array (
        'label' => '(GMT -11:00) Midway, Niue, American Samoa',
        'offset' => -11,
        'zone' => 'Pacific/Midway',
      ),
    -600 =>
      array (
        'label' => '(GMT -10:00) Hawaii-Aleutian Standard Time, Tahiti, Cook Islands',
        'offset' => -10,
        'zone' => 'Pacific/Honolulu',
      ),
    -570 =>
      array (
        'label' => '(GMT -9:30) Marquesas Islands Time',
        'offset' => -9.5,
        'zone' => 'Pacific/Marquesas',
      ),
    -540 =>
      array (
        'label' => '(GMT -9:00) Alaska Standard Time, Gambier Time',
        'offset' => -9,
        'zone' => 'America/Anchorage',
      ),
    -480 =>
      array (
        'label' => '(GMT -8:00) Pacific Standard Time, Pitcairn',
        'offset' => -8,
        'zone' => 'America/Los_Angeles',
      ),
    -420 =>
      array (
        'label' => '(GMT -7:00) Mountain Standard Time',
        'offset' => -7,
        'zone' => 'America/Denver',
      ),
    -360 =>
      array (
        'label' => '(GMT -6:00) Central Standard Time, Central America',
        'offset' => -6,
        'zone' => 'America/Chicago',
      ),
    -300 =>
      array (
        'label' => '(GMT -5:00) Eastern Standard Time, Ecuador, Colombia, Peru',
        'offset' => -5,
        'zone' => 'America/New_York',
      ),
    -270 =>
      array (
        'label' => '(GMT -4:30) Venezuelan Standard Time',
        'offset' => -4.5,
        'zone' => 'America/Caracas',
      ),
    -240 =>
      array (
        'label' => '(GMT -4:00) Atlantic Standard Time, Chile, Bolivia, Paraguay, West Brazil',
        'offset' => -4,
        'zone' => 'America/La_Paz',
      ),
    -210 =>
      array (
        'label' => '(GMT -3:30) Newfoundland Standard Time',
        'offset' => -3.5,
        'zone' => 'America/St_Johns',
      ),
    -180 =>
      array (
        'label' => '(GMT -3:00) Brasília Time (BRT), Argentina Time, Nuuk Greenland',
        'offset' => -3,
        'zone' => 'America/Sao_Paulo',
      ),
    -120 =>
      array (
        'label' => '(GMT -2:00) Mid-Atlantic (South Georgia, Noronha)',
        'offset' => -2,
        'zone' => 'Atlantic/South_Georgia',
      ),
    -60 =>
      array (
        'label' => '(GMT -1:00) Azores Standard Time, Cape Verde Time, Eastern Greenland',
        'offset' => -1,
        'zone' => 'Atlantic/Azores',
      ),
    0 =>
      array (
        'label' => '(GMT) Western European Time, Western Sahara Standard Time',
        'offset' => 0,
        'zone' => 'Europe/London',
      ),
    60 =>
      array (
        'label' => '(GMT +1:00) Central European Time, West Africa Time',
        'offset' => 1,
        'zone' => 'Europe/Paris',
      ),
    120 =>
      array (
        'label' => '(GMT +2:00) Eastern European Time, Israel, Central Africa Time',
        'offset' => 2,
        'zone' => 'Africa/Harare',
      ),
    180 =>
      array (
        'label' => '(GMT +3:00) Belarus, Arabia Standard Time, East Africa Time',
        'offset' => 3,
        'zone' => 'Africa/Kampala',
      ),
    210 =>
      array (
        'label' => '(GMT +3:30) Iran Standard Time',
        'offset' => 3.5,
        'zone' => 'Asia/Tehran',
      ),
    240 =>
      array (
        'label' => '(GMT +4:00) Moscow Standard Time, Gulf Standard Time, Mauritius',
        'offset' => 4,
        'zone' => 'Europe/Moscow',
      ),
    270 =>
      array (
        'label' => '(GMT +4:30) Afghanistan Time',
        'offset' => 4.5,
        'zone' => 'Asia/Kabul',
      ),
    300 =>
      array (
        'label' => '(GMT +5:00) Pakistan Standard Time, Uzbekistan, Maldives',
        'offset' => 5,
        'zone' => 'Asia/Karachi',
      ),
    330 =>
      array (
        'label' => '(GMT +5:30) Indian Standard Time, Sri Lanka Standard Time',
        'offset' => 5.5,
        'zone' => 'Asia/Kolkata',
      ),
    345 =>
      array (
        'label' => '(GMT +5:45) Nepal Standard Time',
        'offset' => 5.75,
        'zone' => 'Asia/Kathmandu',
      ),
    360 =>
      array (
        'label' => '(GMT +6:00) Yekaterinburg, Kyrgyzstan, Bangladesh, Bhutan',
        'offset' => 6,
        'zone' => 'Asia/Dhaka',
      ),
    390 =>
      array (
        'label' => '(GMT +6:30) Myanmar Standard Time, Cocos Islands Time',
        'offset' => 6.5,
        'zone' => 'Asia/Rangoon',
      ),
    420 =>
      array (
        'label' => '(GMT +7:00) Omsk Time, Indochina Time, Indonesia Western Time',
        'offset' => 7,
        'zone' => 'Asia/Omsk',
      ),
    480 =>
      array (
        'label' => '(GMT +8:00) Krasnoyarsk, China, Malaysia, Philippines, Indonesia Central, Australia Western',
        'offset' => 8,
        'zone' => 'Asia/Shanghai',
      ),
    525 =>
      array (
        'label' => '(GMT +8:45) Australian Central Western Standard Time',
        'offset' => 8.75,
        'zone' => 'Australia/Eucla',
      ),
    540 =>
      array (
        'label' => '(GMT +9:00) Irkutsk Time, Korea, Japan Standard Time, Indonesia Eastern Time',
        'offset' => 9,
        'zone' => 'Asia/Tokyo',
      ),
    570 =>
      array (
        'label' => '(GMT +9:30) Australian Central Standard Time',
        'offset' => 9.5,
        'zone' => 'Australia/Darwin',
      ),
    600 =>
      array (
        'label' => '(GMT +10:00) Yakutsk Time, Guam, Australian Eastern Standard Time',
        'offset' => 10,
        'zone' => 'Australia/Brisbane',
      ),
    630 =>
      array (
        'label' => '(GMT +10:30) Lord Howe Standard Time (Australia)',
        'offset' => 10.5,
        'zone' => 'Australia/Lord_Howe',
      ),
    660 =>
      array (
        'label' => '(GMT +11:00) Vladivostok Time, Solomon Islands, Vanuatu, New Caledonia',
        'offset' => 11,
        'zone' => 'Asia/Vladivostok',
      ),
    690 =>
      array (
        'label' => '(GMT +11:30) Norfolk Island Time',
        'offset' => 11.5,
        'zone' => 'Pacific/Norfolk',
      ),
    720 =>
      array (
        'label' => '(GMT +12:00) Magadan Time, New Zealand Standard Time, Marshall Islands, Fiji',
        'offset' => 12,
        'zone' => 'Pacific/Fiji',
      ),
    765 =>
      array (
        'label' => '(GMT +12:45) Chatham Island Standard Time',
        'offset' => 12.75,
        'zone' => 'Pacific/Chatham',
      ),
    780 =>
      array (
        'label' => '(GMT +13:00) West Samoa Time, Phoenix Islands Time, Tokelau Time, Tonga',
        'offset' => 13,
        'zone' => 'Pacific/Fakaofo',
      ),
    840 =>
      array (
        'label' => '(GMT +14:00) Line Islands Time (Kiribati)',
        'offset' => 14,
        'zone' => 'Pacific/Kiritimati',
      )
  );

  /**
   * @param int $minutesOffset
   * @return TimeZone|null
   */
  public function findByOffsetInMinutes($minutesOffset)
  {
    if (array_key_exists($minutesOffset, self::$timeZones)) {
      return $this->createFromZoneData(self::$timeZones[$minutesOffset]);
    }
    return null;
  }

  /**
   * @return ArrayCollection
   */
  public function findAll()
  {
    $zones = new ArrayCollection();
    foreach(self::$timeZones as $minutesOffset => $zoneData) {
      $zones->set($minutesOffset, $this->createFromZoneData($zoneData));
    }
    return $zones;
  }

  private function createFromZoneData(array $zoneData)
  {
    return new TimeZone(
      $zoneData['label'],
      $zoneData['zone'],
      $zoneData['offset'],
      $this->computeSlot($zoneData['offset'])
    );
  }

  private function computeSlot($hoursOffset)
  {
    $slot = intval(floor($hoursOffset + 0.52));
    if ($slot > 12) {
      $slot -= 24;
    }
    return $slot;
  }
}
