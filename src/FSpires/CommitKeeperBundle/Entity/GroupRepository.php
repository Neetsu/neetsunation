<?php
namespace FSpires\CommitKeeperBundle\Entity;

use PDO;
use Doctrine\DBAL\Connection as DC;
use Doctrine\Common\Collections\ArrayCollection;
use FSpires\CommitKeeperBundle\Entity\EntityTagRepository;

/**
 * GroupRepository
 */
class GroupRepository extends EntityTagRepository
{
  /**
   * Find all tags that belong to a group.
   *
   * Returns a collection of categories with the category ids as keys.
   * Each category has a sub-collection of tags.
   *
   * @param integer $group_id The id of the Group
   * @return ArrayCollection of tags
   */
  public function findCategories($group_id)
  {
    return $this->findEntCategories('cgroup', $group_id);
  }


  /**
   * Add default categories to a new group
   */
  public function addDefaultCategories($group_id, $from_user_id)
  {
    if (!($group_id>1)) {
      // Do not change group 1, the default group
      return;
    }

    $param_values = array('group_id' => $group_id);
    $param_types  = array('group_id' => PDO::PARAM_INT);
    if ($from_user_id>1) {
      // Insert from the group of the from_user
      $sql = 'INSERT INTO cgroup_category (cgroup_id, category_id, corder) ' .
        'SELECT :group_id, gc.category_id, gc.corder FROM cgroup_category AS gc'
        . ' INNER JOIN user AS u ON gc.cgroup_id=u.cgroup_id'
        . ' WHERE u.id=:from_user_id';
      $param_values['from_user_id'] = $from_user_id;
      $param_types['from_user_id']  = PDO::PARAM_INT;
    } else {
      // Insert from the default group
      $sql = 'INSERT INTO cgroup_category (cgroup_id, category_id, corder) ' .
        'SELECT :group_id, gc.category_id, gc.corder FROM cgroup_category AS gc'
        . ' WHERE gc.cgroup_id=1';
    }
    $conn = $this->getEntityManager()->getConnection();
    $conn->executeUpdate($sql, $param_values, $param_types);
  }

  /**
   * Save tags that belong to a specific group
   * @param integer $group_id The id of the Group
   * @param ArrayCollection $categories The categories with tags to save
   */
  public function saveTags($group_id, $categories)
  {
    throw new \LogicException('Cannot save group tags');
  }

  private function inOtherGroups($conn, $group_id, $gCatId)
  {
    $countSql = 'SELECT COUNT(category_id) FROM cgroup_category'
      . ' WHERE category_id=:category_id AND cgroup_id!=:group_id';
    $stmt = $conn->executeQuery(
                                $countSql,
                                array('category_id' => $gCatId,
                                      'group_id'    => $group_id),
                                array('category_id' => PDO::PARAM_INT,
                                      'group_id'    => PDO::PARAM_INT)
                                );
    return $stmt->fetchColumn();
  }

  /**
   * Save the categories that belong to a specific group
   * @param integer $group_id The id of the Group
   * @param ArrayCollection $gCategories The categories to save
   * @param ArrayCollection $gCategoriesCache The old categories,
   *                                        when they came from the database
   */
  public function saveCategories($group_id, &$gCategories, $gCategoriesCache)
  {
    $conn = $this->getEntityManager()->getConnection();
    $conn->beginTransaction();
    try {
      $doPurge = false;
      $doReorder = false;
      $gCategoriesToDelete = $gCategoriesCache;
      $Names = array();
      $newgCategories = array();

      foreach ($gCategories as $idx => $gCategory) {
        $gCatId = $gCategory->getId();
        $new_cat = false;
        $changed_cat = false;
        $name = trim($gCategory->getName());
        if ($name) {
          if (array_key_exists($name, $Names)) {
            throw new \LogicException(
            'Cannot have two categories with the same name: ' . $name);
          }
          $Names[$name] = 1;
          if ($gCatId && array_key_exists($gCatId, $gCategoriesCache)) {
            // Category exists
            $oldgCat = $gCategoriesCache[$gCatId];
            // Check if it is modified
            if ($name!=$oldgCat->getName()) {
              // Category name is set and modified
              $changed_cat = true;
            }
            if ($gCategory->getOrder()!=$oldgCat->getOrder()) {
              $doReorder = true;
            }
            unset($gCategoriesToDelete[$gCatId]);
          } else {
            $new_cat = true;
          }
        } else {
          unset($gCategories[$idx]);
        }
        if ($new_cat || $changed_cat) {
          $gCategory->setName($name); //trimmed name
          $newgCategories[$name] = array('cat'=>$gCategory,
                                         'existsInDB'=>$changed_cat);
        }
      }

      $gCategoriesToCreate = array();
      $categoriesToUpdate = array();
      $doNotDelete = array();

      foreach ($newgCategories as $nData) {
        $gCategory   = $nData['cat'];
        $existsInDB = $nData['existsInDB'];

        $gCatId = $gCategory->getId();
        $newId = false;
        if (!$existsInDB) {
          $newId = $gCatId;
        }

        $name = $gCategory->getName();

        $update_cat = false;
        $create_cat = false;

        if (!$newId) {
          // Check if the category exists already
          $selectSql = 'SELECT id FROM category WHERE name=:name';
          $stmt = $conn->executeQuery($selectSql,
                                      array('name' => $name),
                                      array('name' => PDO::PARAM_STR)
                                      );
          $newId = $stmt->fetchColumn();
          if ($newId) {
            if (array_key_exists($newId, $gCategoriesCache)) {
              // That catagory will either be renamed or deleted
              // later in this function, since none of the new
              // categories can have the same name.

              // Check if it is shared with another group:
              $count = $this->inOtherGroups($conn, $group_id, $newId);
              if ($count > 0) {
                // We do not delete, but keep this group
                $doNotDelete[$newId] = 1;
                unset($gCategoriesToDelete[$newId]);
              } else {
                $newId = false;
                $create_cat = true;
              }
            }
          } else {
            $create_cat = true;
          }
        }
        if ($existsInDB && $create_cat) {
          // Only create a new category if the old category is
          // shared with other groups
          $count = $this->inOtherGroups($conn, $group_id, $gCatId);
          if ($count>0) {
            // Delete old category (if it is not reused)
            if (!array_key_exists($gCatId, $doNotDelete)) {
              $gCategoriesToDelete[$gCatId] = true;
            }
          } else {
            $create_cat = false;
            $update_cat = true;
          }
        }

        if ($create_cat) {
          $insertSql = 'INSERT INTO category (name) VALUES (:name)';
          $conn->executeUpdate($insertSql,
                               array('name' => $name),
                               array('name' => PDO::PARAM_STR)
                               );
          $newId = $conn->lastInsertId();
        }

        if ($update_cat) {
          $categoriesToUpdate[$gCatId] = $name;
        } else {
          // Got a new category ready to be inserted into the group
          if (!$newId) {
            throw new \LogicException(
                'New category id should have been set at this point');
          }
          $gCategory->setId($newId);
          $gCategoriesToCreate[$gCategory->getName()] = array(
                                              'cat'    => $gCategory,
                                              'exists' => $existsInDB,
                                              'old_id' => $gCatId
                                                              );
        }
      }

      if (count($categoriesToUpdate)>0) {
        $updateSql = 'UPDATE category SET name=:name WHERE id=:cat_id';
        foreach ($categoriesToUpdate as $id => $name) {
          $conn->executeUpdate($updateSql,
                               array('name'   => $name,
                                     'cat_id' => $id),
                               array('name'   => PDO::PARAM_STR,
                                     'cat_id' => PDO::PARAM_INT)
                               );
        }
      }

      foreach ($gCategoriesToCreate as $newgCatData) {
        $newgCat = $newgCatData['cat'];
        $param_values = array('group_id' => $group_id,
                              'cat_id'   => $newgCat->getId(),
                              'order'    => intval($newgCat->getOrder()));
        $param_types  = array('group_id' => PDO::PARAM_INT,
                              'cat_id'   => PDO::PARAM_INT,
                              'order'    => PDO::PARAM_INT);
        if ($newgCatData['exists']) {
          $sql = "UPDATE cgroup_category"
            . " SET category_id=:cat_id, corder=:order"
            . ' WHERE cgroup_id=:group_id AND category_id=:old_cat_id';
          $param_values['old_cat_id'] = $newgCatData['old_id'];
          $param_types['old_cat_id']  = PDO::PARAM_INT;
          $doPurge = true;
        } else {
          $sql = "INSERT INTO cgroup_category"
            . " (cgroup_id, category_id, corder)"
            . ' VALUES (:group_id, :cat_id, :order)';
        }
        $doReorder = true;
        $gCategoriesCache[$newgCat->getId()] = $newgCat;
        $conn->executeUpdate($sql, $param_values, $param_types);
      }

      // Delete categories from DB that are no longer in the collection
      if (count($gCategoriesToDelete)>0) {
        $gCategoriesToDelete = array_keys($gCategoriesToDelete);
        $deleteSql = "DELETE FROM cgroup_category"
          . ' WHERE cgroup_id=:group_id AND category_id IN (:cat_ids)';
        $conn->executeUpdate($deleteSql,
                             array('group_id' => $group_id,
                                   'cat_ids'  => $gCategoriesToDelete),
                             array('group_id' => PDO::PARAM_INT,
                                   'cat_ids'  => DC::PARAM_INT_ARRAY)
                             );
        $doPurge = true;
        $doReorder = true;
      }

      if ($doReorder) {
        $sorted_categories = $gCategories->toArray();
        $sortFunction = function($a, $b) {
          return $a->getOrder() - $b->getOrder();
        };
        uasort($sorted_categories, $sortFunction);
        $order = 1;
        foreach ($sorted_categories as $gCategory) {
          $gCatId = $gCategory->getId();
          if ($order != $gCategoriesCache[$gCatId]->getOrder()) {
            $updateSql = 'UPDATE cgroup_category'
              . ' SET corder=:order'
              . ' WHERE cgroup_id=:group_id AND category_id=:cat_id';
            $conn->executeUpdate($updateSql,
                                 array('order'    => $order,
                                       'group_id' => $group_id,
                                       'cat_id'   => $gCatId),
                                 array('order'    => PDO::PARAM_INT,
                                       'group_id' => PDO::PARAM_INT,
                                       'cat_id'   => PDO::PARAM_INT)
                                 );

          }
          $gCategory->setOrder($order++);
        }
        $gCategories = new ArrayCollection($sorted_categories);
      }

      if ($doPurge) {
        $purgeSql = 'DELETE FROM ut USING user_tag AS ut'
          . ' INNER JOIN tag AS t ON t.id=ut.tag_id'
          . ' LEFT JOIN cgroup_category AS gc ON gc.category_id=t.category_id'
          . ' WHERE gc.category_id IS NULL';
        $conn->executeUpdate($purgeSql);

        $purgeSql = 'DELETE FROM t USING tag AS t'
          . ' LEFT JOIN cgroup_category AS gc ON gc.category_id=t.category_id'
          . ' LEFT JOIN request_tag AS rt ON rt.tag_id = t.id'
          . ' WHERE rt.tag_id IS NULL AND gc.category_id IS NULL';
        $conn->executeUpdate($purgeSql);

        $purgeSql = 'DELETE FROM c USING category AS c'
          . ' LEFT JOIN tag AS t ON t.category_id=c.id'
          . ' LEFT JOIN cgroup_category AS gc ON gc.category_id=c.id'
          . ' WHERE t.category_id IS NULL AND gc.category_id IS NULL'
          . ' AND c.id!=1';
        $conn->executeUpdate($purgeSql);
      }

      $conn->commit();
    } catch (\Exception $e) {
      $conn->rollback();
      throw $e;
    }
  }
}
