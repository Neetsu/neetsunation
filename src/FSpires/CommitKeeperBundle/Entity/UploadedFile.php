<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * An uploaded file
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class UploadedFile
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var datetime $updatedDate
   *
   * @ORM\Column(name="updated_date", type="datetime", nullable=false)
   */
  private $updatedDate;

  /**
   * URL
   * @var string $url
   *
   * @ORM\Column(name="url", type="string", length=300, nullable=false)
   */
  protected $url;

  /**
   * Content-Type
   * @var string $contentType
   *
   * @ORM\Column(name="content_type", type="string", length=200, nullable=false)
   */
  private $contentType;

  /**
   * Uploaded attachment file
   * @var File $file
   *
   * @Assert\File(maxSize="128000000")
   */
  protected $file;

  /**
   * A property used temporarily while deleting
   */
  private $filenameForRemove = false;


  public function __clone()
  {
      if (empty($this->file)) {
          return;
      }

      $oldName = $this->file->getPathname();
      $matches = array();
      if (preg_match('/X(\d+)$/', $oldName, $matches, PREG_OFFSET_CAPTURE)) {
          $length = $matches[0][1];
          $newName = substr($oldName, 0, $length);
          $newName .= 'X' . (intval($matches[1][0]) + 1);
      } else {
          $newName = $oldName . 'X1';
      }
      link($oldName, $newName);
      $this->file = new File($newName);
  }

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set updatedDate
   *
   * @param datetime $updatedDate
   */
  public function setUpdatedDate($updatedDate=null)
  {
    if (empty($updatedDate)) {
      $updatedDate = new \DateTime('now');
    }
    $this->updatedDate = $updatedDate;
  }

  /**
   * Get updatedDate
   *
   * @return datetime 
   */
  public function getUpdatedDate()
  {
    return $this->updatedDate;
  }

  /**
   * Set an uploaded file
   *
   * @param File $file
   */
  public function setFile(File $file)
  {
    $this->file = $file;
    $this->setUpdatedDate();
  }

  /**
   * Get a newly uploaded file
   */
  public function getFile()
  {
    return $this->file;
  }

  /**
   * Check if we have an uploaded file
   */
  protected function hasNewUploadedFile()
  {
    return !empty($this->file);
  }

  /**
   * @ORM\PrePersist()
   * @ORM\PreUpdate()
   * @return boolean True if file attributes has been set ok
   */
  public function setFileAttributes()
  {
    if (!$this->hasNewUploadedFile()) return false;

    $ext = $this->file->guessExtension();
    if ($ext) {
      $this->url = '.' . $ext;
    } else {
      $this->url = '';
    }

    $this->guessContentType();

    return true;
  }

  private function guessContentType()
  {
      if (!empty($this->contentType)) {
          return;
      }

      $contentType = substr($this->file->getClientMimeType(), 0, 200);
      if ($contentType) {
          $contentType = preg_replace(array('/\s+/', '~[^0-9a-z\-\/]~'),
              array('', '_'), $contentType);
      }
      if (!$contentType || 'application/octet-stream'==$contentType) {
          $contentType = $this->file->getMimeType();
          if (!$contentType) {
              $contentType = 'application/octet-stream';
          }
      }

      $this->contentType = $contentType;
  }

  /**
   * Get the file name of the file
   */
  protected function getFileName()
  {
    return $this->id . $this->url;
  }

  /**
   * @ORM\PostPersist()
   * @ORM\PostUpdate()
   */
  public function storeFile()
  {
    // If $this->filenameForRemove is set, remove the old file
    $this->removeFile();

    // Then check if we have a new file
    if (!$this->hasNewUploadedFile()) return;

    $this->file->move($this->getUploadDir(), $this->getFileName());

    // Make sure only newly uploaded files are stored
    unset($this->file);
  }

  /**
   * Get the file name of the file, with the absolute file path
   */
  public function getAbsoluteFilePath()
  {
    if (empty($this->id)) return false;
    return $this->getUploadDir() . DIRECTORY_SEPARATOR . $this->getFileName();
  }

  /**
   * @ORM\PreRemove()
   */
  public function storeFilenameForRemove()
  {
    // This is necessary because in PostRemove the id is gone,
    // and getAbsolutePath relies on getFileName which relies on $this->id.
    $this->filenameForRemove = $this->getAbsoluteFilePath();
  }

  /**
   * @ORM\PostRemove()
   */
  public function removeFile()
  {
    $file = $this->filenameForRemove;
    if ($file && is_file($file)) {
      unlink($file);
    }
  }

  /**
   * Get the absolute directory path to the directory where the uploaded
   * files and documents are stored.
   */
  abstract protected function getUploadDir();

  /**
   * Set contentType
   *
   * @param string $contentType
   */
  public function setContentType($contentType)
  {
    $this->contentType = $contentType;
  }

  /**
   * Get contentType
   *
   * @return string 
   */
  public function getContentType()
  {
    return $this->contentType;
  }
}
