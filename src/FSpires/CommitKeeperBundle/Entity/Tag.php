<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSpires\CommitKeeperBundle\Entity\Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity
 */
class Tag
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue
   */
  private $id;

  /**
   * @var \FSpires\CommitKeeperBundle\Entity\Category $category
   * This property may be redundant, since a tag is always
   * in a collection of tags belonging to a Category object.
   *
   * Categories and tags are handled manually by our own code
   * This ORM relationship is not used
   * ORM\ManyToOne(targetEntity="Category")
   * ORM\JoinColumn(name="category_id")
   */
  private $category;

  /**
   * @var string $name
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;


  private $source = false;


  public function __construct($id=null) {
    $this->id = $id;
  }

  /**
   * Set Id
   *
   * @param integer $id
   */
  public function setId($id)
  {
    if ($this->id) {
      throw new \LogicException('Cannot change id on a tag.');
    }
    $this->id = $id;
  }

  /**
   * Get Id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set category
   *
   * @param \FSpires\CommitKeeperBundle\Entity\Category $category
   */
  public function setCategory(Category $category)
  {
    $this->category = $category;
  }

  /**
   * Get category
   *
   * @return \FSpires\CommitKeeperBundle\Entity\Category
   */
  public function getCategory()
  {
    return $this->category;
  }

  /**
   * Set name
   *
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Get name
   *
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

  public function setSource($src)
  {
    $this->source = $src;
  }

  public function getSource()
  {
    return $this->source;
  }
}
