<?php

namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSpires\CommitKeeperBundle\Entity\UserTypes
 *
 * @ORM\Table(name="user_type")
 * @ORM\Entity
 */
class UserTypes
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;



    /**
     * Get Id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}