<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * The unit of the budget field.
 *
 * @ORM\Table(name="budget_field_unit")
 * @ORM\Entity
 */
class BudgetUnit
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * Unit name
   * @var string $unit
   *
   * @ORM\Column(name="unit", type="string", length=25, nullable=false)
   */
  private $unit;

  /**
   * Is this one of the preferred units on the top of the select list?
   * @var boolean $preferred
   *
   * @ORM\Column(name="preferred", type="boolean", nullable=false)
   */
  private $preferred;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get unit
   *
   * @return string 
   */
  public function getUnit()
  {
    return $this->unit;
  }

  /**
   * Is a preferred option?
   *
   * @return boolean 
   */
  public function isPreferred()
  {
    return $this->preferred;
  }
}
