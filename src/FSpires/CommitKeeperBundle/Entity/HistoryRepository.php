<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\EntityRepository;

class HistoryRepository extends EntityRepository
{
  /**
   * Find all history that belong to one commitment
   *
   * @param integer $req_id The id of the Request object
   * @return array
   */
  public function findHistory($req_id) {
    return $this->findBy(array('request'=>$req_id),array('eventDate'=>'DESC'));
  }
}
