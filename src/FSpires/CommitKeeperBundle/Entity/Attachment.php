<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * An attachment or a link
 *
 * @ORM\Table(name="attachment")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"validateAttachment"})
 */
class Attachment extends UploadedFile
{
  /**
   * Attachment or link belongs to this request
   * @var Request $request
   *
   * @ORM\ManyToOne(targetEntity="Request", inversedBy="attachments")
   * @ORM\JoinColumn(name="request_id", referencedColumnName="id")
   */
  private $request;

  /**
   * Name
   * @var string $name
   *
   * @ORM\Column(name="name", type="string", length=200, nullable=false)
   * @Assert\NotBlank
   */
  private $name;

  /**
   * Original File Name
   * @var string $origFileName
   *
   * @ORM\Column(name="orig_file_name", type="string", length=250, nullable=false)
   */
  private $origFileName;

  /**
   * True if this is a link
   * @var bool $isLink
   * @ORM\Column(name="islink", type="boolean", nullable=false)
   */
  private $isLink;

  /**
   * Flag, true if this attachment should be deleted
   * @var bool $delete
   */
  private $delete = false;

  /**
   * Flag, true if this attachment has been edited
   * @var bool $edit
   */
  private $edit = false;


  /**
   * Check that we have a valid attachment before a form
   * can be submitted.
   */
  public function validateAttachment($ec)
  {
    if ($this->isLink) {
      $this->url = trim($this->url);
      if (strlen($this->url) < 4) {
        $ec->addViolationAt('linkUrl','attachment.invalidUrl');
        return;
      }
      $matches = array();
      if (preg_match('@^([\w\-\+]+:/{0,2})(.+)$@', $this->url, $matches)) {
        $prefix = $matches[1];
        $rest = trim($matches[2]);
      } else {
        $ec->addViolationAt('linkUrl','attachment.invalidUrl');
        return;
      }
      if (strlen($rest) < 2) {
        $ec->addViolationAt('linkUrl','attachment.invalidUrl');
        return;
      }
      if (!(strncasecmp($prefix,'http',4) && strncasecmp($prefix,'ftp',3))) {
        if (!preg_match('@^[^/.]+\.[^/.]+@', $rest)) {
          $ec->addViolationAt('linkUrl','attachment.invalidUrl');
        }
      }
      return;
    }

    if (!parent::hasNewUploadedFile()) {
      $ec->addViolationAt('file','attachment.noFile');
    }
  }

  /**
   * Check if we have an uploaded file
   */
  protected function hasNewUploadedFile()
  {
    if ($this->isLink) return false;
    return parent::hasNewUploadedFile();
  }

  /**
   * Set request
   *
   * @param \FSpires\CommitKeeperBundle\Entity\Request $request
   */
  public function setRequest(Request $request = null)
  {
    $this->request = $request;
  }

  /**
   * Get request
   *
   * @return \FSpires\CommitKeeperBundle\Entity\Request 
   */
  public function getRequest()
  {
    return $this->request;
  }

  /**
   * Set name
   *
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Get name
   *
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @ORM\PrePersist()
   * @ORM\PreUpdate()
   */
  public function setFileAttributes()
  {
    if (!parent::setFileAttributes()) return;

    $file = $this->getFile();
    if (!$this->origFileName) {
      $this->origFileName = substr($file->getClientOriginalName(), 0, 250);
    }
    if (!$this->origFileName) {
      $this->origFileName = 'attachment' . $this->url;
    }
  }

  /**
   * Get the file name of the file, with the absolute file path
   */
  public function getAbsoluteFilePath()
  {
    if ($this->isLink) return false;
    return parent::getAbsoluteFilePath();
  }

  /**
   * Get the absolute directory path to the directory where the uploaded
   * files and documents are stored.
   */
  protected function getUploadDir()
  {
    return __DIR__.'/../../../../app/attachments';
  }

  /**
   * Get the URL used for the file when it is presented to the browser
   */
  private function getUrlForFile()
  {
    return '/attachment/'. $this->getId();
  }

  /**
   * Set origFileName
   *
   * @param string $origFileName
   */
  public function setOrigFileName($origFileName)
  {
    $this->origFileName = $origFileName;
  }

  /**
   * Get origFileName
   *
   * @return string 
   */
  public function getOrigFileName()
  {
    return $this->origFileName;
  }

  /**
   * Set the url for a link
   * @param string $url
   */
  public function setLinkUrl($url) {
    $url = substr(trim($url), 0, 300);
    $matches = array();
    if (preg_match('@^([\w\-\+]+:/{0,2})(.+)$@', $url, $matches)) {
      $prefix = $matches[1];
      $rest = $matches[2];
    } else {
      $prefix = 'http://';
      $rest = substr($url, 0, 293);
    }
    // No javascript urls are allowed
    if (strncasecmp($prefix,'javasc',6)==0) {
      $rest = substr($prefix . $rest, 0, 293); 
      $prefix = 'http://';
    }

    // Replace illegal chars in URL and trim whitespace
    $rest = trim(strtr($rest, "\"'", '__'));

    $this->url = $prefix . $rest;

    $this->setUpdatedDate();
  }

  /**
   * Get URL for link inputbox
   *
   * @return string 
   */
  public function getLinkUrl()
  {
    if ($this->isLink) {
      return $this->url;
    }

    return '';
  }


  /**
   * Get the computed Url for this attachment or link
   *
   * @return string 
   */
  public function getUrl()
  {
    if ($this->isLink) {
      return $this->url;
    }

    return $this->getUrlForFile();
  }

  /**
   * Set isLink
   *
   * @param boolean $isLink
   */
  public function setIsLink($isLink)
  {
    $this->isLink = (boolean)$isLink;
  }

  /**
   * Is this a Link?
   *
   * @return boolean
   */
  public function getIsLink()
  {
    return $this->isLink;
  }

  /**
   * Is this a Link?
   *
   * @return boolean
   */
  public function isLink()
  {
    return $this->isLink;
  }

  /**
   * Set delete
   *
   * @param boolean $delete
   */
  public function setDelete($delete)
  {
    $this->delete = (boolean)$delete;
  }

  /**
   * Get delete
   *
   * @return boolean
   */
  public function getDelete()
  {
    return $this->delete;
  }

  /**
   * Set edit
   *
   * @param boolean $edit
   */
  public function setEdit($edit)
  {
    $this->edit = (boolean)$edit;
  }

  /**
   * Get edit
   *
   * @return boolean
   */
  public function getEdit()
  {
    return $this->edit;
  }
}
