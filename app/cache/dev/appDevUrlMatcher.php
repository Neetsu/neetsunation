<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/upgrade')) {
                // _assetic_91d0829
                if ($pathinfo === '/css/upgrade.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '91d0829',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_91d0829',);
                }

                if (0 === strpos($pathinfo, '/css/upgrade_')) {
                    // _assetic_91d0829_0
                    if ($pathinfo === '/css/upgrade_main_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '91d0829',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_91d0829_0',);
                    }

                    // _assetic_91d0829_1
                    if ($pathinfo === '/css/upgrade_button_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '91d0829',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_91d0829_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/cdetail')) {
                // _assetic_72636ec
                if ($pathinfo === '/css/cdetail.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_72636ec',);
                }

                if (0 === strpos($pathinfo, '/css/cdetail_')) {
                    // _assetic_72636ec_0
                    if ($pathinfo === '/css/cdetail_jquery-ui-1.10.2.custom_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_72636ec_0',);
                    }

                    // _assetic_72636ec_1
                    if ($pathinfo === '/css/cdetail_main_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_72636ec_1',);
                    }

                    // _assetic_72636ec_2
                    if ($pathinfo === '/css/cdetail_button_3.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_72636ec_2',);
                    }

                    // _assetic_72636ec_3
                    if ($pathinfo === '/css/cdetail_autocomplete_4.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_72636ec_3',);
                    }

                    // _assetic_72636ec_4
                    if ($pathinfo === '/css/cdetail_jsDatePick_ltr.min_5.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_72636ec_4',);
                    }

                    // _assetic_72636ec_5
                    if ($pathinfo === '/css/cdetail_ddslick_6.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_72636ec_5',);
                    }

                    // _assetic_72636ec_6
                    if ($pathinfo === '/css/cdetail_form_7.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 6,  '_format' => 'css',  '_route' => '_assetic_72636ec_6',);
                    }

                    // _assetic_72636ec_7
                    if ($pathinfo === '/css/cdetail_table_8.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '72636ec',  'pos' => 7,  '_format' => 'css',  '_route' => '_assetic_72636ec_7',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/js')) {
            if (0 === strpos($pathinfo, '/js/cdetail')) {
                // _assetic_804f47d
                if ($pathinfo === '/js/cdetail.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_804f47d',);
                }

                if (0 === strpos($pathinfo, '/js/cdetail_part_')) {
                    if (0 === strpos($pathinfo, '/js/cdetail_part_1_')) {
                        // _assetic_804f47d_0
                        if ($pathinfo === '/js/cdetail_part_1_01_jquery-ui-1.10.2.custom_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_804f47d_0',);
                        }

                        if (0 === strpos($pathinfo, '/js/cdetail_part_1_a')) {
                            if (0 === strpos($pathinfo, '/js/cdetail_part_1_add_')) {
                                // _assetic_804f47d_1
                                if ($pathinfo === '/js/cdetail_part_1_add_attachment_2.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_804f47d_1',);
                                }

                                // _assetic_804f47d_2
                                if ($pathinfo === '/js/cdetail_part_1_add_user_tag_3.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_804f47d_2',);
                                }

                            }

                            // _assetic_804f47d_3
                            if ($pathinfo === '/js/cdetail_part_1_alter_list_4.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_804f47d_3',);
                            }

                        }

                        // _assetic_804f47d_4
                        if ($pathinfo === '/js/cdetail_part_1_change_action_5.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_804f47d_4',);
                        }

                        if (0 === strpos($pathinfo, '/js/cdetail_part_1_jquery.')) {
                            // _assetic_804f47d_5
                            if ($pathinfo === '/js/cdetail_part_1_jquery.autocomplete_6.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_804f47d_5',);
                            }

                            // _assetic_804f47d_6
                            if ($pathinfo === '/js/cdetail_part_1_jquery.ddslick.min_7.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_804f47d_6',);
                            }

                        }

                        // _assetic_804f47d_7
                        if ($pathinfo === '/js/cdetail_part_1_newUser_8.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_804f47d_7',);
                        }

                        // _assetic_804f47d_8
                        if ($pathinfo === '/js/cdetail_part_1_stop_repeat_submission_9.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_804f47d_8',);
                        }

                        if (0 === strpos($pathinfo, '/js/cdetail_part_1_t')) {
                            if (0 === strpos($pathinfo, '/js/cdetail_part_1_toggle_')) {
                                // _assetic_804f47d_9
                                if ($pathinfo === '/js/cdetail_part_1_toggle_history_10.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_804f47d_9',);
                                }

                                // _assetic_804f47d_10
                                if ($pathinfo === '/js/cdetail_part_1_toggle_tag_11.js') {
                                    return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_804f47d_10',);
                                }

                            }

                            // _assetic_804f47d_11
                            if ($pathinfo === '/js/cdetail_part_1_twistie_12.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_804f47d_11',);
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/js/cdetail_part_2_')) {
                        // _assetic_804f47d_12
                        if ($pathinfo === '/js/cdetail_part_2_Event_1.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_804f47d_12',);
                        }

                        // _assetic_804f47d_13
                        if ($pathinfo === '/js/cdetail_part_2_SortedTable_2.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_804f47d_13',);
                        }

                        // _assetic_804f47d_14
                        if ($pathinfo === '/js/cdetail_part_2_calcTableHeight_3.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '804f47d',  'pos' => 14,  '_format' => 'js',  '_route' => '_assetic_804f47d_14',);
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/form')) {
                // _assetic_a44d8fe
                if ($pathinfo === '/js/form.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_a44d8fe',);
                }

                if (0 === strpos($pathinfo, '/js/form_part_1_')) {
                    // _assetic_a44d8fe_0
                    if ($pathinfo === '/js/form_part_1_01_jquery-ui-1.10.2.custom_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_0',);
                    }

                    if (0 === strpos($pathinfo, '/js/form_part_1_a')) {
                        if (0 === strpos($pathinfo, '/js/form_part_1_add_')) {
                            // _assetic_a44d8fe_1
                            if ($pathinfo === '/js/form_part_1_add_attachment_2.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_1',);
                            }

                            // _assetic_a44d8fe_2
                            if ($pathinfo === '/js/form_part_1_add_user_tag_3.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_2',);
                            }

                        }

                        // _assetic_a44d8fe_3
                        if ($pathinfo === '/js/form_part_1_alter_list_4.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_3',);
                        }

                    }

                    // _assetic_a44d8fe_4
                    if ($pathinfo === '/js/form_part_1_change_action_5.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_4',);
                    }

                    if (0 === strpos($pathinfo, '/js/form_part_1_jquery.')) {
                        // _assetic_a44d8fe_5
                        if ($pathinfo === '/js/form_part_1_jquery.autocomplete_6.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_5',);
                        }

                        // _assetic_a44d8fe_6
                        if ($pathinfo === '/js/form_part_1_jquery.ddslick.min_7.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_6',);
                        }

                    }

                    // _assetic_a44d8fe_7
                    if ($pathinfo === '/js/form_part_1_newUser_8.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_7',);
                    }

                    // _assetic_a44d8fe_8
                    if ($pathinfo === '/js/form_part_1_stop_repeat_submission_9.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_8',);
                    }

                    if (0 === strpos($pathinfo, '/js/form_part_1_t')) {
                        if (0 === strpos($pathinfo, '/js/form_part_1_toggle_')) {
                            // _assetic_a44d8fe_9
                            if ($pathinfo === '/js/form_part_1_toggle_history_10.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_9',);
                            }

                            // _assetic_a44d8fe_10
                            if ($pathinfo === '/js/form_part_1_toggle_tag_11.js') {
                                return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_10',);
                            }

                        }

                        // _assetic_a44d8fe_11
                        if ($pathinfo === '/js/form_part_1_twistie_12.js') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'a44d8fe',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_a44d8fe_11',);
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/css/form')) {
            // _assetic_cfa0dc6
            if ($pathinfo === '/css/form.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6',);
            }

            if (0 === strpos($pathinfo, '/css/form_')) {
                // _assetic_cfa0dc6_0
                if ($pathinfo === '/css/form_jquery-ui-1.10.2.custom_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_0',);
                }

                // _assetic_cfa0dc6_1
                if ($pathinfo === '/css/form_main_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_1',);
                }

                // _assetic_cfa0dc6_2
                if ($pathinfo === '/css/form_button_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_2',);
                }

                // _assetic_cfa0dc6_3
                if ($pathinfo === '/css/form_autocomplete_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_3',);
                }

                // _assetic_cfa0dc6_4
                if ($pathinfo === '/css/form_jsDatePick_ltr.min_5.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_4',);
                }

                // _assetic_cfa0dc6_5
                if ($pathinfo === '/css/form_ddslick_6.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_5',);
                }

                // _assetic_cfa0dc6_6
                if ($pathinfo === '/css/form_form_7.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cfa0dc6',  'pos' => 6,  '_format' => 'css',  '_route' => '_assetic_cfa0dc6_6',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/ckt')) {
            // _assetic_d5a37cc
            if ($pathinfo === '/js/ckt.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd5a37cc',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_d5a37cc',);
            }

            if (0 === strpos($pathinfo, '/js/ckt_part_1_')) {
                // _assetic_d5a37cc_0
                if ($pathinfo === '/js/ckt_part_1_Event_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'd5a37cc',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_d5a37cc_0',);
                }

                // _assetic_d5a37cc_1
                if ($pathinfo === '/js/ckt_part_1_SortedTable_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'd5a37cc',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_d5a37cc_1',);
                }

                // _assetic_d5a37cc_2
                if ($pathinfo === '/js/ckt_part_1_calcTableHeight_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'd5a37cc',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_d5a37cc_2',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/table')) {
                // _assetic_9698be1
                if ($pathinfo === '/css/table.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9698be1',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_9698be1',);
                }

                // _assetic_9698be1_0
                if ($pathinfo === '/css/table_table_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9698be1',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_9698be1_0',);
                }

            }

            if (0 === strpos($pathinfo, '/css/settings')) {
                // _assetic_8d9dd4d
                if ($pathinfo === '/css/settings.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '8d9dd4d',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_8d9dd4d',);
                }

                if (0 === strpos($pathinfo, '/css/settings_')) {
                    // _assetic_8d9dd4d_0
                    if ($pathinfo === '/css/settings_main_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8d9dd4d',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_8d9dd4d_0',);
                    }

                    // _assetic_8d9dd4d_1
                    if ($pathinfo === '/css/settings_settings_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8d9dd4d',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_8d9dd4d_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/confirm')) {
                // _assetic_4b4fcd0
                if ($pathinfo === '/css/confirm.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4b4fcd0',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_4b4fcd0',);
                }

                // _assetic_4b4fcd0_0
                if ($pathinfo === '/css/confirm_confirm_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '4b4fcd0',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_4b4fcd0_0',);
                }

            }

            if (0 === strpos($pathinfo, '/css/login_debug')) {
                // _assetic_9fd8b10
                if ($pathinfo === '/css/login_debug.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '9fd8b10',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_9fd8b10',);
                }

                if (0 === strpos($pathinfo, '/css/login_debug_')) {
                    if (0 === strpos($pathinfo, '/css/login_debug_part_1_')) {
                        // _assetic_9fd8b10_0
                        if ($pathinfo === '/css/login_debug_part_1_login_1.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '9fd8b10',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_9fd8b10_0',);
                        }

                        // _assetic_9fd8b10_1
                        if ($pathinfo === '/css/login_debug_part_1_users_debug_2.css') {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '9fd8b10',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_9fd8b10_1',);
                        }

                    }

                    // _assetic_9fd8b10_2
                    if ($pathinfo === '/css/login_debug_button_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '9fd8b10',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_9fd8b10_2',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/js/login_debug')) {
            // _assetic_acd585c
            if ($pathinfo === '/js/login_debug.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_acd585c',);
            }

            if (0 === strpos($pathinfo, '/js/login_debug_')) {
                // _assetic_acd585c_0
                if ($pathinfo === '/js/login_debug_translator.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_acd585c_0',);
                }

                // _assetic_acd585c_1
                if ($pathinfo === '/js/login_debug_config_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_acd585c_1',);
                }

                if (0 === strpos($pathinfo, '/js/login_debug_part_')) {
                    // _assetic_acd585c_2
                    if ($pathinfo === '/js/login_debug_part_3_en_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_acd585c_2',);
                    }

                    // _assetic_acd585c_3
                    if ($pathinfo === '/js/login_debug_part_4_jainrain_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_acd585c_3',);
                    }

                    // _assetic_acd585c_4
                    if ($pathinfo === '/js/login_debug_part_5_login_debug_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'acd585c',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_acd585c_4',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/css/login')) {
            // _assetic_7eabdba
            if ($pathinfo === '/css/login.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '7eabdba',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_7eabdba',);
            }

            if (0 === strpos($pathinfo, '/css/login_part_1_')) {
                // _assetic_7eabdba_0
                if ($pathinfo === '/css/login_part_1_login_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7eabdba',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_7eabdba_0',);
                }

                // _assetic_7eabdba_1
                if ($pathinfo === '/css/login_part_1_users_debug_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7eabdba',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_7eabdba_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/login')) {
            // _assetic_7152814
            if ($pathinfo === '/js/login.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 7152814,  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_7152814',);
            }

            if (0 === strpos($pathinfo, '/js/login_')) {
                // _assetic_7152814_0
                if ($pathinfo === '/js/login_translator.min_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 7152814,  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_7152814_0',);
                }

                // _assetic_7152814_1
                if ($pathinfo === '/js/login_config_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 7152814,  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_7152814_1',);
                }

                if (0 === strpos($pathinfo, '/js/login_part_')) {
                    // _assetic_7152814_2
                    if ($pathinfo === '/js/login_part_3_en_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 7152814,  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_7152814_2',);
                    }

                    // _assetic_7152814_3
                    if ($pathinfo === '/js/login_part_4_jainrain_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 7152814,  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_7152814_3',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/css/main')) {
            // _assetic_7c2586b
            if ($pathinfo === '/css/main.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '7c2586b',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_7c2586b',);
            }

            if (0 === strpos($pathinfo, '/css/main_')) {
                // _assetic_7c2586b_0
                if ($pathinfo === '/css/main_main_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7c2586b',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_7c2586b_0',);
                }

                // _assetic_7c2586b_1
                if ($pathinfo === '/css/main_button_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7c2586b',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_7c2586b_1',);
                }

                // _assetic_7c2586b_2
                if ($pathinfo === '/css/main_table_3.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7c2586b',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_7c2586b_2',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/main')) {
            // _assetic_ed74604
            if ($pathinfo === '/js/main.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_ed74604',);
            }

            if (0 === strpos($pathinfo, '/js/main_')) {
                if (0 === strpos($pathinfo, '/js/main_part_1_')) {
                    // _assetic_ed74604_0
                    if ($pathinfo === '/js/main_part_1_00_jquery_1.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_ed74604_0',);
                    }

                    // _assetic_ed74604_1
                    if ($pathinfo === '/js/main_part_1_toggle_menu_2.js') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_ed74604_1',);
                    }

                }

                // _assetic_ed74604_2
                if ($pathinfo === '/js/main_translator.min_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_ed74604_2',);
                }

                // _assetic_ed74604_3
                if ($pathinfo === '/js/main_config_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_ed74604_3',);
                }

                // _assetic_ed74604_4
                if ($pathinfo === '/js/main_part_4_en_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'ed74604',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_ed74604_4',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css/register_openid')) {
            // _assetic_f7d39b8
            if ($pathinfo === '/css/register_openid.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'f7d39b8',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_f7d39b8',);
            }

            if (0 === strpos($pathinfo, '/css/register_openid_')) {
                // _assetic_f7d39b8_0
                if ($pathinfo === '/css/register_openid_autocomplete_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'f7d39b8',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_f7d39b8_0',);
                }

                // _assetic_f7d39b8_1
                if ($pathinfo === '/css/register_openid_register_newopenid_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'f7d39b8',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_f7d39b8_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/register')) {
            // _assetic_2c21011
            if ($pathinfo === '/js/register.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '2c21011',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_2c21011',);
            }

            if (0 === strpos($pathinfo, '/js/register_')) {
                // _assetic_2c21011_0
                if ($pathinfo === '/js/register_00_jquery_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2c21011',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_2c21011_0',);
                }

                // _assetic_2c21011_1
                if ($pathinfo === '/js/register_jquery.autocomplete_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '2c21011',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_2c21011_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css/register')) {
            // _assetic_3f3a540
            if ($pathinfo === '/css/register.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '3f3a540',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_3f3a540',);
            }

            // _assetic_3f3a540_0
            if ($pathinfo === '/css/register_register_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '3f3a540',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_3f3a540_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/login')) {
            // _assetic_29abfcb
            if ($pathinfo === '/js/login.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '29abfcb',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_29abfcb',);
            }

            // _assetic_29abfcb_0
            if ($pathinfo === '/js/login_part_1_jainrain_1.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '29abfcb',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_29abfcb_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/newopenid')) {
            // _assetic_3cf8d3e
            if ($pathinfo === '/css/newopenid.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '3cf8d3e',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_3cf8d3e',);
            }

            // _assetic_3cf8d3e_0
            if ($pathinfo === '/css/newopenid_newopenid_1.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '3cf8d3e',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_3cf8d3e_0',);
            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

        }

        // bazinga_jstranslation_js
        if (0 === strpos($pathinfo, '/translations') && preg_match('#^/translations(?:/(?P<domain>[\\w]+)(?:\\.(?P<_format>js|json))?)?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_bazinga_jstranslation_js;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bazinga_jstranslation_js')), array (  '_controller' => 'bazinga.jstranslation.controller:getTranslationsAction',  'domain' => 'messages',  '_format' => 'js',));
        }
        not_bazinga_jstranslation_js:

        // login_check
        if (preg_match('#^/(?P<_locale>en|es)/login_check$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_check')), array ());
        }

        // logout
        if (preg_match('#^/(?P<_locale>en|es)/logout$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'logout')), array ());
        }

        // login
        if (preg_match('#^/(?P<_locale>en|es)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login')), array (  '_controller' => 'ck.security:loginAction',  'debug' => '1',));
        }

        // clean_login
        if (preg_match('#^/(?P<_locale>en|es)/login/clean$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'clean_login')), array (  '_controller' => 'ck.security:cleanLoginAction',));
        }

        // access_denied
        if (preg_match('#^/(?P<_locale>en|es)/access_denied$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'access_denied')), array (  '_controller' => 'ck.security:accessDeniedAction',));
        }

        // CK_root
        if (preg_match('#^/(?P<_locale>en|es)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'CK_root');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_root')), array (  '_controller' => 'ck.registration:registerAction',  'root' => true,  'userStr' => false,  'table' => 'table',  'reqSelect' => false,));
        }

        // CK_register
        if (preg_match('#^/(?P<_locale>en|es)/register$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_register')), array (  '_controller' => 'ck.registration:registerAction',  'root' => false,));
        }

        // CK_register_addauth
        if (preg_match('#^/(?P<_locale>en|es)/register/addauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_register_addauth')), array (  '_controller' => 'ck.registration:registerOrAddAuthAction',));
        }

        // CK_confirm
        if (preg_match('#^/(?P<_locale>en|es)/confirm$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_confirm')), array (  '_controller' => 'ck.registration:confirmAction',));
        }

        // CK_newopenid
        if (preg_match('#^/(?P<_locale>en|es)/setauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_newopenid')), array (  '_controller' => 'ck.registration:newOpenIdAction',));
        }

        // CK_addopenid
        if (preg_match('#^/(?P<_locale>en|es)/addauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_addopenid')), array (  '_controller' => 'ck.registration:addOpenIdAction',));
        }

        // CK_newContact
        if (preg_match('#^/(?P<_locale>en|es)/newcontact$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_newContact;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_newContact')), array (  '_controller' => 'ck.ajax:newContactAction',));
        }
        not_CK_newContact:

        // CK_upgrade
        if (preg_match('#^/(?P<_locale>en|es)/upgrade$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_upgrade')), array (  '_controller' => 'ck.upgrade:indexAction',  'action' => 'none',));
        }

        // CK_upgrade_action
        if (preg_match('#^/(?P<_locale>en|es)/upgrade/(?P<action>action\\w+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_upgrade_action')), array (  '_controller' => 'ck.upgrade:indexAction',));
        }

        // CK_default
        if (preg_match('#^/(?P<_locale>en|es)/table$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_default')), array (  '_controller' => 'ck.table:indexAction',  'userStr' => false,  'table' => 'table',  'reqSelect' => false,));
        }

        // CK_table
        if (preg_match('#^/(?P<_locale>en|es)/(?P<table>table(Sort\\w+)?)(?:/(?P<reqSelect>PendingByMe|PendingByOthers|AllActive|Closed))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_table')), array (  '_controller' => 'ck.table:indexAction',  'userStr' => false,  'reqSelect' => false,));
        }

        // CK_user_table
        if (preg_match('#^/(?P<_locale>en|es)/(?P<userStr>user\\d+)/(?P<table>table(Sort\\w+)?)(?:/(?P<reqSelect>PendingByMe|PendingByOthers|AllActive|Closed))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_user_table')), array (  '_controller' => 'ck.table:indexAction',  'reqSelect' => false,));
        }

        // CK_new_commitment
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_new_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_commitment')), array (  '_controller' => 'ck.commitment.new:newAction',  'superReqId' => false,));
        }
        not_CK_new_commitment:

        // CK_new_commitment_posted
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_new_commitment_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_commitment_posted')), array (  '_controller' => 'ck.commitment.new_posted:newAction',  'superReqId' => false,));
        }
        not_CK_new_commitment_posted:

        // CK_new_sub_commitment
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)/(?P<superReqId>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_new_sub_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_sub_commitment')), array (  '_controller' => 'ck.commitment.new:newAction',));
        }
        not_CK_new_sub_commitment:

        // CK_new_sub_commitment_posted
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)/(?P<superReqId>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_new_sub_commitment_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_sub_commitment_posted')), array (  '_controller' => 'ck.commitment.new_posted:newAction',));
        }
        not_CK_new_sub_commitment_posted:

        // CK_commitment
        if (preg_match('#^/(?P<_locale>en|es)/commitment$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment')), array (  '_controller' => 'ck.commitment.show:indexAction',  'commitmentIdStr' => false,));
        }
        not_CK_commitment:

        // CK_commitment_user
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_user;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user')), array (  '_controller' => 'ck.commitment.show:indexAction',  'commitmentIdStr' => false,));
        }
        not_CK_commitment_user:

        // CK_commitment_user_id
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_user_id;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user_id')), array (  '_controller' => 'ck.commitment.show:indexAction',));
        }
        not_CK_commitment_user_id:

        // CK_commitment_user_id_posted
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_commitment_user_id_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user_id_posted')), array (  '_controller' => 'ck.commitment.show_posted:indexAction',));
        }
        not_CK_commitment_user_id_posted:

        // CK_commitment_id
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_id;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_id')), array (  '_controller' => 'ck.commitment.show:indexAction',  'userStr' => false,));
        }
        not_CK_commitment_id:

        // CK_commitment_id_posted
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_commitment_id_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_id_posted')), array (  '_controller' => 'ck.commitment.show_posted:indexAction',  'userStr' => false,));
        }
        not_CK_commitment_id_posted:

        // CK_attachment
        if (preg_match('#^/(?P<_locale>en|es)/attachment/(?P<attId>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_attachment')), array (  '_controller' => 'ck.attachment:indexAction',));
        }

        // CK_picture
        if (preg_match('#^/(?P<_locale>en|es)/picture/(?P<pictureId>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_picture')), array (  '_controller' => 'ck.picture:indexAction',));
        }

        // CK_edit_settings
        if (preg_match('#^/(?P<_locale>en|es)/editSettings$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_settings')), array (  '_controller' => 'ck.settings:editSettingsAction',));
        }

        // CK_edit_id
        if (preg_match('#^/(?P<_locale>en|es)/editId$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_id')), array (  '_controller' => 'ck.settings:editIdAction',));
        }

        // CK_edit_picture
        if (preg_match('#^/(?P<_locale>en|es)/editPicture$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_picture')), array (  '_controller' => 'ck.settings:editPictureAction',));
        }

        // CK_edit_group
        if (preg_match('#^/(?P<_locale>en|es)/editGroup$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_group')), array (  '_controller' => 'ck.settings:editGroupAction',));
        }

        // CK_edit_group_categories
        if (preg_match('#^/(?P<_locale>en|es)/editGroupCategories$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_group_categories')), array (  '_controller' => 'ck.settings:editGroupCategoriesAction',));
        }

        // CK_edit_usertags
        if (preg_match('#^/(?P<_locale>en|es)/editUserTags$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_usertags')), array (  '_controller' => 'ck.settings:editPrefsAction',));
        }

        // CK_edit_usertags_category
        if (preg_match('#^/(?P<_locale>en|es)/editUserTags/(?P<category_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_usertags_category')), array (  '_controller' => 'ck.settings:editUserTagCategoryAction',));
        }

        // access_denied_root
        if ($pathinfo === '/access_denied') {
            return array (  '_controller' => 'ck.security:accessDeniedAction',  '_route' => 'access_denied_root',);
        }

        // choose_language
        if (preg_match('#^/(?P<url>.*)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'choose_language')), array (  '_controller' => 'ck.settings:chooseLanguageAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
