<?php

/* CKBundle:Main:main_base.html.twig */
class __TwigTemplate_5adc1d131e2594986996317f2502be7426a65f812eefa67ee281c31ec71e8451 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_javascripts' => array($this, 'block_main_javascripts'),
            'body' => array($this, 'block_body'),
            'head' => array($this, 'block_head'),
            'heading' => array($this, 'block_heading'),
            'headertabs' => array($this, 'block_headertabs'),
            'rightcol' => array($this, 'block_rightcol'),
            'legend' => array($this, 'block_legend'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7c2586b_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7c2586b_0") : $this->env->getExtension('assets')->getAssetUrl("css/main_main_1.css");
            // line 9
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "7c2586b_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7c2586b_1") : $this->env->getExtension('assets')->getAssetUrl("css/main_button_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "7c2586b_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7c2586b_2") : $this->env->getExtension('assets')->getAssetUrl("css/main_table_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "7c2586b"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7c2586b") : $this->env->getExtension('assets')->getAssetUrl("css/main.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
    }

    // line 13
    public function block_main_javascripts($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "ed74604_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604_0") : $this->env->getExtension('assets')->getAssetUrl("js/main_part_1_00_jquery_1.js");
            // line 20
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "ed74604_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604_1") : $this->env->getExtension('assets')->getAssetUrl("js/main_part_1_toggle_menu_2.js");
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "ed74604_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604_2") : $this->env->getExtension('assets')->getAssetUrl("js/main_translator.min_2.js");
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "ed74604_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604_3") : $this->env->getExtension('assets')->getAssetUrl("js/main_config_3.js");
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "ed74604_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604_4") : $this->env->getExtension('assets')->getAssetUrl("js/main_part_4_en_1.js");
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "ed74604"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_ed74604") : $this->env->getExtension('assets')->getAssetUrl("js/main.js");
            echo "    <script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        // line 25
        echo "<div id=\"overlay\">
</div>
<div id=\"ck-header\">
        ";
        // line 28
        $this->displayBlock('head', $context, $blocks);
        // line 59
        echo " 
        <div id=\"ck-bannertitle\">
                <div class=\"ck-bannertitle-shadowtop\">
                </div>
                <h2>
                        ";
        // line 64
        $this->displayBlock('heading', $context, $blocks);
        echo " 
                </h2>
                ";
        // line 66
        $this->displayBlock('headertabs', $context, $blocks);
        echo " 
                <div class=\"ck-bannertitle-shadowbottom\">
                </div>
        </div>
</div>
<div class=\"rightcol\">
        ";
        // line 72
        $this->displayBlock('rightcol', $context, $blocks);
        echo " 
</div>
<div id=\"footerbg\"";
        // line 74
        if (array_key_exists("pageClass", $context)) {
            echo " class=\"";
            echo twig_escape_filter($this->env, (isset($context["pageClass"]) ? $context["pageClass"] : null), "html", null, true);
            echo "\"";
        }
        echo "><div id=\"footer\">
    ";
        // line 75
        $this->displayBlock('legend', $context, $blocks);
        // line 94
        echo "</div></div>
";
    }

    // line 28
    public function block_head($context, array $blocks = array())
    {
        echo " 
        <div id=\"ck-banner\">
                <a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("CK_default");
        echo "\"><img src=\"/images/commitkeeper-logo.png\" width=\"212\" height=\"40\" border=\"0\" alt=\"CommitKeeper\" class=\"ck-logo\" /></a>
                <div class=\"ck-topbuttons\">
                        <div class=\"ck-seperator\">
                        </div>
                        <ul>
                                <li class=\"ck-topbutton request\"> <a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("CK_new_commitment", array("type" => "request"));
        echo "\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("request", array(), "messages");
        echo "</a> </li>
                                <li class=\"ck-topbutton offer\"> <a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("CK_new_commitment", array("type" => "offer"));
        echo "\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("offer", array(), "messages");
        echo "</a> </li>
                                <li class=\"ck-topbutton todo\"> <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("CK_new_commitment", array("type" => "toDo"));
        echo "\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("toDo", array(), "messages");
        echo "</a> </li>
                        </ul>
                        <div class=\"ck-seperator\">
                        </div>
                </div>
                <div class=\"ck-username\">
                  <span class=\"name\">";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["UserName"]) ? $context["UserName"] : null), "html", null, true);
        echo "</span>
                  <span class=\"email\">";
        // line 44
        echo twig_escape_filter($this->env, (isset($context["UserEmail"]) ? $context["UserEmail"] : null), "html", null, true);
        echo "</span>
                </div>
                <div class=\"ck-version\">
                        <div class=\"ck-versionlabel\" title=\"";
        // line 47
        echo $this->env->getExtension('translator')->getTranslator()->trans("version.date", array(), "messages");
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["version"]) ? $context["version"] : null), "date", array()), "html", null, true);
        echo "\">
                                ";
        // line 48
        echo $this->env->getExtension('translator')->getTranslator()->trans("version.info%version%_%revno%", array("%version%" => $this->getAttribute((isset($context["version"]) ? $context["version"] : null), "ver", array()), "%revno%" => $this->getAttribute((isset($context["version"]) ? $context["version"] : null), "revno", array())), "messages");
        // line 49
        echo "                        </div>
                </div>
                <div class=\"ck-topactions\">
                        <ul>
                                <li class=\"ck-topaction help\"><a href=\"/help/";
        // line 53
        echo $this->env->getExtension('translator')->getTranslator()->trans("help.index", array(), "messages");
        echo "\" target=\"_blank\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("Help", array(), "messages");
        echo "</a></li>
                                <li class=\"ck-topaction prefs\"><a href=\"";
        // line 54
        echo $this->env->getExtension('routing')->getPath("CK_edit_settings");
        echo "\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("Settings", array(), "messages");
        echo "</a></li>
                                <li class=\"ck-topaction logout\"><a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("Logout", array(), "messages");
        echo "</a></li>
                        </ul>
                </div>
        </div>
        ";
    }

    // line 64
    public function block_heading($context, array $blocks = array())
    {
    }

    // line 66
    public function block_headertabs($context, array $blocks = array())
    {
        echo " ";
        $this->env->loadTemplate("CKBundle:Main:maintabs.html.twig")->display($context);
        echo " ";
    }

    // line 72
    public function block_rightcol($context, array $blocks = array())
    {
    }

    // line 75
    public function block_legend($context, array $blocks = array())
    {
        // line 76
        echo "        <div class=\"ck-bannertitle-shadowtop\"></div>
        <table border=\"0\" cellspacing=\"1\" cellpadding=\"5\" id=\"ck_legend\">
                <tbody>
                        <tr>
                                <td>";
        // line 80
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.negotiation", array(), "messages");
        echo "<br><img src=\"/images/negotiation.png\" /></td>
                                <td>";
        // line 81
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.progress", array(), "messages");
        echo "<br><img src=\"/images/on_track.png\" />&nbsp;&nbsp;
                                <img src=\"/images/at_risk.png\" />&nbsp;&nbsp;
                                <img src=\"/images/off_track.png\" /></td>
                                <td>";
        // line 84
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.delivered", array(), "messages");
        echo "<br><img src=\"/images/delivered.png\" /></td>
                                <td>";
        // line 85
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.accepted", array(), "messages");
        echo "<br><img src=\"/images/completed.png\" /></td>
                                <td>";
        // line 86
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.canceled", array(), "messages");
        echo "<br><img src=\"/images/cancelled.png\" /></td>
                                <td>";
        // line 87
        echo $this->env->getExtension('translator')->getTranslator()->trans("legend.declined", array(), "messages");
        echo "<br><img src=\"/images/declined.png\" /></td>
                        </tr>
                </tbody>
        </table>
        <div id=\"ck_copyright\">&copy; ";
        // line 91
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo ", <a href=\"http://4spires.com\" target=\"new_window\">4Spires Inc.</a></div>
        <div class=\"ck-bannertitle-shadowbottom\"></div>
    ";
    }

    public function getTemplateName()
    {
        return "CKBundle:Main:main_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 91,  311 => 87,  307 => 86,  303 => 85,  299 => 84,  293 => 81,  289 => 80,  283 => 76,  280 => 75,  275 => 72,  267 => 66,  262 => 64,  251 => 55,  245 => 54,  239 => 53,  233 => 49,  231 => 48,  225 => 47,  219 => 44,  215 => 43,  204 => 37,  198 => 36,  192 => 35,  184 => 30,  178 => 28,  173 => 94,  171 => 75,  163 => 74,  158 => 72,  149 => 66,  144 => 64,  137 => 59,  135 => 28,  130 => 25,  127 => 24,  87 => 20,  82 => 14,  79 => 13,  51 => 9,  46 => 4,  43 => 3,  11 => 1,);
    }
}
