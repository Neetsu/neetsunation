<?php

/* CKBundle:Commitment:datepick.onload.twig */
class __TwigTemplate_b7996a71bda1f1d60bc37186ad79dd9dd14e2cdff59c6d6592843fe98c2e4e44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\$('#";
        echo twig_escape_filter($this->env, (isset($context["id_date_target"]) ? $context["id_date_target"] : null), "html", null, true);
        echo "').datepicker({
  dateFormat:'";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["dpDateFormat"]) ? $context["dpDateFormat"] : null), "html", null, true);
        echo "',
  minDate: 0
});
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Commitment:datepick.onload.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 2,  19 => 1,);
    }
}
