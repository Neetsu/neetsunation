<?php

/* CKBundle:Commitment:main.html.twig */
class __TwigTemplate_cd1404e20f2b27aea46ae34d4d1414fbb937dbd9715ab817fb3e0382d10c90b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CKBundle:Main:main_base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'heading' => array($this, 'block_heading'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'onload' => array($this, 'block_onload'),
            'javascripts' => array($this, 'block_javascripts'),
            'rightcol' => array($this, 'block_rightcol'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CKBundle:Main:main_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 59
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "CKBundle:Form:user_list.html.twig"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_heading($context, array $blocks = array())
    {
        // line 4
        echo $this->env->getExtension('translator')->getTranslator()->trans("heading.details", array(), "messages");
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "72636ec_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_0") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_jquery-ui-1.10.2.custom_1.css");
            // line 19
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_1") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_main_2.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_2") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_button_3.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_3") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_autocomplete_4.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_4") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_jsDatePick_ltr.min_5.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_5") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_ddslick_6.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_6") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_form_7.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "72636ec_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec_7") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail_table_8.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        } else {
            // asset "72636ec"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_72636ec") : $this->env->getExtension('assets')->getAssetUrl("css/cdetail.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        }
        unset($context["asset_url"]);
    }

    // line 23
    public function block_onload($context, array $blocks = array())
    {
        // line 24
        if ((isset($context["HasStatusUpdate"]) ? $context["HasStatusUpdate"] : null)) {
            // line 25
            echo "\$('#commitment_statusUpdate').ddslick({
   embedCSS: false,
   background: '#fff',
   width:  '300px',
   onSelected: function (data) {
     \$('.ck-progress-status').text(data.selectedData.description);
   }
});
";
        }
        // line 34
        echo "
";
        // line 35
        if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
            // line 36
            $this->env->loadTemplate("CKBundle:Commitment:datepick.onload.twig")->display($context);
        }
        // line 38
        echo "
";
        // line 39
        if ((isset($context["hasSupportingRequests"]) ? $context["hasSupportingRequests"] : null)) {
            // line 40
            echo "var RT_table = new SortedTable('RT_table');
RT_table.onbodyclick = function(elm,e) {
  elm = SortedTable.findParent(elm,'tr');
  window.location.href = '";
            // line 43
            echo twig_escape_filter($this->env, (isset($context["suppUrlPrefix"]) ? $context["suppUrlPrefix"] : null), "html", null, true);
            echo "' + elm.id;
}
RT_table.onbodydblclick = RT_table.onbodyclick;
";
        }
    }

    // line 49
    public function block_javascripts($context, array $blocks = array())
    {
        // line 50
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "804f47d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_0") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_01_jquery-ui-1.10.2.custom_1.js");
            // line 55
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_1") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_add_attachment_2.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_2") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_add_user_tag_3.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_3") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_alter_list_4.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_4") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_change_action_5.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_5") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_jquery.autocomplete_6.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_6") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_jquery.ddslick.min_7.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_7") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_newUser_8.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_8") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_stop_repeat_submission_9.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_9") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_toggle_history_10.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_10"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_10") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_toggle_tag_11.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_11"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_11") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_1_twistie_12.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_12"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_12") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_2_Event_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_13"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_13") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_2_SortedTable_2.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "804f47d_14"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d_14") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail_part_2_calcTableHeight_3.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "804f47d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_804f47d") : $this->env->getExtension('assets')->getAssetUrl("js/cdetail.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
    }

    // line 61
    public function block_rightcol($context, array $blocks = array())
    {
        // line 62
        $this->env->loadTemplate("CKBundle:Commitment:show.html.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "CKBundle:Commitment:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 62,  265 => 61,  165 => 55,  161 => 50,  158 => 49,  149 => 43,  144 => 40,  142 => 39,  139 => 38,  136 => 36,  134 => 35,  131 => 34,  120 => 25,  118 => 24,  115 => 23,  57 => 19,  53 => 8,  50 => 7,  46 => 4,  43 => 3,  39 => 1,  37 => 59,  11 => 1,);
    }
}
