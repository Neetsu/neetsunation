<?php

/* CKBundle:Commitment:attachments.html.twig */
class __TwigTemplate_4dda886da1b8d97752b522ab0e69534b211860a4a44891dfb75b5a9b5cbed2d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3><span class=\"ck-twistie\" id=\"ck-twistie_attachments\">&nbsp;</span>";
        echo $this->env->getExtension('translator')->getTranslator()->trans("Attachments.label", array(), "messages");
        echo " (";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Attachments", array())), "html", null, true);
        echo ") ";
        // line 2
        if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
            echo "<span class=\"ck-accordion-right\">
<a href=\"javascript:addAttacmentUpload('atta')\" class=\"ck-attach-link\">";
            // line 3
            echo $this->env->getExtension('translator')->getTranslator()->trans("Attachments.add_atta", array(), "messages");
            echo "</a>
<a href=\"javascript:addAttacmentUpload('link')\" class=\"ck-link-link\">";
            // line 4
            echo $this->env->getExtension('translator')->getTranslator()->trans("Attachments.add_link", array(), "messages");
            echo "</a>
</span>";
        }
        // line 6
        echo "</h3>
<div class=\"ck-attachment_div_tb\">
<script type=\"text/javascript\" language=\"javascript\"><!--
var attachmentIndex=";
        // line 9
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "newAttachments", array())), "html", null, true);
        echo ";
//--></script>
<table>
";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Attachments", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["att"]) {
            // line 13
            echo "<tr>
  <td><a href=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["att"], "Url", array()), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["att"], "Name", array()), "html", null, true);
            echo "</a></td>";
            // line 15
            if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
                // line 16
                echo "<td>
    <button type=\"button\" onclick=\"toggleDeleteAttachment(this)\"
            data-altval=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Undelete"), "html", null, true);
                echo "\">";
                // line 19
                echo $this->env->getExtension('translator')->getTranslator()->trans("Delete", array(), "messages");
                // line 20
                echo "</button>";
                echo                 // line 21
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "attachments", array()), $this->getAttribute($context["loop"], "index0", array()), array(), "array"), "delete", array()), 'widget');
                echo "</td>
";
            }
            // line 23
            echo "</tr>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['att'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
            // line 26
            if (($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "attachments", array(), "any", true, true) &&  !twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "attachments", array())))) {
                // line 27
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "attachments", array()), 'widget');
                echo "
";
            }
            // line 34
            $context["btn"] = $this;
            // line 35
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newAttachments", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["att_f"]) {
                // line 36
                echo "<tr>
  <td>";
                // line 37
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["att_f"], 'widget');
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["att_f"], 'errors');
                echo "</td>
  <td>
    ";
                // line 39
                echo $context["btn"]->getdelete();
                echo "
  </td>
</tr>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['att_f'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "<tr id=\"add_attachment\"
data-prototype-atta=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newAttachments", array()), "vars", array()), "prototype", array()), 'widget', array("link" => 0)));
            echo "\"
data-prototype-link=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["newAttachmentPrototype"]) ? $context["newAttachmentPrototype"] : null), 'widget', array("link" => 1)));
            echo "\"
data-prototype-del=\"";
            // line 46
            echo twig_escape_filter($this->env, $context["btn"]->getdelete());
            echo "\"
>
<td colspan=\"2\"></td>
</tr>
";
        }
        // line 51
        echo "</table>
";
        // line 52
        if (((isset($context["canEdit"]) ? $context["canEdit"] : null) &&  !twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newAttachments", array())))) {
            // line 53
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newAttachments", array()), 'widget');
            echo "
";
        }
        // line 55
        echo "</div>
";
    }

    // line 29
    public function getdelete()
    {
        $context = $this->env->getGlobals();

        $blocks = array();

        ob_start();
        try {
            // line 30
            echo "<button type=\"button\" onclick=\"deleteNewAttachment(this)\">";
            // line 31
            echo $this->env->getExtension('translator')->getTranslator()->trans("Delete", array(), "messages");
            // line 32
            echo "</button>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "CKBundle:Commitment:attachments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 32,  186 => 31,  184 => 30,  175 => 29,  170 => 55,  165 => 53,  163 => 52,  160 => 51,  152 => 46,  148 => 45,  144 => 44,  141 => 43,  131 => 39,  125 => 37,  122 => 36,  118 => 35,  116 => 34,  111 => 27,  109 => 26,  107 => 25,  92 => 23,  87 => 21,  85 => 20,  83 => 19,  80 => 18,  76 => 16,  74 => 15,  69 => 14,  66 => 13,  49 => 12,  43 => 9,  38 => 6,  33 => 4,  29 => 3,  25 => 2,  19 => 1,);
    }
}
