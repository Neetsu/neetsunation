<?php

/* CKBundle:Commitment:history_item.html.twig */
class __TwigTemplate_205d5f258330cc3c751299c6a4653b13f401e2953bb41510d9e39730eb28b606 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "getExtra", array(0 => (isset($context["DateFormat"]) ? $context["DateFormat"] : null)), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["ext"]) {
            // line 2
            echo "<div class=\"ck-cd-history-item\">
  <img class=\"ck-cd-history-item-image\" src=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Actor", array()), "Picture", array()), "Url", array()), "html", null, true);
            echo "\" />
  <span class=\"ck-cd-history-item-details-date\">";
            // line 4
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "EventDate", array()), (isset($context["DateFormat"]) ? $context["DateFormat"] : null)), "html", null, true);
            echo "</span>&nbsp;
  <span class=\"ck-cd-history-item-details-person\">";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Actor", array()), "Name", array()), "html", null, true);
            echo "</span>&nbsp;
  <span class=\"ck-cd-history-item-details-role\">(";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Role", array()), array(), "history"), "html", null, true);
            echo ")</span>&nbsp;
  <span class=\"ck-cd-history-item-details-action\"></span>
  <div class=\"ck-cd-history-item-desc-short\">";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["ext"], "msg", array(), "array"), $this->getAttribute($context["ext"], "vars", array(), "array"), "history"), "html", null, true);
            echo "</div>
  <span class=\"ck-cd-history-item-desc-long\"><p>";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["ext"], "msg", array(), "array"), $this->getAttribute($context["ext"], "vars", array(), "array"), "history"), "html", null, true);
            echo "</p></span>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ext'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "<div class=\"ck-cd-history-item\">
  <img class=\"ck-cd-history-item-image\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Actor", array()), "Picture", array()), "Url", array()), "html", null, true);
        echo "\" />
  <span class=\"ck-cd-history-item-details-date\">";
        // line 14
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "EventDate", array()), (isset($context["DateFormat"]) ? $context["DateFormat"] : null)), "html", null, true);
        echo "</span>&nbsp;
  <span class=\"ck-cd-history-item-details-person\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Actor", array()), "Name", array()), "html", null, true);
        echo "</span>&nbsp;
  <span class=\"ck-cd-history-item-details-role\">(";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Role", array()), array(), "history"), "html", null, true);
        echo ")</span>&nbsp;
  <span class=\"ck-cd-history-item-details-action\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "ActionStr", array()), array(), "history"), "html", null, true);
        echo "</span>
  ";
        // line 18
        if ($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "NeedsTranslation", array())) {
            // line 19
            echo "    ";
            $context["shortDesc"] = $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "ShortDesc", array()), array(), "history");
            // line 20
            echo "    ";
            $context["longDesc"] = $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Description", array()), array(), "history");
            // line 21
            echo "  ";
        } else {
            // line 22
            echo "    ";
            $context["shortDesc"] = $this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "ShortDesc", array());
            // line 23
            echo "    ";
            $context["longDesc"] = $this->getAttribute((isset($context["hist"]) ? $context["hist"] : null), "Description", array());
            // line 24
            echo "  ";
        }
        // line 25
        echo "  <div class=\"ck-cd-history-item-desc-short\">";
        echo twig_escape_filter($this->env, (isset($context["shortDesc"]) ? $context["shortDesc"] : null), "html", null, true);
        echo "</div>
  <span class=\"ck-cd-history-item-desc-long\"><p>";
        // line 26
        echo (isset($context["longDesc"]) ? $context["longDesc"] : null);
        echo "</p></span>
</div>
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Commitment:history_item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 26,  99 => 25,  96 => 24,  93 => 23,  90 => 22,  87 => 21,  84 => 20,  81 => 19,  79 => 18,  75 => 17,  71 => 16,  67 => 15,  63 => 14,  59 => 13,  56 => 12,  47 => 9,  43 => 8,  38 => 6,  34 => 5,  30 => 4,  26 => 3,  23 => 2,  19 => 1,);
    }
}
