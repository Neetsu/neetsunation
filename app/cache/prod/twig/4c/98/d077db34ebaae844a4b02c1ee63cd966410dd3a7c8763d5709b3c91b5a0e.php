<?php

/* CKBundle:Settings:language.html.twig */
class __TwigTemplate_4c98d077db34ebaae844a4b02c1ee63cd966410dd3a7c8763d5709b3c91b5a0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4b4fcd0_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b4fcd0_0") : $this->env->getExtension('assets')->getAssetUrl("css/confirm_confirm_1.css");
            // line 8
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "4b4fcd0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4b4fcd0") : $this->env->getExtension('assets')->getAssetUrl("css/confirm.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        // line 13
        echo "<div id=\"wrapper\">
    <img id=\"commitkeeper\" src=\"/images/login_banner2.png\" alt=\"CommitKeeper\" />

    <div id=\"connect\">
        <table class=\"centre\">
            <tr>
                ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_array_keys_filter((isset($context["languages"]) ? $context["languages"] : null)));
        foreach ($context['_seq'] as $context["_key"] => $context["languageCode"]) {
            // line 20
            echo "                    <td><h2>";
            echo $this->env->getExtension('translator')->getTranslator()->trans("settings.chooseLanguage.header", array(), "messages", $context["languageCode"]);
            echo "</h2></td>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['languageCode'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "            </tr>
            <tr>
                ";
        // line 24
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["languageCode"] => $context["languageName"]) {
            // line 25
            echo "                    <td><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["urlPrefix"]) ? $context["urlPrefix"] : null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $context["languageCode"], "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, (isset($context["urlEnding"]) ? $context["urlEnding"] : null), "html", null, true);
            echo "\">
                            <img title=\"";
            // line 26
            echo twig_escape_filter($this->env, $context["languageName"], "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $context["languageName"], "html", null, true);
            echo "\" src=\"/images/flag_";
            echo twig_escape_filter($this->env, $context["languageCode"], "html", null, true);
            echo ".png\" />
                            <div class=\"caption\">";
            // line 27
            echo twig_escape_filter($this->env, $context["languageName"], "html", null, true);
            echo "</div>
                        </a></td>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['languageCode'], $context['languageName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </tr>
        </table>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Settings:language.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 30,  110 => 27,  102 => 26,  93 => 25,  89 => 24,  85 => 22,  76 => 20,  72 => 19,  64 => 13,  61 => 12,  45 => 8,  40 => 4,  37 => 3,  11 => 1,);
    }
}
