<?php

/* CKBundle:Commitment:show.html.twig */
class __TwigTemplate_73206fb355f1eb2cf631fc502dff366b5b5b883db0238117f368557c3469b286 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\" language=\"javascript\"><!--
var HasStatusUpdate = ";
        // line 2
        echo (((isset($context["HasStatusUpdate"]) ? $context["HasStatusUpdate"] : null)) ? (1) : (0));
        echo ";
//--></script>
<form id=\"c_detail\" action=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["form_action"]) ? $context["form_action"] : null), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo ">
<table class=\"ck-commitment-form-table\" cellspacing=\"0\" width=\"100%\">

<tr>
  <td class=\"ck-cd-header\" colspan=\"2\">
    <div class=\"commitment-status\">
      ";
        // line 10
        if ((isset($context["HasStatusUpdate"]) ? $context["HasStatusUpdate"] : null)) {
            // line 11
            echo "      <div  class=\"commitment-status-update\" id=\"showStatusUpdate\"";
            if ( !(isset($context["cssDisplayStatusUpdate"]) ? $context["cssDisplayStatusUpdate"] : null)) {
                echo " style=\"display: none\"";
            }
            echo ">
      ";
            // line 12
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statusUpdate", array()), 'widget');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statusUpdate", array()), 'errors');
            echo "
      </div>
      ";
        }
        // line 15
        echo "      <div class=\"commitment-status-icon\" id=\"showStatusIcon\"";
        if ((isset($context["cssDisplayStatusUpdate"]) ? $context["cssDisplayStatusUpdate"] : null)) {
            echo " style=\"display: none\"";
        }
        echo ">
      <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute((isset($context["trafficLight"]) ? $context["trafficLight"] : null), "ImageUri", array())), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trafficLight"]) ? $context["trafficLight"] : null), "Title", array()), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["trafficLight"]) ? $context["trafficLight"] : null), "Title", array()), "html", null, true);
        echo "\">
      </div>
    </div>
    <div class=\"commitment-title\">
      <h2>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Title", array()), "html", null, true);
        echo "</h2>
      <div class=\"ck-progress-area\">
        <div class=\"ck-progress\">
          <div class=\"ck-progress-step";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "activeStep", array(0 => 1), "method"), "html", null, true);
        echo "\">&nbsp;</div>
          <div class=\"ck-progress-step";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "activeStep", array(0 => 2), "method"), "html", null, true);
        echo "\">&nbsp;</div>
          <div class=\"ck-progress-step";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "activeStep", array(0 => 3), "method"), "html", null, true);
        echo "\">&nbsp;</div>
          <div class=\"ck-progress-step";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "activeStep", array(0 => 4), "method"), "html", null, true);
        echo "\">&nbsp;</div>
        </div>
        <span class=\"ck-progress-status\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["StatusMessage"]) ? $context["StatusMessage"] : null)), "html", null, true);
        echo "</span>
      </div>
    </div>
    <div class=\"ck-cd-topright\">
        <div class=\"ck-cd-topright-return-list\">
            ";
        // line 33
        echo $this->env->getExtension('translator')->getTranslator()->trans("topright.return%aStart%_%aEnd%", array("%aStart%" => (("<a href=\"" . (isset($context["returnUrl"]) ? $context["returnUrl"] : null)) . "\">"), "%aEnd%" => "</a>"), "messages");
        // line 34
        echo "        </div>

          ";
        // line 36
        if ((isset($context["superReqTitle"]) ? $context["superReqTitle"] : null)) {
            // line 37
            echo "        <div class=\"ck-cd-topright-return-parent\">
                  <span class=\"label\">";
            // line 38
            echo $this->env->getExtension('translator')->getTranslator()->trans("SuperRequest", array(), "messages");
            echo ":</span>
                  ";
            // line 39
            if ((isset($context["superReqUrl"]) ? $context["superReqUrl"] : null)) {
                // line 40
                echo "                  <a href=\"";
                echo twig_escape_filter($this->env, (isset($context["superReqUrl"]) ? $context["superReqUrl"] : null), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["superReqTitle"]) ? $context["superReqTitle"] : null), "html", null, true);
                echo "</a>
                  ";
            } else {
                // line 42
                echo "                  ";
                echo twig_escape_filter($this->env, (isset($context["superReqTitle"]) ? $context["superReqTitle"] : null), "html", null, true);
                echo "
                  ";
            }
            // line 44
            echo "                </div>
          ";
        }
        // line 46
        echo "    </div>
  </td>
</tr>

<tr>
  <td colspan=\"2\" class=\"ck-row-sep\">
    <hr />
  </td>
</tr>

<tr>
  <td class=\"ck-cd-2cols-equal\">
    <div class=\"ck-people\">
      <table width=\"100%\">
        <tr>
          <td class=\"ck-cd-label-col-left label\">";
        // line 61
        echo $this->env->getExtension('translator')->getTranslator()->trans("Requestor", array(), "messages");
        echo ":</td>
          <td class=\"ck-cd-field-col-right\">";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "requestor", array()), "Name", array()), "html", null, true);
        echo "</td>
        </tr>
        <tr>
          <td class=\"label\">";
        // line 65
        echo $this->env->getExtension('translator')->getTranslator()->trans("Performer", array(), "messages");
        echo ":</td>
          <td class=\"ck-cd-field-col-right\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "performer", array()), "Name", array()), "html", null, true);
        echo "</td>
        </tr>
        <tr>
          <td class=\"label\">";
        // line 69
        echo $this->env->getExtension('translator')->getTranslator()->trans("Observers", array(), "messages");
        echo ":</td>
          <td class=\"ck-cd-field-col-right\">
            ";
        // line 71
        if ((isset($context["canEditObservers"]) ? $context["canEditObservers"] : null)) {
            // line 72
            echo "            ";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "observers", array()), 'errors');
            echo "
            ";
            // line 73
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "observers", array()), 'widget', array("attr" => array("class" => "observers_table"), "formNewContact" =>             // line 75
(isset($context["formNewContact"]) ? $context["formNewContact"] : null), "newContactUrl" =>             // line 76
(isset($context["newContactUrl"]) ? $context["newContactUrl"] : null)));
            echo "
            ";
        } else {
            // line 78
            echo "            ";
            echo twig_escape_filter($this->env, ((array_key_exists("ObserverList", $context)) ? (_twig_default_filter((isset($context["ObserverList"]) ? $context["ObserverList"] : null), $this->env->getExtension('translator')->trans("observer.none"))) : ($this->env->getExtension('translator')->trans("observer.none"))), "html", null, true);
            echo "
            ";
        }
        // line 80
        echo "          </td>
        </tr>
      </table>
    </div>
  </td>
  <td class=\"ck-cd-2cols-equal\">
    <table width=\"100%\">
      <tr>
        <td>
";
        // line 89
        if ((isset($context["hasBudgetField"]) ? $context["hasBudgetField"] : null)) {
            // line 90
            echo "<span class=\"label\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("Budget", array(), "messages");
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["budgetUnit"]) ? $context["budgetUnit"] : null), "html", null, true);
            echo ":</span>
<span id=\"showBudget\" class=\"";
            // line 91
            echo twig_escape_filter($this->env, (isset($context["cssShowDate"]) ? $context["cssShowDate"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Budget", array()), "html", null, true);
            echo "</span>
";
            // line 92
            if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
                // line 93
                echo "<span id=\"showNewBudget\" class=\"";
                echo twig_escape_filter($this->env, (isset($context["cssEditDate"]) ? $context["cssEditDate"] : null), "html", null, true);
                echo "\">";
                echo                 // line 94
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newBudget", array()), 'widget');
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newBudget", array()), 'errors');
                // line 95
                echo "</span>
";
            }
        }
        // line 98
        echo "        </td>
        <td>
";
        // line 100
        if ($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Delivered", array())) {
            // line 101
            echo "          <span class=\"label\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("Delivered", array(), "messages");
            echo ":</span>
          <span id=\"showDelivered\">";
            // line 102
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "Delivered", array()), (isset($context["DateFormat"]) ? $context["DateFormat"] : null)), "html", null, true);
            echo "</span>
";
        }
        // line 104
        echo "        </td>
        <td>";
        // line 105
        if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newDueDate", array()), 'errors');
        }
        // line 106
        echo "          <div>
            <span class=\"label\">";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["DateLabel"]) ? $context["DateLabel"] : null)), "html", null, true);
        echo ":</span>
            <span id=\"showDueDate\" class=\"";
        // line 108
        echo twig_escape_filter($this->env, (isset($context["cssShowDate"]) ? $context["cssShowDate"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["Date"]) ? $context["Date"] : null), "html", null, true);
        echo "</span>";
        // line 109
        if ((isset($context["canEdit"]) ? $context["canEdit"] : null)) {
            echo "<span id=\"showNewDate\" class=\"";
            echo twig_escape_filter($this->env, (isset($context["cssEditDate"]) ? $context["cssEditDate"] : null), "html", null, true);
            echo "\">";
            echo             // line 110
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newDueDate", array()), 'widget', array("attr" => array("class" => "date_input")));
            echo "</span>";
        }
        // line 111
        echo "          </div>
        </td>
      </tr>
      <tr>
        <!-- Categories -->
";
        // line 116
        if ((isset($context["canEditCategoryValues"]) ? $context["canEditCategoryValues"] : null)) {
            // line 117
            echo "  ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "categories", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 118
                echo "        <td class=\"ck-cd-3cols-equal label\">";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["category"], 'label');
                echo ":</td>
  ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 120
                echo "        <td>";
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "categories", array()), 'widget');
                echo ":</td>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 123
            echo "  ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 124
                echo "        <td class=\"ck-cd-3cols-equal label\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "Name", array()), "html", null, true);
                echo ":</td>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 127
        echo "      </tr>
";
        // line 128
        if ((isset($context["canEditCategoryValues"]) ? $context["canEditCategoryValues"] : null)) {
            // line 129
            echo "      <tr class=\"ck-tags\">
        ";
            // line 130
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 131
                echo "        <td>
          ";
                // line 132
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["category"], 'widget');
                echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["category"], 'errors');
                echo "
        </td>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 135
            echo "      </tr>
      <tr>
        <!-- Categories errors -->
        <td colspan=\"form.categories|length\">";
            // line 138
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "categories", array()), 'errors');
            echo "</td>
      </tr>
";
        } else {
            // line 141
            echo "      <tr>
        ";
            // line 142
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["req"]) ? $context["req"] : null), "categories", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 143
                echo "        <td class=\"showCatagories\">
          ";
                // line 144
                if ($this->getAttribute($context["category"], "Tag", array())) {
                    // line 145
                    echo "          ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["category"], "Tag", array()), "Name", array()), "html", null, true);
                    echo "
          ";
                } else {
                    // line 147
                    echo "          ";
                    echo $this->env->getExtension('translator')->getTranslator()->trans("tag.no.value", array(), "messages");
                    // line 148
                    echo "          ";
                }
                // line 149
                echo "        </td>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 151
            echo "      </tr>
";
        }
        // line 153
        echo "    </table>
  </td>
</tr>

<tr>
  <td colspan=\"2\" class=\"ck-row-sep\">
    <hr />
  </td>
</tr>

<tr>
  <td class=\"ck-cd-history-header\">
    <span class=\"label\">";
        // line 165
        echo $this->env->getExtension('translator')->getTranslator()->trans("History", array(), "messages");
        echo ":</span>
    <button class=\"sort\" type=\"button\" id=\"ck_history_views\">";
        // line 166
        echo $this->env->getExtension('translator')->getTranslator()->trans("history.view.toFull", array(), "javascript");
        echo "</button>
    <button class=\"sort\" type=\"button\" id=\"ck_history_sort\">";
        // line 167
        echo $this->env->getExtension('translator')->getTranslator()->trans("history.sort.ascending", array(), "javascript");
        echo "</button>
  </td>
  <td class=\"ck-cd-next-action-header\">
    ";
        // line 170
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "action", array())) > 0)) {
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "action", array()), 'label');
        }
        // line 171
        echo "  </td>
</tr>
<tr>
  <td>
    <div id=\"history_list\" class=\"ck-cd-history-area\">
      ";
        // line 176
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history"]) ? $context["history"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["hist"]) {
            // line 177
            echo "      ";
            $this->env->loadTemplate("CKBundle:Commitment:history_item.html.twig")->display($context);
            // line 178
            echo "      ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hist'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 179
        echo "    </div>
  </td>
  <td class=\"ck-cd-action-area\">
    ";
        // line 182
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["form"]) ? $context["form"] : null), "action", array())) > 0)) {
            // line 183
            echo "    <div class=\"ck-cd-actions\">
      ";
            // line 184
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "action", array()), 'widget');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "action", array()), 'errors');
            echo "
    </div>
    <div class=\"ck-cd-comment-metadata\">
      <span class=\"ck-cd-history-item-details-date\">";
            // line 187
            echo twig_escape_filter($this->env, (isset($context["currentDate"]) ? $context["currentDate"] : null), "html", null, true);
            echo "</span>&nbsp;
      <span class=\"ck-cd-history-item-details-person\">";
            // line 188
            echo twig_escape_filter($this->env, (isset($context["UserName"]) ? $context["UserName"] : null), "html", null, true);
            echo "</span>&nbsp;
      <span class=\"ck-cd-history-item-details-role\">(";
            // line 189
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["actorRole"]) ? $context["actorRole"] : null), array(), "history"), "html", null, true);
            echo ")</span>&nbsp;
      <span id=\"current-action\" class=\"ck-cd-history-item-details-action\">";
            // line 190
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["currentAction"]) ? $context["currentAction"] : null), array(), "presenttense"), "html", null, true);
            echo ":</span>
    </div>
    <div class=\"ck-cd-comment-field\">
      ";
            // line 193
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'errors');
            echo "
      ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("rows" => "7")));
            echo "
    </div>
    <div class=\"ck-cd-form-buttons\">
      <input name=\"send\" type=\"submit\" class=\"ck-cd-form-input-send\" value=\"";
            // line 197
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("submit.commitment"), "html", null, true);
            // line 198
            echo "\" />
      <div class=\"ck-cd-form-input-cc\">
        ";
            // line 200
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "CCme", array()), 'widget');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "CCme", array()), 'label');
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "CCme", array()), 'errors');
            echo "
      </div>

      <a class=\"ck-cd-form-input-cancel\" href=\"";
            // line 203
            echo twig_escape_filter($this->env, (isset($context["returnUrl"]) ? $context["returnUrl"] : null), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("Cancel", array(), "messages");
            echo "</a>
    </div>
    ";
        }
        // line 206
        echo "  </td>
</tr>

<tr>
  <td colspan=\"2\" class=\"ck-row-sep\">
    <hr />
  </td>
</tr>

<tr>
  <td colspan=\"2\">
    <div id=\"bottom-accord\">
      <h3><span class=\"ck-twistie\" id=\"ck-twistie_supporting\">&nbsp;</span>";
        // line 218
        echo $this->env->getExtension('translator')->getTranslator()->trans("SuppRequests", array(), "messages");
        echo " (";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["req"]) ? $context["req"] : null), "SupportingRequests", array())), "html", null, true);
        echo ") ";
        if ((isset($context["canAddSupportingRequest"]) ? $context["canAddSupportingRequest"] : null)) {
            echo "<span class=\"ck-accordion-right\"><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["newSuppReqUrl"]) ? $context["newSuppReqUrl"] : null), "html", null, true);
            echo "\" class=\"ck-create-link\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("SuppRequests.add", array(), "messages");
            echo "</a></span>";
        }
        echo "</h3>
      <div class=\"bottom-content\">
";
        // line 220
        if ((isset($context["hasSupportingRequests"]) ? $context["hasSupportingRequests"] : null)) {
            // line 221
            $this->env->loadTemplate("CKBundle:Table:table.html.twig")->display($context);
        }
        // line 223
        echo "      </div>
";
        // line 224
        $this->env->loadTemplate("CKBundle:Commitment:attachments.html.twig")->display($context);
        // line 225
        echo "    </div>
  </td>
</tr>
</table>
";
        // line 229
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'rest');
        echo "
</form>
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Commitment:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  588 => 229,  582 => 225,  580 => 224,  577 => 223,  574 => 221,  572 => 220,  557 => 218,  543 => 206,  535 => 203,  527 => 200,  523 => 198,  521 => 197,  515 => 194,  511 => 193,  505 => 190,  501 => 189,  497 => 188,  493 => 187,  486 => 184,  483 => 183,  481 => 182,  476 => 179,  462 => 178,  459 => 177,  442 => 176,  435 => 171,  431 => 170,  425 => 167,  421 => 166,  417 => 165,  403 => 153,  399 => 151,  392 => 149,  389 => 148,  386 => 147,  380 => 145,  378 => 144,  375 => 143,  371 => 142,  368 => 141,  362 => 138,  357 => 135,  347 => 132,  344 => 131,  340 => 130,  337 => 129,  335 => 128,  332 => 127,  322 => 124,  317 => 123,  307 => 120,  299 => 118,  293 => 117,  291 => 116,  284 => 111,  280 => 110,  275 => 109,  270 => 108,  266 => 107,  263 => 106,  259 => 105,  256 => 104,  251 => 102,  246 => 101,  244 => 100,  240 => 98,  235 => 95,  232 => 94,  228 => 93,  226 => 92,  220 => 91,  213 => 90,  211 => 89,  200 => 80,  194 => 78,  189 => 76,  188 => 75,  187 => 73,  182 => 72,  180 => 71,  175 => 69,  169 => 66,  165 => 65,  159 => 62,  155 => 61,  138 => 46,  134 => 44,  128 => 42,  120 => 40,  118 => 39,  114 => 38,  111 => 37,  109 => 36,  105 => 34,  103 => 33,  95 => 28,  90 => 26,  86 => 25,  82 => 24,  78 => 23,  72 => 20,  61 => 16,  54 => 15,  47 => 12,  40 => 11,  38 => 10,  27 => 4,  22 => 2,  19 => 1,);
    }
}
