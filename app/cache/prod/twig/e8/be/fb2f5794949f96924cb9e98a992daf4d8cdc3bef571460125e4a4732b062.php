<?php

/* CKBundle:Form:Include/newContact.html.twig */
class __TwigTemplate_e8befb2f5794949f96924cb9e98a992daf4d8cdc3bef571460125e4a4732b062 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formNewContact"]) ? $context["formNewContact"] : null), 'errors');
        echo "
";
        // line 2
        $this->env->loadTemplate("CKBundle:Form:Include/inputUser.html.twig")->display(array_merge($context, array("form" => (isset($context["formNewContact"]) ? $context["formNewContact"] : null))));
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formNewContact"]) ? $context["formNewContact"] : null), 'rest');
        echo "
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Form:Include/newContact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 3,  23 => 2,  19 => 1,);
    }
}
