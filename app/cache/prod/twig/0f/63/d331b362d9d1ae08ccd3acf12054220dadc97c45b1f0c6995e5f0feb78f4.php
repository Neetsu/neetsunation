<?php

/* CKBundle:Security:login_debug.html.twig */
class __TwigTemplate_0f63d331b362d9d1ae08ccd3acf12054220dadc97c45b1f0c6995e5f0feb78f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CKBundle:Security:login.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'debug' => array($this, 'block_debug'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CKBundle:Security:login.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "9fd8b10_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9fd8b10_0") : $this->env->getExtension('assets')->getAssetUrl("css/login_debug_part_1_login_1.css");
            // line 9
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "9fd8b10_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9fd8b10_1") : $this->env->getExtension('assets')->getAssetUrl("css/login_debug_part_1_users_debug_2.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
            // asset "9fd8b10_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9fd8b10_2") : $this->env->getExtension('assets')->getAssetUrl("css/login_debug_button_2.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        } else {
            // asset "9fd8b10"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_9fd8b10") : $this->env->getExtension('assets')->getAssetUrl("css/login_debug.css");
            echo "<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
";
        }
        unset($context["asset_url"]);
    }

    // line 13
    public function block_debug($context, array $blocks = array())
    {
        echo "<td id=\"debug\">
<div id=\"trigger\" onclick=\"show_usernames();\">X
<select class=\"userselect\" id=\"userselect\"
        onchange=\"on_userselect(this.value);\">
<option value=\"\" selected=\"selected\">";
        // line 17
        echo $this->env->getExtension('translator')->getTranslator()->trans("login.selectUser", array(), "messages");
        echo "</option>
";
        // line 18
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 19
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "Name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo ")</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "</select>
</div></td>";
    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        // line 25
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "acd585c_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c_0") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug_translator.min_1.js");
            // line 33
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "acd585c_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c_1") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug_config_2.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "acd585c_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c_2") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug_part_3_en_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "acd585c_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c_3") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug_part_4_jainrain_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "acd585c_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c_4") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug_part_5_login_debug_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "acd585c"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_acd585c") : $this->env->getExtension('assets')->getAssetUrl("js/login_debug.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
        // line 35
        echo "<script language=\"javascript\" type=\"text/javascript\"><!--
setupJainRainLogin('";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["login_check_url"]) ? $context["login_check_url"] : null), "html", null, true);
        echo "');
//--></script>
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Security:login_debug.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 36,  152 => 35,  114 => 33,  110 => 25,  107 => 24,  102 => 21,  89 => 19,  85 => 18,  81 => 17,  73 => 13,  45 => 9,  41 => 4,  38 => 3,  11 => 1,);
    }
}
