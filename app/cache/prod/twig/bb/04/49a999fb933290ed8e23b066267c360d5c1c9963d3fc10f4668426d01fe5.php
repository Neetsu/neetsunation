<?php

/* CKBundle:Form:user_list.html.twig */
class __TwigTemplate_bb0449a999fb933290ed8e23b066267c360d5c1c9963d3fc10f4668426d01fe5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'user_list_widget' => array($this, 'block_user_list_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('user_list_widget', $context, $blocks);
    }

    public function block_user_list_widget($context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 9
        $context["macros"] = $this;
        // line 10
        $context["onchange"] = sprintf("onChange%s()", (isset($context["id"]) ? $context["id"] : null));
        // line 11
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : null), array("class" => "users_table"));
        // line 12
        echo "<div id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "container\" class=\"modal_container\">
  <div id=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_modaldialog\" class=\"users_dialog\">
    <div class=\"users_table_div\">
    <table ";
        // line 15
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo " data-prototype=\"";
        echo twig_escape_filter($this->env, $context["macros"]->getuserCheck((isset($context["prototype"]) ? $context["prototype"] : null), (isset($context["onchange"]) ? $context["onchange"] : null)));
        echo "\">
      ";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fixed"]) ? $context["fixed"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 17
            echo "      <tr>
        ";
            // line 18
            $context["f_id"] = sprintf("fixedUser%d", $this->getAttribute($context["user"], "Id", array()));
            // line 19
            echo "        <td><input id=\"";
            echo twig_escape_filter($this->env, (isset($context["f_id"]) ? $context["f_id"] : null), "html", null, true);
            echo "\" type=\"checkbox\" checked=\"checked\" disabled=\"disabled\" />
        <label for=\"";
            // line 20
            echo twig_escape_filter($this->env, (isset($context["f_id"]) ? $context["f_id"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "NameAndEmail", array()), "html", null, true);
            echo "</label></td>
      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "      ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 24
            echo "        ";
            echo $context["macros"]->getuserCheck($context["child"], (isset($context["onchange"]) ? $context["onchange"] : null));
            echo "
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    </table>
    </div>
    <div class=\"ck-dialog-newuser\" data-action=\"";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["newContactUrl"]) ? $context["newContactUrl"] : null), "html", null, true);
        echo "\">
      <span id=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_newcontact\">
        ";
        // line 30
        $this->env->loadTemplate("CKBundle:Form:Include/newContact.html.twig")->display($context);
        // line 31
        echo "      </span>
        <button class=\"ck-new-contact\" type=\"button\" onclick=\"newUserAdd(\$('#";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_newcontact'), \$('#";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "'))\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("userList.add", array(), "messages");
        echo "</button>
    </div>
    <div>
      <button type=\"button\" onclick=\"closeDialog";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "()\" class=\"ck-pad-top-botton\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("userList.save", array(), "messages");
        echo "</button>
      &nbsp;";
        // line 36
        echo $this->env->getExtension('translator')->getTranslator()->trans("userList.select.label", array(), "messages");
        echo ":
      &nbsp;<a href=\"javascript:;\" onclick=\"\$('#";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " input[type=checkbox]').prop('checked',true)\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("userList.select.all", array(), "messages");
        echo "</a>
      &nbsp;<a href=\"javascript:;\" onclick=\"\$('#";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " input[type=checkbox]').prop('checked',false)\">";
        echo $this->env->getExtension('translator')->getTranslator()->trans("userList.select.none", array(), "messages");
        echo "</a>
    </div>
  </div>
<a href=\"javascript:;\" id=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "list\" onclick=\"openDialog";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "()\">";
        echo twig_escape_filter($this->env, (isset($context["list_text"]) ? $context["list_text"] : null), "html", null, true);
        echo "</a>
</div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 44
        echo "<script type=\"text/javascript\">
var changed_";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " = false;
\$(document).ready(function() {
  \$('#overlay').click(function() { closeDialog";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "(); })
});
function ";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["onchange"]) ? $context["onchange"] : null), "html", null, true);
        echo " {
  changed_";
        // line 50
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " = true;
}
function openDialog";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "() {
    \$('#overlay').show();
    \$('#";
        // line 54
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_modaldialog').show();
}
function closeDialog";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "() {
    \$('#overlay').hide();
    \$('#";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_modaldialog').hide();
    if(changed_";
        // line 59
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo ") {
      alter_list(\$('#";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "list'),\$('#";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " td'),' ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("and"), "html", null, true);
        echo " ','";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans(        // line 61
(isset($context["empty_value"]) ? $context["empty_value"] : null)), "html", null, true);
        echo "');
    }
    changed_";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo " = false;
    newUserClean(\$('.ck-dialog-newuser'));
}
</script>
";
    }

    // line 3
    public function getuserCheck($__oneUserCheckForm__ = null, $__onchange__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "oneUserCheckForm" => $__oneUserCheckForm__,
            "onchange" => $__onchange__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 4
            echo "<tr>
  <td>";
            // line 5
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["oneUserCheckForm"]) ? $context["oneUserCheckForm"] : null), 'widget', array("attr" => array("onchange" => (isset($context["onchange"]) ? $context["onchange"] : null))));
            echo             // line 6
$this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["oneUserCheckForm"]) ? $context["oneUserCheckForm"] : null), 'label');
            echo "</td>
</tr>
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "CKBundle:Form:user_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 6,  231 => 5,  228 => 4,  216 => 3,  207 => 63,  202 => 61,  195 => 60,  191 => 59,  187 => 58,  182 => 56,  177 => 54,  172 => 52,  167 => 50,  163 => 49,  158 => 47,  153 => 45,  150 => 44,  140 => 41,  132 => 38,  126 => 37,  122 => 36,  116 => 35,  106 => 32,  103 => 31,  101 => 30,  97 => 29,  93 => 28,  89 => 26,  80 => 24,  75 => 23,  64 => 20,  59 => 19,  57 => 18,  54 => 17,  50 => 16,  44 => 15,  39 => 13,  34 => 12,  32 => 11,  30 => 10,  28 => 9,  26 => 2,  20 => 1,);
    }
}
