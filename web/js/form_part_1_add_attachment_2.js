function addAttacmentUpload(type)
{
    var last_tr = $('#add_attachment');
    var prototype = last_tr.attr('data-prototype-'+type);
    var newForm = prototype.replace(/__name__/g, attachmentIndex);
    attachmentIndex++;
    var newTD = $('<td></td>').append(newForm);
    var delBtn = last_tr.attr('data-prototype-del');
    var delTD = $('<td></td>').append(delBtn);
    var newTR = $('<tr></tr>').append(newTD).append(delTD);
    last_tr.before(newTR);
    newTD.find('input:first').focus();
}

function toggleDeleteAttachment(btn)
{
    var chk_box = btn.nextSibling;
    var setDeleted = !chk_box.checked;
    chk_box.checked = setDeleted;
    var td = chk_box.parentNode.previousSibling;
    if (setDeleted) {
        td.className = 'strikethrough';
    } else {
        td.className = '';
    }
    var btnText = btn.innerText;
    btn.innerText = btn.getAttribute('data-altval');
    btn.setAttribute('data-altval', btnText);
}

function deleteNewAttachment(btn)
{
    $(btn.parentNode.parentNode).remove();
}
