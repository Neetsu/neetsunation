function alter_list(alist, rows, and, text) {
  var list = new Array();
  var label, input;
  $(rows).each(function(){
    input = $(this).children('input')[0];
    if (input && input.checked) {
      label = $(this).children('label')[0];
      if (typeof (label.textContent) != "undefined") {
        text = label.textContent;
      } else {
        text = label.innerText;
      }
      pos = text.indexOf('<');
      if (pos > 0) {
          text = text.substring(0, pos - 1);
      }
      list.push($.trim(text));
    }
  });
  if(list.length) {
    text = list.pop();
    if(list.length) {
      text = list.join(', ') + and + text;
    }
  }
  $(alist).html(text);
}
